# Data delivery automation

A scheduler and task queue for running SAS jobs, finalizing and sending data,
with notifications, with command-line and browser frontends.

--------------------------------------------------------------------------------
_21 Mar 2018_

## Fixing SAS

SAS has such a helpful approach when you read in say character data and try to
cast it to a number. It gives you a note to the log, and moves on to the next 
record. "But I loaded all the records I could, don't you appreciate that?", it
says. "I did everything I could not to crash".  BUT I WANT YOU TO CRASH you
silly dangerous piece of junk....  Mind you, it doesn't consider even _warning_
you about data errors. Errors, SAS -- errors! Not notes tucked away in an
unformatted text file. MISSING RECORDS ARE SERIOUS ERROR CONDITIONS!

The "solution" is to manually abort on data error within _every_ data step. 
That's really no solution at all because (a) the programmers aren't going to
stand for it and (b) even if they are forced to, they are going to miss at 
least one data step per program, by mistake.

So they have invented an ingenious solution over the years: read in the log
file and look for key words indicating a data error occurred. (And hope your
read in data step doesn't itself have errors. And hope you're not picking
up false positives because your field or some value has ERROR in it somewhere.)

I can use the same technique outside of SAS, with a little work. I need
a guaranteed way to get the path to the log file. And my search doesn't
have to be as complex, because true ERROR or WARNINGs in the log will
have forced an error return code. We don't need to pull out every possible 
error line, just detect the presence of at least one.

We are looking for the presence of data-step level errors, which I _think_
look like  `/_ERROR_\s*=\s*[1-9]/`.  Although apparently there's also 
`/LOST CARD/` and `/SAS went to a new line when INPUT/`, if the programmer 
forgets to set STOPOVER in the infile data step. And the `_ERROR_=` line 
doesn't appear if you set `ERRORS=MIN`.  :| Instead you get 
`/Limit set by ERRORS= option reached/`


--------------------------------------------------------------------------------
_26 Feb 2018_

## A test plan

D. suggested a basic test strategy would be to compare the output of the 
manual SAS runs with the output from the automatic runs.

Now that I have the automation rig working, the temptation is to automate the
tests. I could schedule a run that does a checksum comparison of the SAS
output files. (Not the zip files, as it's likely 7zip and python's algorithms 
are slightly different). 

It's probably worth doing this for a sanity check, but they are going to want
to do manual tests independent of the automation rig. 

--------------------------------------------------------------------------------
_25 Feb 2018_

## A re-look at the status API

Right now it's a "list":

    GET /u/1/taskseq/x/status

What is a list of taskseq statuses? There are three levels involved here:

  1. The _task sequence_, which is the environment + task parameters abstracted
     from any particular run;

  2. The _job_, which is a task sequence scheduled to run according to a 
     particular recurrence rule -- a task sequence might be scheduled into
     several jobs;

  3. The _task sequence status_ per se, i.e. the status of each task in an
     actually run or running job -- and there may be multiple runs of a job.


I think it's too much to cram all of this into one endpoint. In particular I
think we should split the list of jobs from the list of statuses. So

    GET /u/1/taskseq/x/job
    GET /u/1/taskseq/x/status

we could still allow a filter by job on the status endpoint, etc. i.e.

    GET /u/1/taskseq/x/status?job=y

But most of the time clients would want to see status of any/all runs regardless 
of schedule, I imagine.

The returned structure looks like (simplified):

    [ 
      { status: 
        { seq_timestamp: ...
        , environ: { ... }
        , messages: 
            [
              { status: ...
              , timestamp: ...
              , message: { ... }
              , result: ...
              , error: { ... }
              }
            ]
        , status: ...
        , result: ...
        , error: { ... }
        }
      , environ: { ... }
      }
    ]


Sorted by `seq_timestamp` and then within messages by the status `id`.  

The other question is about environ. Do we duplicate that side by side with 
every status record? It seems unnecessary since environ is part of status. It
was included to shoehorn it into the CLI report, which was rendering a
"header line". But I think we do that differently. It didn't work right anyway.


--------------------------------------------------------------------------------
_17 Feb 2018_

Yes, the "status" API is still broken. I am aware of it. I think the problem
is basically trying to cram too much into one endpoint. It just needs a good
rethink.

But more urgent than that I think is finishing up email notification. 

For "implicit", i.e. automatic on failure or success, notifications, we could 
go back to sticking it into the environment:

    [Sequence]
    protocol = ABC-987
    account = 123456
    datatype = PFT
    sequence.name = Encoding
    failure.to =
        {ME}
    failure.cc =
        egjertsen@ert.com
    success.to =
        {ME}

(`{ME}` obviously gets us into the question of authentication, and that has to
wait for a discussion with IT.)

But in some sense these settings do not vary by task sequence. I think perhaps
it would be better to have them in application config, e.g. `notification.json`.
They would assume success and failure should go to the user who scheduled the
sequence. Config would specify only _other_ people who should always get the
success/failure notifications. 

But perhaps there should also be per-sequence recipients as well. The SA
should get particular notifications for programs they are responsible for
but not others.

UPDATE: the .ini file syntax looks like this (ignoring `{ME}` for now):

    [Sequence]
    protocol = ABC-987
    account = 123456
    datatype = PFT
    sequence.name = Encoding
    notification.failure.email.to =
        {ME}
    notification.failure.email.cc =
        egjertsen@ert.com
        someone.else@ert.com
    notification.success.email.to =
        {ME}
    notification.success.email.cc =
        egjertsen@ert.com
        someone.else@ert.com

Typically you wouldn't need to set the `from` in the sequence ini file, that
would be set via application config.


--------------------------------------------------------------------------------
_10 Feb 2018_

## Problem with the identity of actually run task sequences

I just realized that `task_seq_id` and `job_id` are not enough to identify
_actually run task sequences_.  The fact that I don't even have a good name for
this basic entity is a symptom of the problem.  The `job_id` identifies the
_schedule_, not a particular schedule instance. Luckily however, we already 
have a piece of state that will serve quite well: the _sequence timestamp_,
aka `Environ.timestamp_started_at`.

We need a new field in the `task_status` table: proposing `seq_timestamp`.

--------------------------------------------------------------------------------
_7 Feb 2018_

## More rethinking notifications

### Try 1: pipeline callbacks

A more robust notification design would not immediately send emails, but store
them up to be sent off periodically _under a single SMTP session_; or assuming
"notification" could be abstracted, determining where and how to send them off
_as a group_.

But this would require a bit more infrastructure, it seems. I am not really
concerned about DoS'ing our SMTP server so I don't think it's worth it to try
to batch emails like this. 

I'm more concerned about the awkwardness of the sequence success and error
callbacks. Really the notification bit should be done async, as a separately
queued task. The race condition can be dealt with by putting a delay on the
notification task.

I am considering adding a "final callback" of some sort in the pipelines 
middleware, when there isn't a `next_message`. Then we could trigger email
notification there. Likewise for the error callback. It makes a nice separation
of concerns, but does open the possibility of race conditions with other
callbacks, unless you use a delay.

The other option is to change the callback middleware to accept a list of
tasks to run, which would be set specially for the last task in the list, and
for the error callback. This has no advantage that I can see though. You still
have to deal with race conditions, and it's more inconvenient to set up.

### Try 2

The problem with adding callbacks to the pipelines middleware, as elegant as
it seems, is that the "last message in pipeline" is indistinguishable from
"single actor message". So any non-pipeline message, including plain callbacks,
are going to then trigger the pipelines callbacks. The way around this would be
to mark all pipeline messages with a certain option, or conversely mark all
single-actor messages with a certain option. But both of these seem very
fragile. Also having callbacks fired two different places introduces a bunch
of entropy in itself.

So the question becomes, with callbacks in one place, how to trigger multiple
callbacks? That might be sequenced, or at least delayed, to avoid race
conditions?

The most flexible perhaps is to pass in a function (a true callback), that does
the enqueuing itself. Something about the open-endedness of this makes me
nervous.

But let me step back for a moment. Essentially I am using callbacks right now
for basic storage of task results. What if this were done instead using an
_application-specific_ middleware, separate from -- and fired off earlier than
-- the callbacks?  Then the only thing callbacks would be used for would be 
firing off the notification emails. And we could add a custom `callback_delay`
actor option to deal with the race. 

**In fact**, we could get rid of the callbacks middleware altogether, I think.
Only define 'global' callbacks for the pipelines middleware. Then every message
that goes through the system is either considered a pipeline message or a
pipeline callback. And pipeline callbacks can be prevented from themselves
triggering pipeline callbacks. There are no individualized callbacks per message 
to worry about.

**Also**, I would like to reexamine dramatiq's built-in Results middleware.
We know it's inadequate for what I'm doing with pipelines. But perhaps 
something could be built on top of it.


_6 Feb 2018_

## Rethinking pipeline state

Ran into this stumbling block trying to implement error notifications: the
'sequence state' needed to select task status records, namely `task_seq_id` and
`job_id`, are hidden when you get to the error callback. I mean, you *could*
get them from the serialized message -> kwargs -> environ -> runtime, but that
is extremely icky.

This reminded me that the way I pass along environ state (including so-called
runtime state), is basically a hack, and needs to be redone anyway.

Here are some lessons.

  1. 'Environ' state is state set by the user from the top. This is distinct
     from state that gets attached to the task sequence when it's prepared
     for enqueuing: so-called runtime state.

  2. Runtime state is 'known' to the framework; environ state is opaque to the
     framework.

  3. Runtime state should be available to _both_ the framework _and_ the tasks
     themselves. In nuts and bolts terms, this means runtime state should be
     available to middleware as message options, _and_ injected into task args
     or kwargs.

  4. The current way timestamps are set for the status records are a bit
     inaccurate. They should be set as early as possible in the first 
     `after_process_message` middleware that fires.

     
My proposal is the following.

  1. Inject 'environ' state as currently, from the top, into kwargs of each 
     message in the sequence.

  2. But runtime state no longer is part of environ. Runtime state is injected
     as a message _option_ for each message in the sequence, _OR_ into the
     first message options only.

  3. Runtime state is injected into tasks in the `pipelines` middleware, along
     with the `last_result`. Potentially this injected state is updated with
     e.g. last task run time or timestamp, etc.

  4. Signatures of all actor functions need to change to add in `runtime`:
     `(runtime, last_result, *, environ, **kwargs)`

  5. Function to combine `runtime` and `environ` data for string templating.

  6. The storage function signatures should be changed to 
     `(runtime, msgdata, timestamp, result)`. Then the `task_seq_id` and 
     `job_id` keys can be pulled out of the runtime. Also if we include
     timestamp in the runtime data.


### Update

If we move `task_seq_id`, `job_id`, and `timestamp` into `environ`, where they
really belong (they don't change), I'm not sure we really need `runtime`. We
could add `runtime` back in as a message option, to store a more accurate
timestamp for the storage callbacks, but might as well just pass these the
_timestamp itself_, not some piece of state that gets passed down the pipeline.


--------------------------------------------------------------------------------
_4 Feb 2018_

## Back to notifications

I think the place to start is with the "explicit" notifications we know about: 
the email that gets sent out at the end of the process, to client contacts for 
instance.

--------------------------------------------------------------------------------
_1 Feb 2018_

## Chaining SAS jobs

The typical process is :

  1. a "gensas" program, which reads data in from csv files and checks it for 
     errors, generating SAS datasets in a standard format; and then
  
  2. an "encoding" program, which processes the standard SAS datasets into
     client-specific formats, which may be other SAS datasets or may be text
     or other file types.

The problem from the point of view of using task chaining (passing the result
of one task into the next), is that the inputs and outputs are a different type,
so this complicates modelling both kinds of SAS tasks using a single function.

If we want to capture everything that a SAS program outputs through chaining,
it gets to be quite hairy. You have the log and output files, the SAS files,
Accounting and other reports, etc. Each SAS program might do something
different.

I think the easiest way to deal with this at the moment is to effectively
return _nothing_ from SAS transactions. The knowledge about file conventions are 
encoded within the task parameters instead. 

What about the input then? Some transactions (e.g. gensas) will expect input
file(s), while others (e.g. encoding) will not. The easiest way to deal with
this is to have an optional parameter, e.g. `last_result_var`:

    transaction_run( last_result, last_result_var='extract', ...)

When specified, you get something like `%LET extract = {last_result};` -- and
when not specified, you don't get that line.


--------------------------------------------------------------------------------
_30 Jan 2018_

## Task sequence timestamp

We don't have a unique identifier for a _task sequence, run at a particular
point in time_. We have `task_status`, which is an even finer level of 
granularity (a single task within a sequence run at a particular point in time);
and we have `job` from APScheduler, which is a task sequence _recurrence rule_,
basically; and we have the abstract `task_sequence`.

And we have a _message_ id and timestamp as well, which is when the individual 
task was constructed right before scheduling (not when run). Not helpful.

BUT, when we reconstruct the message off the scheduler, i.e. when the sequence
starts to run, we can reset the ID and timestamp of the message by setting
these fields to `None`. Then we should be able to get a run-time timestamp off
of `environ.runtime.timestamp`. Granted it *may* not be unique for the
whole sequence, but the odds of that are slim since the timestamps for the
each of the tasks in the sequence are set in a tight loop on the order of 0.001
seconds or less.


_24 Jan 2018_

## SAS details

Right now we have no controls over what the running SAS program does. How it
gets its input and where it puts its output and logs are completely up to the
program. This is less than ideal, especially given we have to be sure that
the files we pick up downstream are the actual files that were just processed
by SAS, and likewise the files that SAS picks up are the right ones from the
previous step. 

Ideally each SAS run would be done in a kind of sandbox, but I think sandboxing
SAS in the way we would want is probably not possible. So we are left with
conventions. 

My thought is to generate not only a cmd file, but a SAS 'environment' file 
that `%include`s the main SAS program. This file would set all the necessary
macro variables, set the log output, and initialize the ODS (PDF) output. It
also would clean up afterwards (close the ODS output for instance). It would
be a very pared down version of their `SAS_Shell` macro: no directories created,
no messagebox prompt, no error handling.

The thing I am realizing is that these SAS programs typically write a bunch of
different files in a bunch of different places according to arcane conventions
that may or may not be documented in some SWI. 

For encoding runs I think output looks like this:

    <account>
        |
        ------ documents/sdlc/encoding/<version>/encode_log_<version>.pdf
        ------ ToSponsor/*.sas7bdat or .xpt
                 |
                 -------- Backup/*_<timestamp>.sas7bdat or .xpt
                 -------- DM_review/encode_output_<version>_<timestamp>.pdf
                 

For consistency's sake I would _love_ to move all the output under a single
timestamp folder, or version-timestamp folder: 


    <account>
       |
       ------ auto/encoding 
                |
                ------- <version>_<timestamp>
                            |
                            ------- log
                            ------- output
                            ------- ToSponsor
       

Taking it one step further: given running the _gensas_ and then _encoding_ are
part of the same sequence:


    <account>/
       |
       ------ auto/ 
                |
                ------- <timestamp>/
                            |
                            ------- gensas/
                            |         |
                            |         ------ log/
                            |         ------ output/
                            ------- checks/
                            ------- encoding/
                            |         |
                            |         ------ log/
                            |         ------ output/
                            ------- data/
                            ------- ToSponsor/
                            ------- eCOA_gensas_<account>_<version>.sas
                            ------- <account>_<version>.sas
                            ------- gensas.cmd
                            ------- gensas.sas
                            ------- encoding.cmd
                            ------- encoding.sas

 
Note copying the SAS programs themselves into each timestamp subfolder. That
way it could be run again without danger of the program changing. (We could also
copy the csv source files into the timestamp folders, but that could 
significantly increase storage requirements if we are running every day.)

## What needs to change in the existing SAS programs?

Right now the encoding programs are designed to be run themselves from the top;
so the actual account-specific body of the program is run between two calls to
this `SAS_shell` macro that sets up and tears down the environment. My
suggestion will be to move the body of the program out, so we can run it either
within the `SAS_shell`, or some other environment. My preference would be to
write this as a function (SAS macro, the closest you can get to a function in 
SAS) so the inputs are explicit, but ... whatever they want to do.

*UPDATE 1/30*: They tell me that the body of the program should not use any
macro variables. They are only needed for the environment setup, read-in, etc.
So it's probably safe to just `%include` it.


## How to model this in terms of tasks

We don't want to burden our (at this point theoretical) end user with the
construction of tasks which are really implementation details. There should be
no reason they need to `SetupSasSandbox(..) | RunSas(..)` . There should be
a top level task `RunSasInSandbox(..)`, or just call it `RunSas(..)`, since
we don't want to run SAS in any other way. Keep the minimal `RunSas` of course
but as a simple function, not a task. 



--------------------------------------------------------------------------------
_14 Jan 2018_

## Notifications

It's possible to stick notifications onto the end of the pipeline (and onto the
failure callbacks). But I am considering, now that we have persistence sorted,
that it could just as easily be done after the fact. Which would allow the
possibility for batching, i.e. digest emails, and rate control. The disadvantage
is once we are polling the dbase it's no longer on a queue and we have to 
manage updating notification status manually. I think that kills this option.
It's not worth it just to get email batching -- there won't be enough emails
sending out to worry about this anyway.

Perhaps this is a case for `group` sequences -- i.e. both the persistence and
the notifications fired off in parallel. That would mean a change to the
callbacks middleware, which currently deals with single-actor messages.

### Do we need notification config in the environ?

I would rather move it out. I feel like notification should be a bit more
flexible in how it's configured. And we don't have the infrastructure in place
yet to really be making decisions about it. It's really a UI question and we
don't have a UI yet.

### What is the environ?

Originally the concept was to have a set of variables that could be used by
individual tasks, either directly or through templating, so e.g. you can save
a file which includes the account number or job ID or whatever in its name; or
you can set a SAS macro variable; etc. The concept is not bad. But notification
data is quite a bit more structured than that, and anyway it's not useful data
for arbitrary tasks -- it's useful only for notification tasks.

--------------------------------------------------------------------------------
_13 Jan 2018_

## On the other side of the demo

With task sequencing more or less pinned down (and soon to be rolled into
dramatiq core), my thoughts go to the last bit of 'queue infrastructure', which
is results storage.

The primary aim is to have available the status (not yet run, success, or 
failure) of each of the subtasks within a task sequence, accessible via some
kind of sequence identifier. Secondarily, it would be useful to have details:
what the result was of a particular subtask, what the failure and traceback
was, what the external logs were in the case of SAS.

Dramatiq's result stores are not quite sufficient. They are not set up to
capture errors, and they identify messages opaquely -- i.e. they expect you to
query the results while the message is in memory -- and not as part of a 
pipeline. 

Also it would require setting up memcached on Windows, which looks a bit shaky.

To me, I don't understand why we couldn't just push results/failures back onto
rabbitmq (using an option to protect against infinite regress), to be handled
by actors that write them to a db. _We don't need strong consistency_ in this
case, ie. we don't need to have the worker wait for the sequence to complete.
The frontend clients are going to be polling anyway.

And we can store the "sequence id" (which can be the apscheduler job id, if
we pre-generate it) as part of the environ each message has. Then the results
store can index on this field.

    CREATE TABLE IF NOT EXISTS task_result
         ( id INTEGER PRIMARY KEY AUTOCOMPLETE 
         , seq_id TEXT    # the apscheduler id
         , message_id TEXT   # hash of the message ? or just message.id ?
         , status TEXT
         , status_timestamp FLOAT
         , message BLOB
         , result BLOB
         , log BLOB
         )
    ;

    # querying sequence status
    SELECT * FROM task_result WHERE seq_id = ? 
        ORDER BY status_timestamp;

    # before_process_message
    INSERT INTO task_result
        ( status, status_timestamp, seq_id, message_id, message )
        VALUES (?, ?, ?, ?, ?)
    ;

    # on success
    UPDATE task_result 
        SET status = 'SUCCESS', status_timestamp = ?, 
            message = ?, result = ?, log = ?
        WHERE seq_id = ? and message_id = ?
    ;

Hopefully, it's ok to do these kinds of updates. It should be, given all dbase
writes are going through the queue; we should be guaranteed to have a record
by the time the task succeeds, for instance.

The thing it doesn't have is the overall status of the sequence; how do we
know if the sequence has finished? 

My proposal is to have a special task at the head of the sequence that inserts
records for the rest of the sequence, e.g. with status "NOT STARTED". Then
each task can just UPDATE its status; and success of the sequence can be 
determined by status = "SUCCESS" for all the records in the `seq_id`.



--------------------------------------------------------------------------------
_9 Jan 2018_

## Again priorities

I seem to need to flesh out type safety before getting to anything else -- is
this a general programmer trait or a personal obsession?

The point is not spending too much time on the user interface no matter what 
form -- we have to focus on getting the scheduler and queue hooked up. And the
actual tasks written!

This structure sent from the client is very close to what needs to be sent
directly to a function named `check_tasks`. Let's save the validation for later.

    { "template": "CheckFiles"
    , "params":
        { "in_directory": "%HOME%\\csv"
        , "files_named": "*.csv"
        , "modified":
            { "quantity": 1
            , "unit": "hours"
            , ...
            }
        , ...
        }
    }




--------------------------------------------------------------------------------
_8 Jan 2018_

## Clarification of priorities

This morning, struggling with various concepts around data serialization between
front and back ends, and thinking about the demo later this week, and all that
needs to be done quite apart from that, I thought: _What if I only demo the
frontend?_ Nothing hooked up, just a form for task sequences and a form for
scheduling them. Then just spend the time on presenting the backend in an 
understandable way. That way we can gauge better if I'm going down the right
path.

D. then spoke to me this afternoon: what is needed is _exactly the reverse_. 
We don't need a web interface. We need the actual scheduling/queueing of jobs
as a proof of concept. If necessary, I can be the one to feed the job
and scheduling parameters into the system. The users can fill out a form, e.g.
Google form, in the meantime, which I can manually use to set up scheduled
jobs. And we can even live with that for awhile, if I can show someone else how
to do it too.

## Command line interface?

I'm imagining an .ini file format for job parameters, plus a cron expression
for scheduling.

    $ bin/sched task --account 050123 --datatype ECG --task "m:/ecg/050123/auto.ini" \
        --cron "0 16 15 * 1-5"  --host "localhost" --port "8080"

    [345:987] Scheduled task ECG 050123 Encoding
    Next run time: ---

    $ bin/sched change 345:987 --cron "0 17 15 * 1-5"

    [345:1089] Changed schedule for task ECG 050123 Encoding 
    Next run time: ---

    $ bin/sched cancel 345:1089

    [345:1089] Cancelled schedule for task ECG 050123 Encoding

    $ bin/sched add 345 --cron "0 17 * * 1-5"

    [345:1102] Scheduled task ECG 050123 Encoding
    Next run time: ---

    $ bin/sched list 345 --format csv

    task_id,sched_id,task,sched_by,sched_status,queue_status
    345,1102,Scheduled task ECG 050123 Encoding,egjertsen@ert.com,Next run on ---,Not queued


Authentication ...? Of course. But it will be just me running on localhost for
awhile here.


## Sidebar on typing tasks

There is always a tension between 'strongly typed' tasks, task 'templates', and
the need to construct _new_ tasks as a list of named typed parameters -- 
essentially dicts. 

From the point of the view of the future (always dangerous to take this point
of view), almost certainly if this system is put into production there will
be a need for new tasks. It's less work if we don't need to design a type,
encoder, decoder, and schema for each new task, in the front-end. Not because
these need to be constructed dynamically by users, necessarily: but because it's
a hell of a lot of work _for me_. 

From the point of view of the user, it's better to be able to prevent type
errors, or at least report them, as soon as possible. By the point they get
submitted to the scheduler, they had _better_ be in the right shape, but ideally
the user will find this out much sooner.

Now we have two clients. It would be nice to move the task typing out of 
the client and into either the server or JSON schemas. 

For the Elm client, I think it means changing Data.Task.Template from 
essentially

    type Template 
        = CheckFiles (List (String, Param)) 
        | Sas (List (String, Param)) 
        | ...

to just a value type:

    type Task = Task { template : Template, params : List (String, Param) }

The server controls what params go under what template in what order. So the
'task template' builder and concrete templates in View.Task.Template will go
away. The one complication is the validation functions; but let's say we have
regex + named formats that correspond to functions.

(A side effect of this is **we can drop the top two 'ctor' levels in the
data**. We only need 'ctor' for parameters, which have UI implications and thus
need this bit of info for the Elm client to be able to reconstruct the view.)

The thing is, templates are not the same as types. JSON schemas are not going
to cut it, unless we use `default`, and write something to generate default
instances from schemas. In that case we can't use JSON schema's reuse mechanism 
(`$ref`) for parameter defaults (and I don't want to even look at what they've 
dreamed of with `$extend` since I quit paying attention), but that shouldn't 
matter. It does get a bit confusing in terms of `ctor` and communicating
type information in the instance itself.

Here are the first two parameters of the CheckFiles task as JSON schema. As
you can see, it's quite verbose.

    { "type": "array"
    , "items": 
        [ { "type": "array"
          , "minItems": 2
          , "maxItems": 2
          , "items":
              [ { "type": "string", "constant": "in directory" }
              , { "type": "object"
                , "required": ["ctor", "value"]
                , "properties":
                    { "ctor": { "type": "string", "constant": "TextValue" }
                    , "value":
                        {"type": "string", "default": "%HOME%"}
                    }
                }
              ]
          }
        , { "type": "array"
          , "minItems": 2
          , "maxItems": 2
          , "items":
              [ { "type": "string", "constant": "files named" }
              , { "type": "object"
                , "required": ["ctor", "value"]
                , "properties":
                    { "ctor": { "type": "string", "constant": "TextValue" }
                    , "value":
                        {"type": "string"}
                    }
                }
              ]
          }
        , ...
        ]
    }
    

Another possibility is to move the param label (and identifier) into the
Param type; not just the ParamForm type.

Then we could have

    type Task = Task { template = Template, params = List Param }

and

    { "type": "array"
    , "items": 
        [ { "type": "object"
          , "properties":
              { "ctor": { "type": "string", "constant": "TextValue" }
              , "value":
                  { "type": "object" 
                  , "properties": 
                      { "label" : 
                          { "type": "string", "default": "in directory"  }
                      , "identifier": 
                          { "type": "string", "default": "in_directory" }
                      , "value": 
                          { "type": "string", "default": "%HOME%" }
                      }
                  }
              }
          }
        , ...
        ]
    }

Unfortunately, we can't do a simple `{ identifier: param }` dict if we want
to preserve param order for form editing.

The idea is that from this schema we could derive a default CheckFiles template
object -- a tuple of dicts:

    ( { "ctor": "TextValue"
      , "value": 
          { "label": "in directory"
          , "identifier": "in_directory"
          , "value": "%HOME%"
          }
      }
    , ...
    )


I _think_, armed with server-defined task templates of some kind, we can drop 
the extra parameter-type information in the proposed .ini file.

If we have a default template object as described above, we can insert data from 
the CLI client sent in a very simple format -- based on identifier:

    { "template": "CheckFiles"
    , "params":
        { "in_directory": "%HOME%\\csv"
        , "files_named": "*.csv"
        , "modified":
            { "quantity": 1
            , "unit": "hours"
            , ...
            }
        , ...
        }
    }

Any unknown identifiers will result in an error. The final merged object is
then validated against the template schema, which will detect any type errors
etc.



## Back to the CLI

Let me flesh out what a typical ini file might look like.

    [Sequence]
    protocol = ABC-987
    account = 123456
    datatype = PFT
    sequence.name = Encoding
    error.to =
        %ME%
    error.cc =
        egjertsen@ert.com
    success.to =
        %ME%
    confirmation.from = Eric Gjertsen <egjertsen@ert.com>
    confirmation.to =
        johndoe@sponsor.com
        janesmith@cro.com
    confirmation.cc =
        %ME%
        someoneelse@ert.com
    confirmation.template =
        Dear John and Jane,
        Please find your latest data for %PROTOCOL% zipped and uploaded to the FTP site
        (%PROTOCOL%_%SEQUENCE_TIMESTAMP%.zip). Please let me know if any questions.
        Eric

    [CheckFiles.0]
    in_directory = %HOME%\csv
    files_named = *.csv
    modified.range = within
    modified.quantity = 1
    modified.unit = hours
    modified.comparison = before
    modified.time_var = scheduled

    [Sas.0]
    in_directory = %HOME%
    program_file = %ACCOUNT%_v1_0_1.sas
    sas_options = 
        -ERRORABEND 
        -NOOVP
        -SPLASH OFF

    [Zip.0]
    in_directory = %HOME%\data
    files_named = *_.xpt
    modified.range = within
    modified.quantity = 10
    modified.unit = minutes
    modified.comparison = after
    modified.time_var = last_queued
    to_directory = %HOME%\ToSponsor
    zip_name = %PROTOCOL%_%SEQUENCE_TIMESTAMP%.zip
    zip_refresh = 0

    [Checkfiles.1]
    in_directory = %HOME%\ToSponsor
    files_named = %PROTOCOL%_%SEQUENCE_TIMESTAMP%.zip
    
    [Sftp.0]
    in_directory = %HOME%\ToSponsor
    files_named = %PROTOCOL%_%SEQUENCE_TIMESTAMP%.zip
    

Notes:

- ini section names  --> task template types. The .0, .1 etc. is ignored, it's
  just to make repeated task types unique.

- ini param names  --> task param keys

- complex params are "param.key"

- group text params are listed one per line

- %VARIABLES% are globally scoped and resolved when the task sequence is
  run.

- %SEQUENCE_TIMESTAMP% is useful as a timestamp that identifies a single
  sequence run. So you can identify files without having to rely on timing.
  
- Perhaps in addition we want to have %LAST_RESULT% which is what the
  previous task returned, as a string? Not sure.

- %ME% is the email of the person who schedules the task.

- Three kinds of notifications are parameterized: error and success are
  internal; and confirmation is external, with a custom template.


With ini files, all values are strings. This only comes into play in a few
places, e.g. `modified.quantity` (int) and `zip_refresh` (bool) above. Might
just deal with this manually in the CLI client.



--------------------------------------------------------------------------------
_7 Jan 2018_

## Schemas

Looking at the python jsonschema library, it seems like the mechanism for
resolving references (JSON pointers) within a collection of schema documents
is RefResolver. The API is weird but I think I understand it. It means that
either (1) each schema needs a top-level id which is a URL; or (2) there is
some convention for mapping file names to a URL. The `schemas` object then
that you load in is a `store` that you pass in to `RefResolver` when you need
to resolve a reference.

Beyond that, there are various places schemas are needed. Typically:

CRUD verb             |  What validated
----------------------|---------------------------
list                  |  URL query, response
create                |  request
read                  |  response
update                |  request
delete                |  -

I suppose you could have three possible schemas for every verb. I mean, `delete`
is unlikely to have request/response, but could have query params. `list` and
`read` are unlikely to have request params. But it just makes implementation
easier if they all have the possibility of all three.

So the schema files might look like this, one file per _resource_:

    { "id": "http://localhost/schema/api/1/taskseq"
    , "definitions":
        { "list.query": 
            { "properties":
                { "limit": {  }
                , "from": { }
                }
            , "required": ["limit"]
            }
        , "list.request": { "type": "null" }
        , "list.response": 
            { "type": "array"
            , "items": { "$ref": "#" } 
            }
        ...
        }
    , "properties":
        { ... schema for taskseq entity ... }
    }

So you'd stick the query/request/response schemas in the _definitions_. 

Alternatively you could have separate schema files, e.g. `taskseq`, 
`taskseq.list.query`, `taskseq.list.response`, etc. which might make it 
slightly easier to generate but means either (1) remote references which are 
annoying or (2) schema duplication.

In any case, there's the question of 'what is the minimum amount of conventions
can I bake into this mini framework'?  

It occurs to me validation is a good candidate for decorators/wrapping 
functions. 

    api = (
        Api([
            resource( 'taskseq', id=r'\d+'
                list=validate_response('http://localhost/schemas/api/1/#/definitions/list.response', schemas)(taskseq.list),
                update=validate_request('http://localhost/schemas/api/1/#/definitions/update.request', schemas)(taskseq.update),
                resources=(
                    resource( 'schedule',
                        create=taskseq.schedule.post,
                        delete=taskseq.schedule.delete
                    ),  
                    # ... other sub-resources
                )
            ),
            not_found( errhandlers.not_found ),
        ], config = config
         , id = '1'
         , name = 'u'
        )
    )

Of course that's ugly, but it's the most generic: it makes no assumptions about
how the schemas are organized. On top of this you could easily build:

    api = (
        Api([
            resource( 'taskseq', id=r'\d+'
                list=validate_list('taskseq')(taskseq.list),
                update=validate_update('taskseq')(taskseq.update),
                resources=(
                    resource( 'schedule',
                        create=taskseq.schedule.post,
                        delete=taskseq.schedule.delete
                    ),  
                    # ... other sub-resources
                )
            ),
            not_found( errhandlers.not_found ),
        ], config = config
         , id = '1'
         , name = 'u'
        )
    )

where `validate_list`, `validate_update` could take the schemas from 
`config.schemas`, and build the url/fragments using whatever convention we
decide on.

What I'm saying essentially is that I don't think we need to make schemas a
core part of the handlers, it doesn't need to be a first class concept of the
Api, it can be a kind of extension through these decorators.

And more importantly, we can **put off working on schema validation** and get
on with this thing I am supposed to demo in 4 days!


--------------------------------------------------------------------------------
_4 Jan 2018_

## WSGI

I know people like Flask and it's familiar and all that, but the thread local
globals business grates on my nerves. Magic for the sake of supposed 'developer
happiness' DOES NOT MAKE ME HAPPY! (It also comes bundled with a bunch of crap
I don't need (Jinja etc) -- nothing like Django of course but still.) Give
me `def handle_route(req, res)` and my mind is already more at ease.

At the same time my little `fungi` framework is not appropriate here. Too many
different services and side effects to have to shoehorn myself and future 
maintainers into a (mostly-) pure functional paradigm. Also not certain it is
python 3 compatible.

But it's not hard to write a router + config "framework" on top of WebOb that
does just what I need it to and no more.

### Draft 1

    schemas = load_schemas( SCHEMA_PATH )

    api = (
        Api([
            resource( 'taskseq', id=r'\d+', schema='taskseq#',
                list=taskseq.list,
                update=taskseq.update,
                resources=(
                    resource( 'schedule',
                        create=taskseq.schedule.post,
                        delete=taskseq.schedule.delete
                    ),  
                    # ... other sub-resources
                )
            ),
            not_found( errhandlers.not_found ),
        ], config = config
         , id = '1'
         , name = 'u'
         , schemas = schemas
        )
    )

    app = wsgi_app( route(api) )

    # route : Api -> (Request -> JSON Value)
    # wsgi_app : (Request -> JSON Value) -> environ -> start_response -> WSGI response

This one embraces the REST paradigm. Routing table is built via typical 
conventions using  api name + api id + resource name + { rules for CRUD routes }
and a custom regex for id format. Sub-resources inherit the matcher of
the parent 'read' route to build their own matchers. 

It also makes use of JSON Schema. The resources can specify a schema, which is
a JSON pointer into one of the schemas included in the Api, which will be used
appropriately for list and read (output schema), create and update (input).


    class Api:
        def __init__(self, resources, id, name=u'', prefix={}, schemas=[], config={})
            self.id = id
            self.name = name
            self.schemas = schemas
            self.config = config
            self.resources = resources
            self.templates = compile_url_templates(name, id)

        def match(self, method, url):
            match = self.templates.prefix(url)
            if match is None: 
                return self.not_found(method, url)
            else
                (_, rest) = match
                if method == u'GET' and rest == u'':
                    return ((), self.__call__)
                else:
                    try:
                        return next(self.match_resource(self, method, rest))
                    except StopIteration:
                        return self.not_found(method, url)

        def match_resource(self, method, url):
            for resource in self.resources:
                match = resource.match(method, url)
                if match is None:
                    continue
                else:
                    yield match

        def __call__(self, request, params, config):
            """ Do something clever like return a dictionary of resource routes etc. 
            """
            

    class Resource:
        def __init__(self, name, id=None, schema=None, 
                     list=None, read=None, create=None, update=None, delete=None, patch=None,
                     resources=[]):
            self.name = name
            self.id = id
            self.schema = schema
            self.list = list
            self.read = read
            self.create = create
            self.update = update
            self.delete = delete
            self.patch = patch
            self.resources = resources
            self.templates = compile_url_templates(name, id)

        def match(self, method, url):
            match = self.templates.prefix(url)
            if match is None: 
                return None
            else
                (params, rest) = match
                if rest == u'':
                    if method == u'GET':
                        return (params, self.list)
                    elif method == u'POST':
                        return (params, self.create)
                    else:
                        return None
                else:
                    single_match = self.templates.single(url)
                    if single_match is None:




--------------------------------------------------------------------------------
_3 Jan 2018_

I'm coming to the conclusion that Websockets have limited use for this 
application. The only thing they would be useful for is broadcasting changes
to schedules/queue status. But the rest of the app is standard request-response, 
which we're better off using HTTP for. We might as well just poll for changes 
to schedules/queue status, to start with, and avoid a lot of potential hassle
on the backend.


_2 Jan 2018_

## Back to the back end

So basically it seems we want to implement `POST /api/1/taskseq`. Create
sequence, independent of the account, etc, because we don't have any of that
structure set up yet. Doing that will at least force the dbase and ORM setup,
if not much else. Eventually we want to validate the incoming JSON, but really
it's not necessary since we are passing it through as a blob to the dbase.

The fun starts when we get the scheduling UI done, then we get to talk to the
apscheduler and hook up dramatiq etc. I think the aim should be to do the
simplest possible UI for scheduling. Run this every day at x time starting on y
date, something like that. We want to get on to the fun ASAP ;)

There is the question about using websockets instead of REST API however. I
think it is worth a little bit more investigation. The frontend seems no 
problem. The backend we have to look at since the apscheduler and rabbitmq 
communication not to mention the database would need to be async non-threaded 
most likely as well. Unless perhaps put rabbitmq and the database on separate
threads + async queues between them and the main thread or something. The
apscheduler has an asyncio interface.


--------------------------------------------------------------------------------
_1 Jan 2018_

## Modelling task sequences

Thus far, there is no real data structure difference between "Check for 
existence of files" (a blank form) and "Check for existence of files greater 
than 100MB modified within 1 hour before scheduled time". (In fact -- the 
beauty of generic parameters -- ultimately both of these are the same type as
"Run SAS program" or "Zip files", etc.)

In day to day use, users want to choose a template task and modify it as needed.
In other words -- selection from a menu or whatever means "I want a copy of
this to work from", for this particular _sequence_ I'm setting up.

I don't think they'd want to see every permutation of settings ever used in the
menu. Particularly with the SAS, zip, etc. jobs, they are all custom params.
Perhaps there is a choice to "Save these settings" as they go (i.e. not 
necessarily when 100% valid), and maybe "for this account only" etc. But that's
future finesse.

Let's say to begin with the template tasks are all fixed, not stored in the
dbase or anything, sidestepping the issue of dealing with validation functions.

The model of building these sequences of task forms could simply be:

    type alias TaskFormSequence =
        { active : Maybe Int
        , forms : List TaskForm   -- or Array
        }

Assuming they are editing one task at a time. 

Would be nice to have a drag and drop reorder UI here I think.

There may be other state belonging to the sequence such as whether the sequence
should be retried if it fails and up to how many times if so:

    type alias TaskFormSequence =
        { active : Maybe Int
        , maxRetries : Maybe Int
        , forms : List TaskForm
        }

A more fundamental question is thus far each of these things are lists of
anonymous parameters. At some point these bundles have to be hooked up to actual
known tasks, you know, so they can actually be run?  So here is really where
the difference comes in for these basic, fixed task templates (which other
templates may be based on): they each have to be tied to known tasks, which 
means really they should be typed themselves. 

But the most obvious thing to do seems 'too open'. You can't just call any
bundle of parameters valid for a SAS job.

    type TaskFormType
        = ConfirmFiles TaskForm
        | SAS TaskForm
        | Zip TaskForm
        | SFTP TaskForm
        | EmailNotif TaskForm

I suppose the way around it is to not export the constructors and only let
you use validation functions from the outside:

    confirmFilesTaskForm : TaskForm -> Result String TaskFormType
        -- check that it has all the param forms it needs
        -- i.e. checking id's of params and that they match the required type

It seems like a bunch of extra work, but it's the price we pay for generic
structures I suppose.

Another way might be to **only** provide the template tasks wrapped, ie.

    templateConfirmFilesTaskForm : TaskFormType

And provide a

    mapTaskForm : (TaskForm -> TaskForm) -> TaskFormType -> TaskFormType

to update the underlying generic params while maintaining the type wrapper.



--------------------------------------------------------------------------------
_30 Dec 2017_

## Is it possible to genericize task parameters for building the UI?

Say we don't want to have to model each task type statically. So adding a new 
task type doesn't require too much change to the UI, and down the line could
perhaps be de/serialized and loaded in from config, designed through its own
UI etc.

- 'Check for existence of and size/date modified of _files_ _in_', 
- 'Run SAS _program_ _in_ with _sas options_ expecting _log files_', 
- 'Zip _files_ _in_ _to_', 
- 'SFTP files _to server_ _with credentials_ _in folder_, retrying _times_',
- 'Notify by email using template _subject_ and _body_ _to/cc/bcc_'

Elm sketch:

    checkFilesTask = 
        { label : Just "Check that files exist"
        , params = 
            [ textParam "in directory" <| validateWith validDirectory
            , textParam "files named" <| validateWith validFileGlob
            , disabledParam <|
                relativeTimeParam "Modified" <| timeComparison After
            , disabledParam <|
                relativeSizeParam "File size is" <| sizeComparison GT
            ]
        }

    type alias Task = 
        { label : Maybe String
        , params : List TaskParam
        }

    type TaskParam
        = TextParam TextParamData
        | LongTextParam TextParamData    -- for textbox
        | SensitiveTextParam SensitiveTextParamData
        | NumberParam NumberParamData
        | RelativeSizeParam
            { label : Maybe String
            , id : String
            , quantity : NumberParamData
            , unit : SizeUnit
            , comparison : EnumParamData Order
            }
        | RelativeTimeParam
            { label : Maybe String
            , id : String
            , timeVar : EnumParamData TimeVar
            , quantity : NumberParamData
            , unit : EnumParamData TimeUnit 
            , comparison : EnumParamData TimeComparison
            }
        | OptionalParam 
            { enable : Bool
            , param : TaskParam
            }
        | GroupTextParam    -- for "sas options" and "to/cc/bcc"
            { label : Maybe String
            , id : String
            , params : List TextParamData
            }

    type TimeVar
        = SubmittedTime
        | ScheduledTime
        | LastQueuedTime

    type alias TextParamData =
        { label : Maybe String
        , id : String
        , width : Maybe Int
        , value : ValidationResult String
        , validate : String -> Result String String
        }

    type alias SensitiveTextParamData =
        { label : Maybe String
        , id : String
        , width : Maybe Int
        , value : ValidationResult String
        , validate : String -> Result String String
        , showText : Bool
        }

    type alias NumberParamData =
        { label : Maybe String
        , id : String
        , width : Maybe Int
        , value : ValidationResult Int
        , min : Maybe Int
        , max : Maybe Int
        , step : Maybe Int
        , validate : String -> Result String Int
        }

    type alias EnumParamData enum =
        { label : Maybe String
        , id : String
        , width : Maybe Int    
        , value : Maybe enum
        , options : List enum
        , toString : enum -> String
        }

    {-- This is the 'valid' state that gets encoded --}

    type alias TaskValue = List (String, TaskParamValue)

    type TaskParamValue
       = TextValue String
       | NumberValue Int
       | RelativeTimeValue
           { timeVar : TimeVar
           , quantity : Int
           , unit : TimeUnit
           , comparison : TimeComparison
           }
       | RelativeSizeValue
           { quantity : Int
           , unit : SizeUnit
           , comparison : Order
           }
       | OptionalValue (Maybe TaskParamValue)
       | GroupTextValue (List String)


    validate : Task -> Result String TaskValue
        -- Note: simple params have their own `validate`. Complex params like
        -- RelativeTimeParam have a hard-coded validation function based on if
        -- all the necessary pieces are set.


    updateTaskFromValue : TaskValue -> Task -> Task
        -- we need this function to inject a valid TaskValue into a Task (Form)
        -- for editing existing tasks, e.g.


## Basic database schema

    CREATE TABLE data_type IF NOT EXISTS
        ( id INTEGER PRIMARY KEY AUTOINCREMENT
        , name TEXT NOT NULL
        )
    ;
    CREATE INDEX IF NOT EXISTS idx_data_type_name ON data_type ( name );
    
    CREATE TABLE task_type IF NOT EXISTS
        ( id INTEGER PRIMARY KEY AUTOINCREMENT
        , name TEXT NOT NULL
        )
    ;
    CREATE INDEX IF NOT EXISTS idx_task_type_name ON task_type ( name );

    CREATE TABLE account IF NOT EXISTS
        ( id INTEGER PRIMARY KEY AUTOINCREMENT
        , account TEXT NOT NULL
        , protocol TEXT NOT NULL
        , client TEXT NOT NULL
        , data_type_id INTEGER NOT NULL
        , cmis_id TEXT
        , gcal_id TEXT
        , CONSTRAINT unq_account 
            UNIQUE ( account, protocol, client, data_type_id ) 
                ON CONFLICT ABORT
        , CONSTRAINT fk_account_data_type 
            FOREIGN KEY ( data_type_id ) REFERENCES data_type ( id )
                ON DELETE RESTRICT
        )
    ;
    CREATE INDEX IF NOT EXISTS idx_account_account ON account ( account );
    CREATE INDEX IF NOT EXISTS idx_account_data_type_id ON account ( data_type_id );
    
    CREATE TABLE task_sequence IF NOT EXISTS
        ( id INTEGER PRIMARY KEY AUTOINCREMENT
        , name TEXT NOT NULL
        , source_seq_id INTEGER
        , account_id INTEGER
        , CONSTRAINT fk_task_sequence_source
            FOREIGN KEY ( source_seq_id ) REFERENCES task_sequence ( id )
                ON DELETE SET NULL
        , CONSTRAINT fk_task_sequence_account
            FOREIGN KEY ( account_id ) REFERENCES account ( id )
                ON DELETE SET NULL
        )
    ;
    CREATE INDEX IF NOT EXISTS idx_task_sequence_name ON task_sequence ( name );
    CREATE INDEX IF NOT EXISTS idx_task_sequence_source_seq_id 
        ON task_sequence ( source_seq_id ) WHERE source_seq_id NOT NULL;
    CREATE INDEX IF NOT EXISTS idx_task_sequence_account_id 
        ON task_sequence ( account_id ) WHERE account_id NOT NULL;

    CREATE TABLE task IF NOT EXISTS
        ( id INTEGER PRIMARY KEY AUTOINCREMENT
        , task_type_id INTEGER NOT NULL
        , task_seq_id INTEGER NOT NULL
        , task_seq_order INTEGER NOT NULL
        , params BLOB
        , CONSTRAINT fk_task_task_type
            FOREIGN KEY ( task_id ) REFERENCES task_type ( id )
                ON DELETE RESTRICT
        , CONSTRAINT fk_task_task_sequence
            FOREIGN KEY ( task_seq_id ) REFERENCES task_sequence ( id )
                ON DELETE CASCADE
        )
    ;
    CREATE INDEX IF NOT EXISTS idx_task_task_type_id ON task ( task_type_id );
    CREATE INDEX IF NOT EXISTS idx_task_task_seq_id ON task ( task_seq_id );
    CREATE INDEX IF NOT EXISTS idx_task_task_seq_order ON task ( task_seq_order );


Scheduling state is all handled in the `apschedule_jobs` table, which I'm kind 
of thinking should be in its own sqlite db, even though that means separate 
sqlalchemy connections; but that shouldn't matter because apscheduler handles
the connection internally. The process of scheduling a task sequence is:

1. Select all the tasks in the seq from the database in order
2. Map these rows to actors >> messages, injecting the task_id and task_seq_id
   somewhere (options?) for tracking purposes.
3. Reduce the messages to a sequence for enqueuing to dramatiq.
4. `scheduler.add_job`


--------------------------------------------------------------------------------
_29 Dec 2017_

An API sketch, at least for the apscheduler/database side of things.

  Endpoint                                         | Description
---------------------------------------------------|---------------------------
  GET /api/1/account/<account>                     | Account + task sequences
  GET /api/1/taskseq/<taskseq>                     | Task sequence + schedule(s)
  GET /api/1/taskseq/<taskseq>/task/<task>         | Task
                                                   |
  POST /api/1/account                              | Create account
  POST /api/1/taskseq                              | Create sequence**
                                                   |
  POST /api/1/account/<account>/taskseq            | Create sequence under account**
  PUT  /api/1/account/<account>/taskseq/<taskseq>  | Copy sequence under account**
                                                   |
  PUT /api/1/taskseq/<taskseq>                     | Update sequence***
  DELETE /api/1/taskseq/<taskseq>                  | Delete sequence
                                                   |
  DELETE /api/1/taskseq/<taskseq>/task/<task>      | Delete task from sequence
                                                   |
  POST /api/1/taskseq/<taskseq>/schedule           | Add a schedule for sequence
  DELETE /api/1/taskseq/<taskseq>/schedule         | Delete all schedules for sequence
  DELETE /api/1/taskseq/<taskseq>/schedule/<sched> | Delete one schedule for sequence
  PUT /api/1/taskseq/<taskseq>/schedule/on         | Resume all schedules for sequence
  PUT /api/1/taskseq/<taskseq>/schedule/off        | Pause all schedules for sequence
  PUT /api/1/taskseq/<taskseq>/schedule/<sched>/on | Resume one schedule for sequence
  PUT /api/1/taskseq/<taskseq>/schedule/<sched>/off| Pause one schedule for sequence
  

** I think we should leave open that we might want 'model sequences'. Note the
'copy sequence under account' endpoint.

*** I think the entire sequence should be submitted when a change is made to
any task or the ordering in the sequence. Note the sequence ID is unique whether
it's a model or attached to an account, thus we can drop the account ID from
the URL.


More important than all this REST malarky though is sorting out the data types.
There are quite a lot of formats it has to pass through. And the task params
are quite complex. I am thinking we could use JSON Schema up front here.


--------------------------------------------------------------------------------
_29 Dec 2017_

## Switching gears

Rather than clean up my job queue hackery, I think the point is demonstrated it
can do all I need it to do. What isn't so clear is how users are going to
interact with the system. This means:

  1. Defining sequences of tasks;
  2. Scheduling them;
  3. Monitoring job status.

My thought is to present relatively high level concepts in terms of tasks.
While not completely abstracting away e.g. file system conventions.

- 'Check for existence of and size/date modified of _files_ _in_', 
- 'Run SAS _program_ _in_ with _sas options_ expecting _log files_', 
- 'Zip _files_ _in_ _to_', 
- 'SFTP files _to server_ _with credentials_ _in folder_, retrying _times_',
- 'Notify by email using template _subject_ and _body_ _to/cc/bcc_'

Sequences belong to accounts, i.e. client-protocol-datatypes. One account can
have more than one sequence scheduled.

Re scheduling, Bogdan's suggestion is to use `apscheduler` to trigger putting
tasks on the task queue. Looking at it, it does have key things I need such as
ability to get jobs due before a timepoint; updating, pausing, resuming, and 
removing a particular job.  And it can use an SQLite backend.

Re monitoring, we have then two sources for the state of the job, apscheduler
and dramatiq. We can get a list of 'tasks yet to be queued', say for the next
two days. But queued, processing, what step in the sequence is processing, 
success, and failure task state has to be gotten out of dramatiq. I think
we can use a common 'job group ID' to pass through both, which will make things 
easier.

The key in getting internal state out of dramatiq is to make use of 
middleware to write state out to a database. I'm a bit nervous about using
SQLite here, although perhaps if we piped updates back through a single queue.


## The game of what does D. want

All of the above is a bit of a tall order to get done "by January". I feel like
it is either the task definition UI, or task monitoring UI, that is possible,
but not both.

I think it's more realistic to work on the definition/scheduling UI. It's the
more complex interface. The only thing I can think of as an alternative
would be some kind of config/csv file they edit to define all the options, but
then that's a bunch of work too just to be thrown away later, and is confusing
to demo that if that's not even related to the real UI. On the other hand, it's
easier to do a quick listing for a dummy monitoring UI, even if it's two lists 
(scheduled vs queued/processing/completed jobs), and doesn't have e.g. pause,
resume, cancel, run now, etc. interactions.

## The need for a separate server

I think I can ask for an in-network *nix VM to run the frontend. I'm a bit 
worried with RabbitMQ + dramatiq + apscheduler + SFTP client + SAS, to add
nginx + flask app possibly with websockets,... that's getting to be a lot
of CPU and I/O. For a little Windows 7 box. And nginx is limited on Windows.



--------------------------------------------------------------------------------
_28 Dec 2017_

## The long slog, and a shortcut

Copying and adjusting all the mechanics of Dramatiq workers to fit my needs is 
indeed painful.

Another way, in the interests of getting something ready for the demo, may be 
using Dramatiq middleware. There is a middleware called "callbacks" that allows
very simple chaining: 

    class Callbacks(Middleware):
        """Middleware that lets you chain success and failure callbacks
        onto Actors.
        """

        @property
        def actor_options(self):
            return {
                "on_failure",
                "on_success",
            }

        def after_process_message(self, broker, message, *, result=None, exception=None):
            if exception is None:
                actor_name = message.options.get("on_success")
                if actor_name:
                    actor = broker.get_actor(actor_name)
                    actor.send(message.asdict(), result)

            else:
                actor_name = message.options.get("on_failure")
                if actor_name:
                    actor = broker.get_actor(actor_name)
                    actor.send(message.asdict(), {
                        "type": type(exception).__name__,
                        "message": str(exception),
                    })

                    
The problem from my point of view is that the only input to each success callback
is the result of the last job. True chaining rather than sequencing. And it's
not possible to specify the args for the successor function in `actor_options`;
these are statically defined in the actor function decorator. Likewise, dynamic
sequences are hard to define (at least using decorators).

If we had a message() method on Actor, which generates the message but doesn't 
enqueue it, then we could:

    seq = sequence( actor_function.message(some_args), another_actor.message(other_args) )
    broker.enqueue( seq )
    
    def sequence( messages, on_failure=None, result_key=None ):
        def _chain(msg, prevmsg):
            if prevmsg is None:
                msg.options['on_failure'] = on_failure
                return msg
            prevmsg.options['on_success'] = msg
            prevmsg.options['on_failure'] = on_failure
            prevmsg.options['result_key'] = result_key
            return prevmsg
        return reduce( _chain, msgs.__reversed__(), None )

        
And in a middleware that processes `on_success` as _messages_ rather than actor names:

        def after_process_message(self, broker, message, *, result=None, exception=None):
            if exception is None:
                next_msg = message.options.get("on_success")
                if next_msg:
                    result_key = message.options.get("result_key")
                    if result_key:
                        next_msg.kwargs[result_key] = result
                    broker.enqueue(next_msg)
            else:
                actor_name = message.options.get("on_failure")
                if actor_name:
                    actor = broker.get_actor(actor_name)
                    actor.send(message.asdict(), {
                        "type": type(exception).__name__,
                        "message": str(exception),
                    })

                    
In retrospect I wonder if Bogdan wishes he had made the Message class public and
extensible via subclassing instead of by "dict free for all". Ah the joys of
dynamic middleware schemes.

Anyway. The short term plan looks like this:

  1. Monkey patch dramatiq.actor.Actor to add `message`.
     `Actor.message = monkey_patch_function.__get__(Actor)`
  2. Add `sequence`, and 'Sequence' middleware above
  3. Try running some simple sequence on linux using the dramatiq workers etc.
  4. Launch a single worker process on Windows. See if SIGINT/SIGTERM work.

UPDATE: 1, 2, and 4 work. Could not get linux VM to talk to RabbitMQ on host,
but no matter. The damn hacked together thing works on Windows!

Unfortunately, it _does_ have to be a stopgap. GNU Affero is pretty clear. So
the long slog is sometime in my future ;)  But looking at it now was useful. I
think baking in the extra resources for dealing with failures/successes/events
on different RabbitMQ queues is overkill, Bogdan is right. I don't particularly
like this middleware free-for-all, but at least it doesn't suck up resources and
make it even more insane to follow the code.


--------------------------------------------------------------------------------
_26 Dec 2017_

## Considering RabbitMQ again, continued

### Practically speaking part 3

Bundling up the main process that spawns workers doesn't seem like the biggest 
priority. I think we could just spin up multiple worker processes manually
(or in fact just one is probably all that's needed for the demo). So I think
we should focus on what the worker process should look like, given the necessary
differences from Dramatiq -- both technical (running on Windows) and in terms
of requirements (running sequences AKA chaining AKA simple workflows).

## Binding the queue handlers from the app

Except for (1) the dynamic binding to RabbitMQ queues, and (2) the way its 
decorators encourage global brokers, Dramatiq's technique for declaring queue
functions seems generally good for my needs and I see no reason not to copy it,
not using the name 'actor' though, which is confusing.  Something like:

    @broker.work( priority=10 )   # sets WORK queue callback in broker for task type 'sas_job', but does NOT declare queue
    def sas_job( params ):
        ....
        
    sas_job.send(...)    # publish to broker-configured WORK exchange
    
Make the decorator a method of an actual broker; which means you have to 
initialize the broker and import it into your task module; this happens both on 
your app side and on the worker process side.  So your worker process doesn't
get to specify what broker to use; your tasks/app do. Less frameworky.

And the broker initialization is where the queues and exchanges are specified
and declared, perhaps loading from a config file.

Then for the ERROR and RESULT queues ( FAIL and SUCCEED ?)
         
    @broker.work_failed
    def handle_error_result( result_msg ):
        ....
        
    @broker.work_succeeded
    def handle_result( result_msg ):
        ....

And EVENT -- (although should we have separate queues per event if we 
are going to allow filtering like this, even if that means an extra consumer
thread per listened-to event?) :

    @broker.after_work_delayed
    def log_after_work_delayed( event_msg ):
        ....
        
    @broker.on_events( broker.BEFORE_WORK, broker.AFTER_WORK )
    ....
    

    
--------------------------------------------------------------------------------
_24 Dec 2017_

## Considering RabbitMQ again, continued

### Practically speaking part 1

If I can redo Dramatiq's process handling to work on Windows, for the purposes
of _January demo_ I could just use Dramatiq. Although sequencing tasks might be
tricky.


### Practically speaking part 2

Regarding my concept below. I think we can manage a great simplification 
regarding queues. Recall that each queue does cost a thread per worker process
to consume. There is no advantage to using more than one queue to process tasks;
they are all processed the same way.

On the other hand, publishing to the results and/or error queues you may very
well want to go through a fanout exchange. The processing is a little different
as well.

What I am thinking is the broker client manages a fixed set of queues: let's 
call them WORK, RESULT, ERROR, and EVENT. When the broker gets set up on either
the producer or consumer side, these queues get configured: their names,
exchanges they are bound to and other properties of the queues and exchanges.

The producer (your app) writes to WORK. The consumer (worker processes) read 
from WORK, and write to WORK, RESULT, ERROR, and EVENT; other worker processes 
optionally read from RESULT, ERROR, and EVENT.

For each queue, worker processes have 1 consumer thread and n worker threads,
and overall there are n total producer threads.

So assuming all 4 queues are consumed, that's 4 + 5n threads. An internal 
queue shuttles work from the consumer thread to worker threads ("incoming"),
and from all the worker threads to producer threads ("outgoing"). 

Maybe we can simplify to WORK, RESULT, and EVENT?

    WORK:    consumer  --- inq ---->  { worker1, worker2, worker3, ... } \
    RESULT:  consumer  --- inq ---->  { worker1, worker2, worker3, ... }  --- outq ---> { producer1, producer2, producer3, ... }
    EVENT:   consumer  --- inq ---->  { worker1, worker2, worker3, ... } /
    

_24 Dec 2017_

## Considering RabbitMQ again, continued

What if messages are basically commands -- serialized union types -- and we 
have a kind of router that we pipe messages through, instead of actors?

We could define queues separately from the router via some kind of config. That
way we don't run into the timing problem with consuming queues before declaring
them. The queue data is independent of the definition of the routes and can
be accessed by both worker processes (to create 1 consumer thread per queue) and 
the broker (to declare the queues).


    SASCmd = Record('SASCmd', { 
      'account': unicode,
      'datatype': unicode,
      ...
    })
    
    Routes = [ SASCmd, FTPCmd ]
    
    router = match( Routes, { 
      SASCmd : run_sas ,
      FTPCmd : run_ftp
    })

On the one hand it's nice to have the routing completely independent of the 
queue layer... on the other hand it's a bit weird to think you could have any
kind of message appearing on any queue. And what defines what message type goes 
on what queue anyway?

Let's say instead the router works like this: each queue has its own union type. 
When a message comes in on a particular queue then it runs it against a router 
(or simple handler) for that queue. 

In the easiest case it's a dict:

    route_sas = match( SASCmd, {
       SimpleSASCmd : run_sas_simple,
       ComplexSASCmd : run_sas_complex
    })
    
    def run_ftp( ftp_cmd ):
      ....
      
    queue_handlers = {
      'ert.sas' : route_sas,
      'ert.ftp' : run_ftp
    }

Following Dramatiq's lead, there are at least _delay_ and _end-delay_ queues for 
each base queue, to manage delayed tasks. (Actually though I'm not sure if 
end-delays are necessary; couldn't the deadletter queue just be the main queue 
again?)

### What about chained tasks?

Here arises the question, what about publishing to queues from workers? How is
that done? Dramatiq doesn't do this. Perhaps a separate in-memory queue
that the worker thread publishes to?  That then a 'publisher' thread picks up
and asks the broker to write to the actual RabbitMQ queue?

But before those mechanics, the question is are chained tasks special cases that
the 'worker runtimes' handle, or is it up to the user-defined handlers?

Here's more or less how it would work in the worker thread (putting results and
errors onto a result_queue) for a single task.

    def process_message(self, message):
        try:
            res = None
            if not message.failed():
                res = self.run_command(message.queue_name, message.command)
                
                self.result_queue.put( Success(message, res) )

        except NoHandler as e:
            self.logger.warning("Message %s has no handler.", message)
            self.result_queue.put( Failure(message, e) )
            
        except SkipMessage as e:
            self.logger.warning("Message %s was skipped.", message)
            self.result_queue.put( Failure(message, e) )

        except BaseException as e:
            self.logger.warning("Failed to process message %s with unhandled exception.", message, exc_info=True)
            self.result_queue.put( Failure(message, e, exc_info=True) )

        finally:
            self.consumers[message.queue_name].post_process_message(message)
            self.work_queue.task_done()

            
Chained tasks in our context are a bit funny. They don't really care about the
results of the previous task, just whether or not they succeeded or not. Also,
chains are different so success in one job doesn't automatically mean the same
next job should be kicked off in every case. The top-level command has to
include all the information about the entire chain of commands that are to
follow.

So I think commands are

    type Command = SAS SASParams | FTP FTPParams | Chain (List Command)

and messages are

    type alias Message = 
        { queue_name : String 
        , ... other metadata ... 
        , command : Command 
        }

Then perhaps the internal `run_command` could return not only the result but
also the next command to be run in sequence, if any. Then that next command 
(if present) could be pushed onto a `next_queue`, to be ultimately published to
RabbitMQ:

    def process_message(self, message):
        try:
            res = None
            if not message.failed():
                ( res, next_cmd ) = self.run_command(message.queue_name, message.command)
                
                self.result_queue.put( Success(message, res) )
                if next_cmd:
                  self.next_queue.put( next_cmd )

Except, except: we have to know what queue to route the next command to. We 
really need to push a _message_, not a _command_.  And our router doesn't know
how to go from commands to queue names. Maybe there is not a one to one mapping.
So we need this to be user supplied. 

So maybe?

    type Command = SAS SASParams | FTP FTPParams | Sequence (List (String, Command))
        
        
    def process_message(self, message):
        try:
            res = None
            if not message.failed():
                ( res, next_msg ) = self.run_command(message)
                
                self.result_queue.put( Success(message, res) )
                if next_cmd:
                    self.next_queue.put( next_msg )

To avoid the extra overhead of another queue, maybe we can have one 
`outgoing_queue`:

    def process_message(self, message):
        try:
            res = None
            if not message.failed():
                ( res, next_msg ) = self.run_command(message)
                
                self.outgoing_queue.put( Success(message, res) )
                if next_msg:
                    self.outgoing_queue.put( next_msg )

                    
We could even cut down further on the messaging by sending the next message
_with_ the success, i.e. `type Result e a = Error e | Success a Message`. Not 
sure about that though, as it would require a handler to do something both with
the result and with the next Message. Better to just fire it off independently 
I think.


### Sidebar on publishing

The setup I'm suggesting is quite different from Dramatiq's, which kind of
allow user code to do double duty -- when imported into the worker processes
it sets up the consumers and when imported from your app it gives you `send` and
`send_with_options` for publishing. Very elegant.

My point is some of the internal complexity this brings in can be avoided by 
splitting the consumer and publisher definitions, and just using common data 
structures on both the worker and the app sides, more or less.

What we need for the publishing side is a way of converting commands into
messages to submit. And it seems clear this has much the same signature as
`send_with_options`, but with the command as subject instead of as a method on
the actor object.

Also I think the `queue_name` should be mandatory given we are routing internally
using the queue name.

So really fundamentally, publishing is wrapping up `(queue_name, command)` 
(plus a few more options, priority, etc.) in a message wrapper.


### Handling success and failure

The other simplification Dramatiq makes is to always use the default (direct)
exchange to which all the queues are hooked up (queues names are used as the 
routing key). Makes sense given other message brokers don't have this distinct
concept of an exchange.

But we may be able to make use of several exchanges, for instance for success and 
failure results; their queues could be bound to separate 'success' and 
'failure' exchanges.

Really though the key is that errors and successes should all go to one queue
to be dealt with centrally I think.

The other thing is that 'results commands' need to be special cased so they
don't cause an infinite chain -- we don't need to hear about the results of
results.


    def process_message(self, message):
        try:
            res = None
            if not message.failed():
                ( res, next_msg ) = self.run_command(message)
                
                # logging here is ok even for results messages
                
                if not message.is_result():
                    self.outgoing_queue.put( Success(message, res) )
                    
                    if next_msg:
                        self.outgoing_queue.put( next_msg )




--------------------------------------------------------------------------------
_23 Dec 2017_

## Considering RabbitMQ again

OMG, I came across Dramatiq again, I'd forgotten about it. Bogdan who I know 
from Elm world. It's so perfect!  Too bad it's (a) Unix only -- uses fork; and
(b) costs $2000/year for close-source license (good for Bogdan, but won't fly
in my case).

So I propose studying its source code to understand how to use pika in a
multithreaded/process context. From what I understand, 

  - Similar to Sidekiq, you launch a process that manages a set of worker
    processes for consuming messages;
    
  - Messages are consumed off a dedicated thread with an inactivity timeout of
    1s. "Whenever a consumer gets a message from Rabbit, it puts it on a shared 
    in-memory work queue where it is grabbed and processed by any of n worker 
    threads. Once the worker thread is done processing the message, it gets put 
    on an 'acks' queue that belongs to the consumer. When the consumer hits the 
    inactivity timeout it acks any pending messages off its queue and then waits
    for new messages from RMQ."  (https://github.com/pika/pika/issues/892)

In other words, it's a process controlling a number of worker processes, each of 
which have (1) a number of threads equal to the number of subscribed queues, 
each of which are consuming messages, and putting them on (2) a common 
thread-safe in-memory priority queue, which (3) a number of threads are 
consuming and running in the context of an 'actor' class, and then (4) pushing
the results onto another in-memory queue on the consuming thread, which queue is
(5) consumed after processing messages pauses (the 'inactivity timeout'), and 
converted to acks or nacks in the consumer thread.

The bulk of the shuffling back and forth between RabbitMQ and local queues and
back is done in 
https://github.com/Bogdanp/dramatiq/blob/master/dramatiq/worker.py

(Apparently, issue with this is if you are setting the QoS, which is the max 
number of messages that can be consumed at once (before an (n)ack gets returned 
for one of them), on all threads. It ends up waiting for this relatively long 
inactivity timeout before processing the ack's, instead of right away. Bogdan
put some hack in place to work around this. But that really doesn't apply to us.)

The stop() method on these workers is, as you can imagine, quite complex, but
not too bad. The only thing to do besides joining the consumer and worker 
threads is processing any remaining acks on the local queue in each consumer.
(Surprisingly, I see that this can be done after joining the threads!)  Any
remaining un-acked tasks on the local work queue don't have to be requeued in
RabbitMQ.

Re. fork, I believe multiprocessing.Process will give us what we need.


I believe it's possible to copy most of the basic queue/thread/process 
mechanics of Dramatiq for our needs. 

A lot of the bells and whistles we can drop, of course, but in particular the
observables-based 'middleware' seems unnecessary, which is also used for binding 
consumer threads to queues after they are declared in the broker (which I can't
even follow how it works exactly).

The interactions between Actor, Broker, and Worker could be rethought in
my opinion, especially given we don't need a generic Broker.



    
--------------------------------------------------------------------------------
_22 Dec 2017_

## Considering Huey again

It is _very_ easy to get lost in the thickets of concurrency models -- python
has _many_. Let's remind ourselves what is actually needed here.

1. We are not talking about high volume, thousands of tasks per second 
scenarios. Actually it is quite low volume, even if every data send were to be
automated.

2. There are fundamentally two kinds of tasks: running an external process
(SAS, e.g.), and running an in-process, blocking, routine in python (e.g., SFTP
client).

3. We want to limit the number of subprocesses that can run in parallel to 
avoid taking down the server.

4. Users need to be able to schedule tasks to kick off at a specified time of
day. Preferrably, this would be done at the 'recurrence rule' level, i.e. once
for the whole series. But it should also be possible to adjust/delete individual
scheduled tasks.

5. 'Tasks' are in fact chains of tasks. For example: first run the gensas, then 
run the encoding, then zip the output, then FTP. 


Regarding 1-3, my investigations led me to `multiprocessing.pool.ThreadPool`, 
which let you specify a limited number of threads within which subprocesses can
run, as outlined here: 

https://stackoverflow.com/questions/26774781/python-multiple-subprocess-with-a-pool-queue-recover-output-as-soon-as-one-finis#26783779

But my sense is that starting at such a low level, it would be very difficult to
build up to other application-level needs, e.g. task retry, scheduling, 
monitoring, etc.

Also, Huey has its own thread pool implementation, which seems fine. 

Regarding task chaining. It certainly seems possible to do something like
this in Huey:

    @huey.task()
    def sas_task( params, next_tasks=[]) :
        ...
        if next_tasks:
            (next_task, remaining_tasks) = next_tasks
            next_task(remaining_tasks)
 
Assuming that these tasks are partially-applied with their params at some top
level.  Or perhaps this ugliness is easier:

    @huey.task()
    def sequence( tasks_and_params ):
        for (task, params) in tasks_and_params:
            res = task(**params)
            res(blocking=True)


Regarding scheduling and recurrence. Here there are two problems:

1. Huey expects that each task function corresponds to one periodic schedule, 
whereas we have multiple instances of task functions running on different
schedules.

2. As discussed, we already have recurrence state (though incomplete) in the
datasends calendar; it's unclear how this can be synced with the Huey scheduler.


It is possible to register periodic tasks dynamically in Huey, e.g.:

    # Note trap tasks and params in closure because periodic tasks can't have arguments themselves
    for (period, tasks_and_params) in datasends:
      huey.periodic_task( rrule(period) )( lambda : sequence(tasks_and_params) )

      
Also, we can identify tasks by their ID, either in the web app or in another
scheduled task. So I *think* we can use `revoke_by_id`, `restore_by_id` to manage 
changes to periodic tasks.   

BUT: I am not sure I want to use periodic tasks -- for one thing, they don't
support retries.


This brings us to the question of recurrence state. Really the first question is
for the users, which might be why I'm having such trouble. The first step is
going to be manual for now: generating the extract files. So do you want to

  1. Specify a particular time of day the extracts will be ready, once, or
  2. Click a button to indicate it's ready, each time ?
  
And if 1, what happens if you want to say "it's delayed by an hour, but just 
for today's send" ?

(Alternatively, what if we just set the retries to some large number with a 
half-hour delay, so it covers the whole day?)

This is how I imagine it working. They do enter a time of day, as a 'failsafe'.
But within the web app, they can choose to 'run now' / run at a different time.

The second question then is how to sync the calendar with the scheduler. My 
thought is to poll the calendar for a limited date range -- say today through
tomorrow -- filtering on events that are marked as 'auto delivery' or whatever.
These are compared against a list of scheduled, running and completed tasks; any
not on these lists are then scheduled. (You can build lists of running and 
completed tasks via the `pre_execute` and `post_execute` hooks; a list
of currently scheduled tasks is available through `storage.enqueued_items`; tho
none of these are very efficient in Huey, nor any other persistent queue that
I know of.)  I wish there was a `schedule` hook, or that the event listener
stuff worked for SQLite.


But when it syncs the calendar with the scheduler, what tells the scheduler what 
to run, and how to run it?  Should we even consider adding all of that into the
calendar app?

Another idea, then. Let's say the scheduler app defines all the state needed and
there is no polling of the calendar. The user setting up the automation enters
in the client, account, protocol, data type, etc. There is a lookup
against some slice of the calendar data. (Perhaps they enter the 'next due date' 
to use as a filter?)  They can also then get the RRULE from the calendar. They
then enter e.g. time of day to run, and configure the sequence of tasks to run.
All of this state belongs to the scheduler app. Perhaps there is a "refresh from
calendar" in case of changes to the RRULE -- the only bit of state that
is needed from the calendar.  One-off changes in schedule, cancellations, etc.
can just be dealt with by manually cancelling/running them in the scheduler app.
(Potentially I could get these as EXDATEs, but probably not worth the
effort, I don't think GCal provides these as such).


My view of Huey is it almost gives us what we want, but is a bit limited, esp.
the SQLite backend.

  - Would need to add error storage to SQLite 
  - Would need to add a 'rrule' helper function similar to 'crontab', if we use
    the RRULE from the calendar / periodic tasks
  - Inability of periodic tasks to retry
  - Schedule data is opaque
  - Task status is relatively opaque
  - Nowhere near the flexibility of RabbitMQ


I am considering a RabbitMQ based solution that would have an external
scheduler. I.e. poll defined tasks for the next scheduled run, if any 'waiting'
tasks with a next schedule earlier than now, then publish them to a run queue.
The main question in my mind is which RabbitMQ library is best to use 
considering we are running long-running subprocesses; and is there a way to
limit consumption on a queue to n tasks at a time, etc.


--------------------------------------------------------------------------------
_6 Dec 2017_

As great as Huey seems, the sqlite backend is not as fully featured as Redis.
In particular it doesn't have the event listeners (which the redis backend
implements quite easily with pubsub). So monitoring would be more of a 
challenge. And using redis on windows seems quite risky; running redis 
externally on a *nix VM is possible if we could swing that.

The alternative I'm looking at now is RabbitMQ which is well supported on 
Windows. 

  - There is a task queue solution which is not celery :) built on
    RabbitMQ, called Kuyruk. It seems actively maintained. Windows is not
    officially supported, but I think the only thing to deal with there is
    disabling signals. Looking through the issues I am not very convinced the
    author is that experienced with building these things though.

  - Potentially a RabbitMQ backend could be added to Huey. It looks like there
    was one pre-1.0.0, but then it was removed with new features coming in to
    the redis backend.

  - What do these higher-level solutions provide exactly? The point of AMQP is
    that it provides the basic functionality needed to construct application-
    specific task queues. Perhaps the basic driver is enough?


Re. scheduling. AMQP does not really have a mechanism for delaying message
consumption, that I can see, unless you count a TTL hack (use temporary
queues with TTL, and only process the message when it expires and hits the
dead-letter queue, which can be specified).

See:
https://www.javacodegeeks.com/2012/04/rabbitmq-scheduled-message-delivery.html


Alternatively -- and this relates to the "unknown" problem below about syncing
the Google calendar with the scheduler -- we could avoid having a scheduler at
all. The calendar provides the schedule _data_, yes, but a polling process 
could go out to the calendar and check for any _due but not yet queued_,
queue them up for immediate execution, and update the calendar. Sounds a bit 
fragile, but possibly it could work.

Again -- unknowns. Push them off. In January we could simply have a manual
trigger.


## A worklist of sorts

The key thing is not to get bogged down in AMQP land.

- confirm basic 'task queue' operation with Pika async (Select) adapter, and
  with long-running subprocesses on separate threads

After that, let's move on to the actual stuff, eh?  The SAS job controller and
the FTP.



--------------------------------------------------------------------------------
_5 Dec 2017_

## Initial sketch of the main pieces involved.

  Infrastructure
    - Persistent task queue
    - Database for delivery-spec data and queue reporting backend
    - Mail server (use ERT's? or service like mailgun?)
  
  Backend
    - Enqueue SAS jobs*
    - Run SAS jobs in subprocess (worker)
    - Run FTP jobs (worker)
    - Run email notification jobs (worker)

  Frontend
    - Interface for specifying delivery specs (FTP settings, email contacts,
      compression settings) -- add to datasends app?**
    - Interface for setting SAS jobs schedule*
    - Monitoring interface organized around study (individual job within study)


### Persistent queue

The first thing to get out of the way is that I am very wary of using Google 
pull queues. I would have to deal with two fundamental issues to consider it:

  1. A non-polling mechanism for triggering pulls. Or at least, a polling 
     mechanism that doesn't require setting up OS-level tasks for each queue.

  2. More accurate/transparent task status querying. Perhaps the new API 
     improves this.

Also we have to consider the complexity, and potential for API flux. It is still
beta.

#### What other options?  

FYI Amazon's and Microsoft's cloud queues do have "long polling" for receiving
tasks. At least with Microsoft's, it seems this can be a comet-style,
indefinite connection. (I wonder why these queues never use a web hook style
mechanism? Anyway that wouldn't work for me of course.)  But there are all the
same complexities dealing with cloud services.

#### What about a local queue?

The standard queue on Windows is MSMQ. But that commits us to .NET. (OK, you 
can interface with it through a COM wrapper in python etc., but it is a bit
of a hack). I do want to pick up F#, but this does not seem the right
opportunity for that!

Which brings us to queues with python or ruby or nodejs bindings, basically,
that also are going to run on Windows. Plenty of them are built on redis, but
redis for windows is basically abandoned. (You can run it in the new ubuntu
shell in Windows 10, but SAS 9.2 doesn't run on Windows 10). The most popular
python one -- celery -- also runs on RabbitMQ, which could be installed on
Windows.  But the complexity of both celery and RabbitMQ are a bit daunting.

Casting around for other options, I came across Huey, which is quite nice
looking, relatively recent and actively maintained. Has an option for a sqlite
backend instead of redis, which is interesting. Has event hooks for building
monitoring applications. 


### Unknowns

What is the best way to trigger a SAS job?  I don't really know. Two options
come to mind:

  1. When an extract file gets dropped in a watched folder
  2. According to a schedule provided by DMs, perhaps via the datasends calendar

Some of this depends on the extract process. I prefer the second option, 
because it lets them schedule things in advance, and thus allows us to see
the upcoming workload. But it does come with some complexity, if we are talking
about using the datasends calendar.

First of all, it's a bit awkward to access the calendar from an app running on
a local server. You'd have to use a service account with domain-wide
delegation or whatever, since the google account context won't be present.
(But let's assume this is possible, because the workarounds are horrendous.)

Let's say there's some kind of flag that indicates 'this is one I want to have
automatically run'. At the _series_ level. So the app queries on any individual
tasks with this flag set in the next (week? 2 weeks? month?). 

But here we end up with a serious problem regarding state. Once scheduled in
Huey (or any queue solution really), it is opaque. How do we cancel / 
reschedule?  How do we keep the calendar in sync with the scheduler?

I don't know, I suspect it's a hard problem, but the important thing to realize
is _I don't have to solve it by January_. We only have to set up one, or a few,
studies, by that time. The integration with the Google calendar does not have
to happen before then.



--------------------------------------------------------------------------------
_4 Dec 2017_

I had a conversation with R.. As usual, more questions than answers. 

Initially, she seemed to indicate the final piece, the _data delivery_, is
the most tedious part. But later after I outlined other places I thought could
be improved, she said the delivery step -- for Janssen anyway -- was not that 
big a deal. The most time consuming part is the checks and corrections. Not 
surprising given it is irreducably manual. But how can that be automated? She
asked. In the meantime they are relying on hand-written notes on the SAS reports
to keep track of issues. I described the idea of having a system to track these,
and at the same time collect strategic data, but not very well.


## What this is really about according to D.

ECOA data goes through minimal QC. The client essentially wants it with *no*
QC, daily, in their format. We apparently used to provide this through the
transaction system (StudyWorks), but the guy who maintained that has left.

Despite the insane deadline (January!), D. was helpful in outlining what
parts to focus on vs. what could be done in SAS at the outset. The idea of
doing anything besides the bare minimum in SAS, the idea of expanding our
horrible SAS code base to new areas, grates on my nerves of course.

  1. The source data will be CSV files that the DMs will get out of StudyWorks
     and dump on the SAS workstation. (I need to find out more about this - 
     there appears to be a way something like this is already set up on the
     SAS server).  This is a manual step; D. suggests waiting until we have
     a corporate ODS system to interface with before trying to automate, since 
     otherwise we will be dealing with each system separately.

  2. Either the act of transferring the source files, or a timed schedule, 
     triggers the running of a SAS program to process the source files.

  3. After the SAS program finishes, the output files are packaged up according
     to account-specific specs, and scheduled for FTP delivery, again according
     to specs.

  4. FTP jobs are run off the queue.

  5. Confirmation emails are sent when FTP jobs are completed successfully,
     probably also off a queue.





--------------------------------------------------------------------------------
_2 Dec 2017_

It's an odd situation. My boss thinks this one system provides a good model for
automation of data sends. But everything that needs to be automated, this system
*doesn't* automate. Everything it *does* automate, we don't really need. It's 
hard to know exactly what his motivation is, especially since it's coming down
from *his* boss. 

Meanwhile the poor DMs are being hit with daily data sends for certain studies.

I am going to assume for the moment that the source is the same.

The question to ask the DMs is really: what are the most tedious parts of doing
daily sends? Is it in fact the final data delivery, looking up file details
and passwords and such; or doing the checks; or overreads; corrections; or 
running the data itself; or something else / all of the above?

I suspect it is "all of the above", ie. there are lots of little steps and
looking up things and to-ing and fro-ing at each stage. Data delivery has always
been notoriously piecemeal and never given any budget -- not even on the radar.

That doesn't mean the solution should be to tackle the most tedious parts. A
great deal of it is interfacing with systems I have no control over. It has to
be some intersection of (a) tedious work and (b) things I have control over, or
that are currently done completely manually.

The other crucial thing to understand from the outset is that the demand for
further automation comes from _increased pressure from clients for speed up of 
data sends_. It's not going to help to simply automate the process, because the
work is just going to come that much faster. An essential weapon for employees
and their managers is access to data. They have to be able to make the case
for more resources, more hires, etc. by showing the increase in their workload,
the number of data issues with particular clients, etc. If there's anything my
boss, with his experience navigating a military bureaucracy, has taught me. 


## SAS generation via Google Forms: a mini post mortem

My own opinion is that introducing this process did reduce the number of steps
required to run checks, especially given DMs only have to fill out a form
once. But it has drawbacks as well, clearly. 

Two specimens:

1. D. several times has said, "but you could do the same thing, but without
Google forms, right?". Meaning, can't you integrate the interface where you
specify parameters with the interface where you run the actual checks?

2. R. said, "you have to go first here, then over there. And you don't
know if it's running or not".

In order to "fit within the cracks" of existing programs and processes, and with
a natural hesitation on my part to introduce _yet another user interface_, the
system feels ... not quite put together. One step above running a shell script
and going out to fetch the output. With an additional barrier of _lack of an
immediate feedback loop_, since jobs are queued.

Furthermore, the system does not really provide any additional insight into 
cross-study workload, at least not very easily. 


## What could a DM workflow look like?

I hesitate to sketch this without gaining more understanding of the tedious 
steps. But from what I understand of the problems.

1. **Data sends are scheduled via the datasends calendar app**. That is to say,
creating or updating the date due of a task on the calendar, corresponds to
setting or updating an actual automated task on a queue. Scary!

What does this task actually do then?

First, keep in mind there is an irreducably manual process here, and it's quite 
important that this part of it **not** be automated. That's the judgement of
which reported errors need data corrections, or in general what action is needed
given reported errors. 

My understanding of this manual process is roughly:

    1. Examine reported errors in data from Expert
    2. Make a set of corrections or take other actions based on errors
    3. If corrections were made, then repeat from the beginning
    4. Release production data for encoding and delivery
    
One way of looking at automation is _minimizing the manual steps around a 
manual process_. So in this case, the fetching of data from Expert, the 
running of QC checks, the running of production encoding and the file delivery
would all be minimized. Further, the entire process would be controlled from
a user interface centered around categorizing reported errors. The user
decides when to 'refresh' the data from Expert, and once all errors are 
accounted for, to 'release' the data to production and to the client.





2. **Extract data is pulled off of Expert**. Here we hit into significant issues.
Yes, web scraping is possible. Yes, user-specified parameters are possible
("QC Incremental", etc.)  But Expert keeps track of extracts, and for
incremental sends, it _calculates change flags based on a comparison extract_. 
If this proposed system also keeps track of extracts, what's to guarantee the
list is the same between the two systems?  You could say, we don't care about
Expert's change flag calculations, we will do our own calculations. That means
a full scale validation effort. (More likely, a manual process will remain for
incremental sends.)


Another significant issue here is that as currently conceived, each task 
corresponds to multiple extractions -- a manual loop from extract, run checks,
make corrections, do another extract, run checks, everything ok so convert QC
to production extract, run checks, run production filters.

