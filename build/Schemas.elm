port module Schemas exposing (..)

import JsonSchema exposing (Schema)
import JsonSchema.Encoder exposing (encode)

import Data.Task.Sequence

type alias NamedSchema =
    ( String, Schema )

    
schemas : List NamedSchema
schemas =
    [ ( "task.sequence", Data.Task.Sequence.schema ) ]


emitSchemas : List NamedSchema -> Cmd ()
emitSchemas namedSchemas =
    namedSchemas
        |> List.map (encodeNamedSchema >> emit)
        |> Cmd.batch


encodeNamedSchema : NamedSchema -> ( String, String )
encodeNamedSchema ( title, schema ) =
    ( title, encode schema )


main : Platform.Program Never () ()
main =
    Platform.program
        { init = ( (), emitSchemas schemas )
        , update = \_ _ -> ( (), Cmd.none )
        , subscriptions = \_ -> Sub.none
        }



{-| Emits pairs of schema name and schema JSON string -}
port emit : ( String, String ) -> Cmd a
