#!/usr/bin/env node

if (process.argv.length < 4) {
  fail(`Generate JSON schema files using json-elm-schema
Usage:
  bin/generate-schemas <elm-module> <output-directory>
`)
}

const fs = require('fs')

const path = require('path')
const temp = require('temp').track()
const compiler = require('node-elm-compiler')

const appName    = process.argv[2]
const sourcePath = path.resolve(process.argv[2]) + '.elm'
const prefixPath = path.resolve(process.argv[3])

const targetPath = temp.path({ suffix: '.js' })

compiler.compileSync(sourcePath, {
  yes: true,
  output: targetPath,
  cwd: '.',
})

const Elm = require(targetPath)
const app = Elm[appName].worker()

app.ports.emit.subscribe(function (payload) {
  const title = payload[0]
  const json = payload[1]
  const fileName = title + '.json'
  const schemaPath = path.join(prefixPath, fileName)

  fs.writeFile(schemaPath, json, (err) => {
    if (err) throw err

    console.log('Wrote schema for "' +
      title + '" to ' + schemaPath)
  })
})

function fail (msg) {
  process.stderr.write(msg)
  process.exit(1)
}
