import os.path
import unittest

from model.environ import Environ
from task.checksum import md5

from helper import clear_output, fixtures_file, output_file

class TestChecksum(unittest.TestCase):

  def setUp(self):
      clear_output()

  def assert_file_exists(self, file):
      self.assertTrue( os.path.exists(file), "File doesn't exist: %s" % (file,) )
  
  def test_checksum_files(self):
      environ = Environ( 
          client="Test", 
          protocol="Protocol", 
          account="999999",
          datatype="ECOA",
          location="BOS"
      )
      files = [
          fixtures_file('test_checksum\\Test\\Protocol\\ToSponsor\\file1.txt'),
          fixtures_file('test_checksum\\Test\\Protocol\\ToSponsor\\file2.txt')
      ]

      md5( files,
         environ=environ.to_dict(),
         file_name="checksum.txt",
         to_directory=output_file('test_checksum\\{client}\\{protocol}')
      )

      self.assert_file_exists( output_file('test_checksum\\Test\\Protocol\\checksum.txt') )

      # TODO check against CertUtils

if __name__ == '__main__':
  unittest.main()


