import os.path
import unittest

from model.environ import Environ
from task.zip import zip_files

from helper import clear_output, fixtures_file, output_file

class TestZipFiles(unittest.TestCase):

  def setUp(self):
      clear_output()

  def assert_file_exists(self, file):
      self.assertTrue( os.path.exists(file), "File doesn't exist: %s" % (file,) )
  
  def test_zip_files_no_password(self):
      environ = Environ( 
          client="Test", 
          protocol="Protocol", 
          account="999999",
          datatype="ECOA",
          location="BOS"
      )
      files = [
          fixtures_file('test_zip_files\\Test\\Protocol\\ToSponsor\\file1.txt'),
          fixtures_file('test_zip_files\\Test\\Protocol\\ToSponsor\\file2.txt')
      ]

      zip_files( files,
         environ=environ.to_dict(),
         file_name="test.zip",
         to_directory=output_file('test_zip_files\\{client}\\{protocol}')
      )

      self.assert_file_exists( output_file('test_zip_files\\Test\\Protocol\\test.zip') )

      # TODO unzip and check against originals

if __name__ == '__main__':
  unittest.main()

