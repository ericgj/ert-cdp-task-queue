import unittest
from webtest import TestApp

from helper import create_test_db, clear_test_db

from app import api, app
from model.taskseq import TaskSeq, Task, Param, Environ

class TestApi(unittest.TestCase):

    def test_api_path_for(self):
        act = api.path_for('taskseq.create', u_id='1')
        self.assertEqual( '/u/1/taskseq', act)

        act2 = api.path_for('taskseq.read', u_id='1', id='123')
        self.assertEqual( '/u/1/taskseq/123', act2)


class TestTaskSeq(unittest.TestCase):

    def setUp(self):
        create_test_db()
        clear_test_db(['task', 'task_sequence'])

    def test_create_taskseq(self):
        data = TaskSeq( Environ(name='test'),
            tasks=[
                Task( 'CheckFiles', 
                    params=[
                        Param( 'in directory', 'TextValue', '%HOME%\\csv' ),
                        Param( 'named', 'TextValue', '*.csv' )
                    ]
                ),
                Task( 'Sas',
                    params=[
                        Param( 'in directory', 'TextValue', '%HOME%' ),
                        Param( 'program file', 'TextValue', '%ACCOUNT%_v1_0_1.sas' ),
                        Param( 'sas options', 'GroupTextValue',
                               [ '-ERRORABEND' ] )
                    ]
                )
            ]
        ).to_dict()
        
        testapp = TestApp(app)
        create_resp = testapp.post_json( 
            api.path_for('taskseq.create', u_id='1'), 
            data
        )
        print(create_resp)

        loc = create_resp.location

        read_resp = testapp.get( loc )
        print(read_resp)



if __name__ == '__main__':
  unittest.main()

