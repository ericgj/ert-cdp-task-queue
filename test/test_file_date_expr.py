import unittest
from datetime import datetime

from model.filesys import FileDateExpr, DATE_UNIT, DATE_COMPARISON, DATE_RANGE

def show(t1,t2):
    return "%d  <=>  %d" % (t1,t2)

class TestFileDateExpr(unittest.TestCase):

    def test_secs_before_within(self):
        expr = FileDateExpr(
            quantity=30,
            unit=DATE_UNIT.seconds, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.within
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertTrue( expr.evaluate(t, t), show(t,t) )
        self.assertTrue( expr.evaluate(t - 29, t), show(t-29,t) )
        self.assertFalse( expr.evaluate(t - 30, t), show(t-30,t) )
        self.assertFalse( expr.evaluate(t - 31, t), show(t-31,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_mins_before_within(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.minutes, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.within
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertTrue( expr.evaluate(t, t), show(t,t) )
        self.assertTrue( expr.evaluate(t - 60, t), show(t-60,t) )
        self.assertFalse( expr.evaluate(t - 120, t), show(t-120,t) )
        self.assertFalse( expr.evaluate(t - 121, t), show(t-121,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_hrs_before_within(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.hours, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.within
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertTrue( expr.evaluate(t, t), show(t,t) )
        self.assertTrue( expr.evaluate(t - (60 * 60), t), show(t-(60*60),t) )
        self.assertFalse( expr.evaluate(t - (120 * 60), t), show(t-(120*60),t) )
        self.assertFalse( expr.evaluate(t - (120 * 60) - 1, t), show(t-(120*60)-1,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_days_before_within(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.days, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.within
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertTrue( expr.evaluate(t, t), show(t,t) )
        self.assertTrue( expr.evaluate(t - (60 * 60 * 24), t), show(t-(60*60*24),t) )
        self.assertFalse( expr.evaluate(t - (120 * 60 * 24), t), show(t-(120*60*24),t) )
        self.assertFalse( expr.evaluate(t - (120 * 60 * 24) - 1, t), show(t-(120*60*24)-1,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )


    def test_secs_before_at_least(self):
        expr = FileDateExpr(
            quantity=30,
            unit=DATE_UNIT.seconds, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.at_least
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertFalse( expr.evaluate(t, t), show(t,t) )
        self.assertFalse( expr.evaluate(t - 29, t), show(t-29,t) )
        self.assertTrue( expr.evaluate(t - 30, t), show(t-30,t) )
        self.assertTrue( expr.evaluate(t - 31, t), show(t-31,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_mins_before_at_least(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.minutes, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.at_least
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertFalse( expr.evaluate(t, t), show(t,t) )
        self.assertFalse( expr.evaluate(t - 60, t), show(t-60,t) )
        self.assertTrue( expr.evaluate(t - 120, t), show(t-120,t) )
        self.assertTrue( expr.evaluate(t - 121, t), show(t-121,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_hrs_before_at_least(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.hours, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.at_least
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertFalse( expr.evaluate(t, t), show(t,t) )
        self.assertFalse( expr.evaluate(t - (60 * 60), t), show(t-(60*60),t) )
        self.assertTrue( expr.evaluate(t - (120 * 60), t), show(t-(120*60),t) )
        self.assertTrue( expr.evaluate(t - (120 * 60) - 1, t), show(t-(120*60)-1,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )

    def test_days_before_at_least(self):
        expr = FileDateExpr(
            quantity=2,
            unit=DATE_UNIT.days, 
            comparison=DATE_COMPARISON.before,
            range=DATE_RANGE.at_least
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertFalse( expr.evaluate(t, t), show(t,t) )
        self.assertFalse( expr.evaluate(t - (60 * 60 * 24), t), show(t-(60*60*24),t) )
        self.assertTrue( expr.evaluate(t - (120 * 60 * 24), t), show(t-(120*60*24),t) )
        self.assertTrue( expr.evaluate(t - (120 * 60 * 24) - 1, t), show(t-(120*60*24)-1,t) )
        self.assertFalse( expr.evaluate(t + 1, t), show(t+1,t) )


    def test_secs_after_within(self):
        expr = FileDateExpr(
            quantity=30,
            unit=DATE_UNIT.seconds, 
            comparison=DATE_COMPARISON.after,
            range=DATE_RANGE.within
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertTrue( expr.evaluate(t, t), show(t,t) )
        self.assertTrue( expr.evaluate(t + 29, t), show(t+29,t) )
        self.assertFalse( expr.evaluate(t + 30, t), show(t+30,t) )
        self.assertFalse( expr.evaluate(t + 31, t), show(t+31,t) )
        self.assertFalse( expr.evaluate(t - 1, t), show(t-1,t) )

    def test_secs_after_at_least(self):
        expr = FileDateExpr(
            quantity=30,
            unit=DATE_UNIT.seconds, 
            comparison=DATE_COMPARISON.after,
            range=DATE_RANGE.at_least
        )
        print(str(expr) + u"\n")
        t = datetime.now().timestamp()
        self.assertFalse( expr.evaluate(t, t), show(t,t) )
        self.assertFalse( expr.evaluate(t + 29, t), show(t+29,t) )
        self.assertTrue( expr.evaluate(t + 30, t), show(t+30,t) )
        self.assertTrue( expr.evaluate(t + 31, t), show(t+31,t) )
        self.assertFalse( expr.evaluate(t - 1, t), show(t-1,t) )


if __name__ == '__main__':
  unittest.main()


