from datetime import datetime, timedelta
import time
import os
import os.path
import shutil
import logging

logging.basicConfig(level=logging.DEBUG)

from webtest import TestApp

os.environ['APP_ENV'] = 'test'

from apscheduler.schedulers.background import BackgroundScheduler

import env
import db
import db.taskseq

db_config = env.db_config()


def minute_in_future(dt):
    return (dt + timedelta(minutes=1)).minute


def fixtures_file(fname):
    return env.local_file( os.path.join('test', 'fixtures', fname) )

def fixtures_json(fname):
    return env.load_json( fixtures_file(fname) )

def output_file(fname):
    return env.local_file( os.path.join('test', 'output', fname) )

def clear_output():
    shutil.rmtree( os.path.join('test', 'output') )
    os.mkdir( os.path.join('test', 'output') )
    
def copy_fixture_to_output(fname):
    shutil.copytree( fixtures_file(fname), output_file(fname) )

# note: if not exists
def create_test_db( ):
    script = None
    with open( env.local_file('db/create.sql'), 'r') as f:
        script = f.read()
    db_config = env.db_config()
    conn = db.connect( **db_config )
    conn.executescript(script)
    conn.commit()

def clear_test_db( tables ):
    conn = db.connect( **env.db_config() )
    conn.executescript(
        ";\n\n".join([
            "DELETE FROM %s" % t for t in tables
        ])
    )
    conn.commit()


def clear_scheduler( ):
    scheduler = None
    try:
        scheduler = BackgroundScheduler( env.scheduler_config() )
        scheduler.start()

        jobs = scheduler.get_jobs()
        for job in jobs:
            job.remove()

    finally:
        if not scheduler is None:
            scheduler.shutdown()


"""
This is a bit too "full stack". In some ways that's good, it tests the actual
endpoint, but it means every test takes up to a minute extra to run. Preferably
we would change this to interact directly with the scheduler and mq broker to
submit the sequence immediately. Or just test tasks outside of the scheduler
and queue.
"""
def create_and_submit_sequence(seq, *, api, app): 
    storage = db.taskseq.Storage(db_config)
    id = storage.create( seq )

    t = minute_in_future( datetime.utcnow() )
    crontab = "%d * * * *" % (t,)
    print("Expecting it to be enqueued at %d after the hour" % (t,))

    testapp = TestApp(app)
    resp = testapp.post_json( 
        api.path_for(
            'taskseq.schedule.create', 
            u_id='1', taskseq_id=str(id)
        ), 
        { 'crontab': crontab } 
    )
    print(resp)

    print("Waiting....")
    time.sleep(60)


