import unittest

from helper import (
  create_test_db, clear_test_db, clear_scheduler, fixtures_file, output_file,
  create_and_submit_sequence
)

from app import api, app
from model.taskseq import TaskSeq, Task, Param
from model.environ import Environ
from model.notification import Notification, NotificationOptions, Email

class TestTaskSchedule(unittest.TestCase):

    def setUp(self):
        clear_scheduler()
        create_test_db()
        clear_test_db(['task', 'task_sequence'])

    def test_schedule_files_tasks(self):
        email_options = Email(
            from_=None,
            to=set(),
            cc=set([ "manager@example.com" ])
        )
        notif = Notification(
            failure=NotificationOptions(email=email_options),
            success=NotificationOptions(email=email_options)
        )
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS",
            notification=notif
        )

        """ Note the CopyFiles task here also tests date formatting """
        seq = TaskSeq( environ,
            tasks=[
                Task( 'CheckFiles', 
                    params=[
                        Param( 'in_directory', 'TextValue', 
                            fixtures_file('test_schedule_files_tasks\\success') 
                        ),
                        Param( 'files_named', 'TextValue', '*.csv' )
                    ]
                ),
                Task( 'CopyFiles',
                    params=[
                        Param( 'to_directory', 'TextValue',
                            output_file(
                                'test_schedule_files_tasks\\' + 
                                '{runtime_utc_datetime_started_at:%Y-%m-%d}'
                            )
                        ),
                        Param( 'create_directory', 'BooleanValue', True )
                    ]
                ),
                Task( 'VerifyFiles',
                    params=[
                        Param( 'source_directory', 'TextValue',
                            fixtures_file('test_schedule_files_tasks\\success')
                        )
                    ]
                ),
                Task( 'CheckFiles', 
                    params=[
                        Param( 'in_directory', 'TextValue', 
                            fixtures_file('test_schedule_files_tasks\\fail') 
                        ),
                        Param( 'files_named', 'TextValue', '*.csv' )
                    ]
                )
            ]
        )

        create_and_submit_sequence(seq, api=api, app=app)




if __name__ == '__main__':
  unittest.main()

