import unittest
import os
import os.path
from datetime import datetime
from glob import glob

from helper import fixtures_file

from task.filesys import check_files, FilesNotFoundException


def get_most_recent_modified(dir, files_named):
    s = get_stat(dir, files_named, lambda s: s.st_mtime, True)
    return None if s is None else s.st_mtime

def get_biggest(dir, files_named):
    s = get_stat(dir, files_named, lambda s: s.st_size, True)
    return None if s is None else s.st_size

def get_stat(dir, files_named, sorter, desc=False):
    files = glob( os.path.join(dir, files_named) )
    if len(files) == 0:
        return None
    else:
        stats = sorted(
            [ os.stat(f) for f in files ],
            key=sorter,
            reverse=desc
        )
        return stats[0]

def dummy_environ_at_timestamp(ts):
    return {
      'client': 'CLIENT',
      'protocol': 'PROTOCOL',
      'account': '123456',
      'datatype': 'ECOA',
      'location': 'PHL',
      'runtime': {
          'task_seq_id': '987',
          'job_id': 'abc123',
          'timestamp_started_at': ts
      }
    }


class TestCheckFiles(unittest.TestCase):

    def test_check_files_with_date_filter_matching(self):
        dir = fixtures_file('test_check_files_with_date_filter')
        files_named = '*'
        modified = {
            'quantity': 1,
            'unit': 'hours',
            'comparison': 'before',
            'range': 'within'
        }
        latest = get_most_recent_modified(dir, files_named)

        environ = dummy_environ_at_timestamp( latest + (60 * 59) )

        check_files( None,
           environ=environ,
           in_directory=dir,
           files_named=files_named,
           modified=modified
        )

        self.assertTrue(True)


    def test_check_files_with_date_filter_not_matching(self):
        dir = fixtures_file('test_check_files_with_date_filter')
        files_named = '*'
        modified = {
            'quantity': 30,
            'unit': 'minutes',
            'comparison': 'before',
            'range': 'within'
        }
        latest = get_most_recent_modified(dir, files_named)

        environ = dummy_environ_at_timestamp( latest + (60 * 59) )

        with self.assertRaises( FilesNotFoundException ):
            check_files( None,
               environ=environ,
               in_directory=dir,
               files_named=files_named,
               modified=modified
            )

    def test_check_files_with_size_filter_matching(self):
        dir = fixtures_file('test_check_files_with_size_filter')
        files_named = '*'
        biggest = get_biggest(dir, files_named)
        size = {
          'quantity': biggest // 1000,
          'unit': 'kb',
          'comparison': '>'
        }
        
        environ = dummy_environ_at_timestamp( datetime.now().timestamp() )

        check_files( None,
            environ=environ,
            in_directory=dir,
            files_named=files_named,
            file_size=size
        )

        self.assertTrue(True)

    def test_check_files_with_size_filter_not_matching(self):
        dir = fixtures_file('test_check_files_with_size_filter')
        files_named = '*'
        biggest = get_biggest(dir, files_named)
        size = {
          'quantity': (biggest // 1000) + 1,
          'unit': 'kb',
          'comparison': '>'
        }
        
        environ = dummy_environ_at_timestamp( datetime.now().timestamp() )

        with self.assertRaises( FilesNotFoundException ):
            check_files( None,
                environ=environ,
                in_directory=dir,
                files_named=files_named,
                file_size=size
            )




if __name__ == '__main__':
  unittest.main()

