import logging
import os.path
import unittest

logging.basicConfig(level=logging.DEBUG)

import env
from model.environ import Environ
from model.notification import Notification, NotificationOptions, Email
from model.taskseq.status import Status

from task.notification import email, success_email, failure_email

from helper import clear_output, fixtures_file, output_file

class TestNotifyEmail(unittest.TestCase):

    def test_notify_email_markdown_body_no_attachments(self):
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS",
            notification=None
        )

        body="""
# Data delivery notification

Data for **{{datatype}} {{protocol}}** has been copied to the sftp site. Please 
verify the data from your end.

Thanks,<br/>
{{email_from}}

        """

        email( [],
            environ=environ.to_dict(),
            from_addr= "Obi Wan Kenobi <obi@example.com>",
            to_addrs=[ "recip1@example.com", "Bugs Bunny <bug@bunny.com>" ],
            subject="Your data is ready for {datatype} {protocol} (ref: {account})",
            body=body
        )


    def test_notify_success(self):
        email_options = Email(
            from_="Obi Wan Kenobi <obi@example.com>",
            to=set([ "recip1@example.com", "Bugs Bunny <bug@bunny.com>" ]),
            cc=set([ "manager@example.com" ])
        )
        notif = Notification(
            success=NotificationOptions(email=email_options),
            failure=NotificationOptions(email=None)
        )
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS",
            notification=notif
        )

        base_notif = Notification.from_dict(env.notification_config())

        status = Status(
            task_seq_id="12345", 
            job_id="abc123", 
            seq_timestamp=1518971663,
            environ=environ,
            messages=[]
        )

        success_email( status.to_dict(), 
            environ=environ.to_dict(), 
            notification=base_notif.to_dict() 
        )

    def test_notify_failure(self):
        email_options = Email(
            from_=None,
            to=set([ "recip1@example.com", "Bugs Bunny <bug@bunny.com>" ]),
            bcc=set([ "manager@example.com" ])
        )
        notif = Notification(
            failure=NotificationOptions(email=email_options),
            success=NotificationOptions(email=None)
        )
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS",
            notification=notif
        )

        base_notif = Notification.from_dict(env.notification_config())

        status = Status(
            task_seq_id="12345", 
            job_id="abc123", 
            seq_timestamp=1518971663,
            environ=environ,
            messages=[]
        )

        failure_email( status.to_dict(), 
            environ=environ.to_dict(), 
            notification=base_notif.to_dict() 
        )



if __name__ == '__main__':
  unittest.main()

