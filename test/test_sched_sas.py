import unittest

from helper import (
  create_test_db, clear_test_db, clear_scheduler, 
  fixtures_file, output_file, clear_output, copy_fixture_to_output, 
  create_and_submit_sequence
)

from app import api, app
from model.taskseq import TaskSeq, Task, Param
from model.environ import Environ
from model.notification import Notification,NotificationOptions,Email


class TestTaskSchedule(unittest.TestCase):

    def setUp(self):
        clear_scheduler()
        create_test_db()
        clear_test_db(['task', 'task_sequence'])
        clear_output()

    def xxx_test_schedule_check_files_and_run_sas_tasks(self):
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS"
        )
        seq = TaskSeq( environ,
            tasks=[
                Task( 'CheckFiles', 
                    params=[
                        Param( 'in_directory', 'TextValue', 
                               # 'E:\\community\\eCOA\\StudyWorks\\{client}\\{protocol}\\csv') ,
                               fixtures_file('test_schedule_run_sas\\{client}\\{protocol}\\csv')) ,
                        Param( 'files_named', 'TextValue', '*.txt' )
                    ]
                ),
                Task( 'RunSas', 
                    params=[
                        Param( 'in_directory', 'TextValue',
                               # 'E:\\community\\eCOA\\StudyWorks\\{client}\\{protocol}') ,
                               fixtures_file('test_schedule_run_sas\\{client}\\{protocol}')) ,
                        Param( 'program_file', 'TextValue',
                               'gensas_wrapper.sas') 
                    ]
                )
            ]
        )

        create_and_submit_sequence(seq, api=api, app=app)


    def test_schedule_run_sas_transaction(self):
        email_options = Email(
            from_="Obi Wan Kenobi <obi@example.com>",
            to=set([ "recip1@example.com", "Bugs Bunny <bug@bunny.com>" ])
        )
        notif = Notification(
            success=NotificationOptions(email=email_options),
            failure=NotificationOptions(email=None)
        )
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS",
            notification=notif
        )
        seq = TaskSeq( environ,
            tasks=[
                Task( 'SelectFiles', 
                    params=[
                        Param( 'in_directory', 'TextValue', 
                               # 'E:\community\eCOA\StudyWorks\\{client}\\{protocol}\\csv'
                               output_file(
                                   'test_schedule_run_sas_transaction\\{client}\\{protocol}\\csv'
                               )
                        ) ,
                        Param( 'files_named', 'TextValue', '*.txt' ) ,
                        Param( 'order_by', 'TextValue', 'mtime' ) ,
                        Param( 'order_reverse', 'BooleanValue', True ),
                        Param( 'limit', 'NumericValue', 1 )
                    ]
                ),
                Task( 'RunSasTransaction', 
                    params=[
                        Param( 'in_directory', 'TextValue',
                               # 'E:\community\eCOA\StudyWorks\\{client}\\{protocol}'
                               output_file('test_schedule_run_sas_transaction\\{client}\\{protocol}')
                        ) ,
                        Param( 'program_file', 'TextValue',
                               '&macro\\gensas.sas') ,
                        Param( 'program_name', 'TextValue',
                               'gensas') ,
                        Param( 'last_result_var', 'TextValue',
                               'extract') ,
                        Param( 'libnames', 'GroupTextValue',
                               [ ('checks', 'checks') ] )
                    ]
                ) ,
                Task( 'RunSasTransaction', 
                    params=[
                        Param( 'in_directory', 'TextValue',
                               # 'E:\community\eCOA\StudyWorks\\{client}\\{protocol}'
                               output_file('test_schedule_run_sas_transaction\\{client}\\{protocol}')
                        ) ,
                        Param( 'program_file', 'TextValue',
                               'Protocol_v1.0.sas') ,
                        Param( 'program_name', 'TextValue',
                               'encoding') ,
                        Param( 'libnames', 'GroupTextValue',
                               [ ('checks', 'checks'),
                                 ('dta', 'data'),
                                 ('tospon', 'ToSponsor')
                               ] 
                        )
                    ]
                )
            ]
        )

        copy_fixture_to_output('test_schedule_run_sas_transaction')

        create_and_submit_sequence(seq, api=api, app=app)



if __name__ == '__main__':
  unittest.main()

