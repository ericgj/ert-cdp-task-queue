import os.path
import unittest

from model.environ import Environ
from task.filesys import copy_files

from helper import clear_output, fixtures_file, output_file

class TestCopyFiles(unittest.TestCase):

  def setUp(self):
      clear_output()

  def assert_file_exists(self, file):
      self.assertTrue( os.path.exists(file), "File doesn't exist: %s" % (file,) )
 
  def test_copy_files_with_verify(self):

      environ = Environ( 
          client="Test", 
          protocol="Protocol", 
          account="999999",
          datatype="ECOA",
          location="BOS"
      )
      files = [
          fixtures_file('test_copy_files\\Test\\Protocol\\ToSponsor\\file1.txt'),
          fixtures_file('test_copy_files\\Test\\Protocol\\ToSponsor\\file2.txt')
      ]

      os.makedirs(output_file('test_copy_files\\Test\\Protocol'), exist_ok=True) 

      copy_files( files,
          environ=environ.to_dict(),
          to_directory=output_file('test_copy_files\\Test\\Protocol'),
          verify=True
      )


if __name__ == '__main__':
  unittest.main()

