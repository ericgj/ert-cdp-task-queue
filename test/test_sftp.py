import unittest

from helper import (
  create_test_db, clear_test_db, clear_scheduler, fixtures_file, fixtures_json, 
  create_and_submit_sequence
)

from app import api, app
from model.taskseq import TaskSeq, Task, Param
from model.environ import Environ


class TestTaskScheduleSftp(unittest.TestCase):

    def setUp(self):
        clear_scheduler()
        create_test_db()
        clear_test_db(['task', 'task_sequence'])

    
    def test_sftp_upload(self):
        sftp_params = fixtures_json('secrets/test_sftp_upload.json')
        environ = Environ( 
            client="Test", 
            protocol="Protocol", 
            account="999999",
            datatype="ECOA",
            location="BOS"
        )
        seq = TaskSeq( environ,
            tasks=[
                Task( 'CheckFiles', 
                    params=[
                        Param( 'in_directory', 'TextValue', 
                               fixtures_file('test_sftp_upload\\{client}\\{protocol}\\ToSponsor') ) ,
                        Param( 'files_named', 'TextValue', '*.zip' )
                    ]
                ),
                Task( 'UploadSftp', 
                    params=[
                        Param( 'host', 'TextValue', sftp_params['host'] ) ,
                        Param( 'port', 'NumericValue', sftp_params['port'] ) ,
                        Param( 'username', 'TextValue', sftp_params['username'] ),
                        Param( 'password', 'TextValue', sftp_params['password'] ),
                        Param( 'remote_dir', 'TextValue', sftp_params['remote_dir'] )
                    ]
                )
            ]
        )

        create_and_submit_sequence(seq, api=api, app=app)


if __name__ == '__main__':
  unittest.main()


