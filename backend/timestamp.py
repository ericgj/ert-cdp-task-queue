from datetime import datetime
from time import gmtime
import pytz

def to_utc_datetime(ts):
    return datetime( *gmtime(ts)[:6], tzinfo=pytz.utc )

"""
Note: not currently used. If we need this, need to add the zonename to Environ
and taskseq.Status models, presumably loaded in through some manual config file 
since Windows does not use IANA timezone names...
"""
def to_local_datetime(ts, zonename):
    tz = pytz.timezone(zonename)
    return tz.localize( datetime( *gmtime(ts)[:6] ) )

def utc_string(ts):
    return to_utc_datetime(ts).strftime(u'%Y%m%d%H%M%SZ')

def utc_human(ts):
    return to_utc_datetime(ts).strftime(u'%e %b %Y at %I:%M:%S%p (UTC)')

