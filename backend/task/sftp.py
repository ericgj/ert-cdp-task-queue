import logging
import os.path
import socket

import paramiko

from model.environ import Environ

logger = logging.getLogger(__name__)

class SftpError(Exception):
    def __init__(self, error, log_file=None):
        self.error = error
        self.attachments = [ log_file ]
        
    def __str__(self):
        return str(self.error)


def upload( local_files, *, environ, host, port, username, password, remote_dir, log=None, sftp_options={} ):

    data = Environ.from_dict(environ).to_data()
    host = host.format(**data)
    remote_dir = remote_dir.format(**data)
    log  = None if log is None else log.format(**data)
    
    if type(local_files) == str:
        local_files = [ local_files ]
    
    if not log is None:
        sftp_options['log'] = log

    # Optionally specify a known hosts file

    known_hosts = sftp_options.get(u'known_hosts')
    host_keys = {}
    hostkeytype = None
    hostkey = None
    if known_hosts:
        try:
            host_keys = paramiko.util.load_host_keys( known_hosts )
        except IOError:
            logger.warn(u"Unable to open known hosts file: %s" % known_hosts)

    if host in host_keys:
        hostkeytype = host_keys[host].keys()[0]
        hostkey = host_keys[host][hostkeytype]
        logger.info("Using host key of type %s" % hostkeytype)

    
    # Optionally log to external file, but probably not needed. Logs should
    # just go to the worker's logger

    sftp_log = sftp_options.get(u'log')
    if sftp_log:
        paramiko.util.log_to_file(sftp_log)

    try:
        transport = paramiko.Transport((host,port))
        transport.connect(hostkey, username, password, gss_auth=False)
        sftp = paramiko.SFTPClient.from_transport(transport)
    
        remote_files = []
        for f in local_files:
            remote_file = os.path.join(remote_dir, os.path.basename(f))
            remote_files.append(remote_file)
    
            logger.info("Uploading %s ---> %s" % (f, remote_file))
            sftp.put(f, remote_file)
    
        transport.close()
        
    except Exception as e:
        transport.close()
        raise SftpError(e, log_file=sftp_log)

    return remote_files

