import os
import os.path
import logging
import shutil
from glob import glob

from util import take
from model.environ import Environ
from model.filesys import FileSizeExpr, FileDateExpr
from task.checksum import hash_file

logger = logging.getLogger(__name__)

class FilesNotFoundException(Exception):
    def __init__(self, dir, fileglob, modified=None, file_size=None):
        self.dir = dir
        self.fileglob = fileglob
        self.modified = modified
        self.file_size = file_size

    def __str__(self):
        modified_expr, file_size_expr = None, None
        if not self.modified is None:
            modified_expr = FileDateExpr.from_dict(self.modified)

        if not self.file_size is None:
            file_size_expr = FileSizeExpr.from_dict(self.file_size)

        filters = (
            ( [] if modified_expr is None else [ u'modified ' + str(modified_expr)] ) +
            ( [] if file_size_expr is None else [ u'size is ' + str(file_size_expr)] )
        )
        if len(filters) == 0:
            return u"No files found in '%s' matching '%s'" % (self.dir, self.fileglob) 
        else:
            return u"No files found in '%s' matching '%s' where %s" % (
                self.dir, 
                self.fileglob,
                u" and ".join(filters)
            )


class FileNotFoundException(Exception):
    def __init__(self, file):
        self.file = file

    def __str__(self):
        return u"File not found: '%s'" % (self.file,)
        
class DirectoryNotFoundException(Exception):
    def __init__(self, dir):
        self.dir = dir

    def __str__(self):
        return u"Directory not found: '%s'" % (self.dir,)
        
class FileVerifyException(Exception):
    def __init__(self, source, target):
        self.source = source
        self.target = target

    def __str__(self):
        return u"File copy failed: '%s' ----> '%s'" % (self.source,self.target)


def select_files(last_result, *, 
    environ, 
    in_directory, 
    files_named=u'*', 
    modified=None, 
    file_size=None, 
    order_by=u'name', 
    order_reverse=False,
    limit=None
):

    def sorter(pair):
        filename, stat = pair
        if order_by == u'name':
            return filename
        elif order_by == u'mtime':
            return stat.st_mtime
        elif order_by == u'size':
            return stat.st_size
        else:
            return filename

    def filterer(ts, stat):
        if not modified is None:
            modified_expr = FileDateExpr.from_dict(modified)

        if not file_size is None:
            file_size_expr = FileSizeExpr.from_dict(file_size)

        return (
            ( modified is None or modified_expr.evaluate(stat.st_mtime, ts) ) and
            ( file_size is None or file_size_expr.evaluate(stat.st_size) )
        )


    environ = Environ.from_dict(environ)
    data = environ.to_data()

    in_directory = in_directory.format(**data)
    files_named = files_named.format(**data)

    if not os.path.exists(in_directory):
        raise DirectoryNotFoundException(in_directory)

    files = glob( os.path.join( in_directory, files_named ) )

    stats = sorted(
        [ (f, os.stat(f)) for f in files 
            if filterer( environ.runtime.timestamp_started_at, os.stat(f)) 
        ],
        key=sorter,
        reverse=order_reverse
    )
    
    if limit is None:
        return [ os.path.normpath(f) for (f,_) in stats ]
    else:
        return [ os.path.normpath(f) for (f,_) in take(limit, stats) ]


def check_files( last_result, *,
    environ,
    in_directory,
    files_named,
    modified=None,
    file_size=None
):
    result = select_files(last_result, 
        environ=environ,
        in_directory=in_directory,
        files_named=files_named,
        modified=modified,
        file_size=file_size
    )

    if len(result) < 1:
        raise FilesNotFoundException(in_directory, files_named, modified, file_size)
    else:
        return result


def move_files( last_result, *,
    environ,
    to_directory,
    create_directory=False,
):
    data = Environ.from_dict(environ).to_data()
    to_directory = to_directory.format(**data)
    
    if isinstance(last_result,str):
        last_result = [ last_result ]
        
    if create_directory:
        mkdir_p(to_directory)
    else:
        if not os.path.exists(to_directory):
            raise DirectoryNotFoundException(to_directory)
            
    result = []
    for fname in last_result:
        target = os.path.join(to_directory, os.path.basename(fname))

        logger.info(u"Moving %s ----> %s" % (fname, target))
        os.rename(fname, target)
        logger.info(u"Moved %s ----> %s" % (fname, target))

        result.append(target)
        
    return result


def copy_files( last_result, *,
    environ,
    to_directory,
    create_directory=False,
    verify=False,
):
    data = Environ.from_dict(environ).to_data()
    to_directory = to_directory.format(**data)

    if isinstance(last_result,str):
        last_result = [ last_result ]

    if create_directory:
        mkdir_p(to_directory)
    else:
        if not os.path.exists(to_directory):
            raise DirectoryNotFoundException(to_directory)

    result = []
    for fname in last_result:
        target = os.path.join(to_directory, os.path.basename(fname))
        
        logger.info(u"Copying %s ----> %s" % (fname, target))
        _copy_file(fname, target)
        logger.info(u"Copied %s ----> %s" % (fname, target))
        
        if verify:
            if not _verify_file(fname, target):
                raise FileVerifyException(fname, target)
            else:
                logger.info(u"Verified copy %s ----> %s" % (fname, target))

        result.append(target)

    return result


def verify_files( last_result, *,
    environ,
    source_directory
):
    data = Environ.from_dict(environ).to_data()
    source_directory = source_directory.format(**data)

    if isinstance(last_result,str):
        last_result = [ last_result ]

    result = []
    for fname in last_result:
        source = os.path.join(source_directory, os.path.basename(fname))
        if not _verify_file(source, fname):
            raise FileVerifyException(source, fname)
        logger.info(u"Verified copy %s ----> %s" % (source, fname))
        result.append((source, fname))

    return result


def _verify_file(source, target):
    if not os.path.exists(source):
        raise FileNotFoundException(source)

    if not os.path.exists(target):
        raise FileNotFoundException(target)

    source_hash = hash_file(source, u'md5')
    target_hash = hash_file(target, u'md5')

    return source_hash.digest() == target_hash.digest()


def _copy_file(source, target):
    shutil.copy2(source, target)   

def _fast_copy_file(source, target, buffer_size=131072):
    """
    Fast copy adapted from
    https://stackoverflow.com/a/28129677/268977
    
    NOTE: for some reason it doesn't seem to be working (Windows? network fs?)
    So this is currently not used.
    """

    try:
        O_BINARY = os.O_BINARY
    except AttributeError:
        O_BINARY = 0

    read_flags = os.O_RDONLY | O_BINARY
    write_flags = os.O_WRONLY | os.O_CREAT | os.O_TRUNC | O_BINARY

    fin, fout = None, None
    try:
        fin = os.open(source, read_flags)
        stat = os.fstat(fin)
        fout = os.open(target, write_flags, stat.st_mode)
        for chunk in _iter_chunk(fin, buffer_size):
            os.write(fout, chunk)

    finally:
        if not fin is None: 
            os.close(fin)
        if not fout is None:
            os.close(fout)


def _iter_chunk(stream, size):
    chunk = os.read(stream, size)
    if chunk == b'':
        raise StopIteration
    else:
        yield chunk



def mkdir_p(name):
    return os.makedirs(name, exist_ok=True)


