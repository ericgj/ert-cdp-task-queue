import os
import os.path
from zipfile import ZipFile, ZIP_DEFLATED

from model.environ import Environ

def zip_files(last_result, *, environ, file_name, to_directory, password=None):
    
    assert (not last_result is None), u"No files to zip"
    if isinstance(last_result, list):
        assert len(last_result) > 0, u"No files to zip"
    else:
        assert isinstance(last_result,str), \
            u"Unknown type of last result: %s" % (last_result.__class__.__name__)
        last_result = [ last_result ]

    environ = Environ.from_dict(environ)
    data = environ.to_data()
    
    file_name = file_name.format(**data)
    to_directory = to_directory.format(**data)
    output_file = os.path.join( to_directory, file_name )

    os.makedirs(to_directory, exist_ok=True)

    with ZipFile( output_file, 'w', compression=ZIP_DEFLATED ) as z:
        if not password is None:
            z.setpassword(password)
        for fname in last_result:
            z.write(fname, arcname=os.path.basename(fname))

    return [ output_file ]

