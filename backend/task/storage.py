import logging

from dramatiq.message import Message

import env
import db
from model.environ import Environ
from model.taskseq.status import Status

logger = logging.getLogger(__name__)


def store_sequence( environ, messages ):
    environ = Environ.from_dict(environ)
    task_seq_id = environ.runtime.task_seq_id
    job_id = environ.runtime.job_id
    ts = environ.runtime.timestamp_started_at
    messages = [ Message(**m) for m in messages ]
    conn = db.connect( **env.db_config() )
    db.taskstatus_init( 
        conn,
        task_seq_id=task_seq_id, 
        job_id=job_id, 
        timestamp=ts, 
        messages=messages
    )
    

def fetch_sequence_status( environ ):
    environ = Environ.from_dict(environ)
    task_seq_id = environ.runtime.task_seq_id
    job_id = environ.runtime.job_id
    conn = db.connect( **env.db_config() )
    return (
        Status(
            job=None,
            job_id=job_id,
            environ=environ,
            messages=db.taskstatus_list( conn, task_seq_id=task_seq_id, job_id=job_id )
        ).to_dict()
    )

def store_task_starting( ts, msgdata ):
    conn = db.connect( **env.db_config() )
    db.taskstatus_update_starting(
        conn,
        timestamp=ts,
        message=Message(**msgdata)
    )

def store_task_success( ts, msgdata, result ):
    conn = db.connect( **env.db_config() )
    db.taskstatus_update_success(
        conn,
        timestamp=ts,
        message=Message(**msgdata),
        result=result
    )

def store_task_failure( ts, msgdata, error ):
    conn = db.connect( **env.db_config() )
    db.taskstatus_update_failure(
        conn,
        timestamp=ts,
        message=Message(**msgdata),
        error=error
    )


