import logging
import os.path
from smtplib import SMTP
from email.message import EmailMessage

from mistune import Markdown
markdown = Markdown()
import pystache
import mimetypes

import env
from model.environ import Environ
from model.notification import Notification
from model.email import Config
from model.taskseq.status import Status

logger = logging.getLogger(__name__)

HTML_HEADER = """
<style>
  table {
    border-collapse: collapse;
  }

  th, td {
    border: 1px solid #ccc;
    padding: 10px;
    text-align: left;
  }

  code {
    font-size: 12pt;
  }
</style>
"""

class EmailNotificationNotDefined(Exception):
    def __init__(self, options):
        self.options = options

    def __str__(self):
        return u"No email notification options defined"


def success_email( status, *,
    environ,
    notification
):
    subject = u'Completed: {client} {account} {datatype} Protocol {protocol}'
    body = """
# Success
## {{client}} {{account}} {{datatype}} Protocol {{protocol}}

Started at {{runtime_human_started_at}}

Completed job without errors.

| Timestamp                     | Task Seq ID             | Job ID                 |
|-------------------------------|-------------------------|------------------------| 
| {{runtime_string_started_at}} | {{runtime_task_seq_id}} | {{runtime_job_id}}     |


## Details

{{#last_result}}

Total processing time: {{elapsed_time_human}}

| Task          | Status         | Result                    |
|---------------|----------------|---------------------------|
{{#messages}}{{#message}} | {{actor_name}}{{/message}} | {{status}} | {{#result_list}}`{{.}}`<br/>{{/result_list}} |
{{/messages}}

{{/last_result}}

    """

    environ = Environ.from_dict(environ)
    notif = Notification.from_dict(notification)
    notif = notif | environ.notification

    success_email = notif.success.email

    if success_email is None:
        raise EmailNotificationNotDefined(notif.success)

    attachs = Status.from_dict(status).attachments()

    return email( status, 
        environ=environ.to_dict(), 
        from_addr=success_email.from_,
        subject=subject,
        body=body,
        to_addrs=list(success_email.to),
        cc_addrs=list(success_email.cc),
        bcc_addrs=list(success_email.bcc),
        attachments=attachs
    )


def failure_email( status, *,
    environ,
    notification
):
    subject = u'Failed: {client} {account} {datatype} Protocol {protocol}'
    body = """
# Failed
## {{client}} {{account}} {{datatype}} Protocol {{protocol}}

Started at {{runtime_human_started_at}}

An error occurred processing the job, see details below.

| Timestamp                     | Task Seq ID             | Job ID                 |
|-------------------------------|-------------------------|------------------------| 
| {{runtime_string_started_at}} | {{runtime_task_seq_id}} | {{runtime_job_id}}     |


## Details

{{#last_result}}

Total processing time: {{elapsed_time_human}}

| Task          | Status         | Result/Error              |
|---------------|----------------|---------------------------|
{{#messages}}{{#message}} | {{actor_name}}{{/message}} | {{status}} | {{#error}}`{{message}}`{{/error}}{{#result_list}}`{{.}}`<br/>{{/result_list}} |
{{/messages}}


## Traceback

{{#error}}{{#traceback}}    {{{.}}}
{{/traceback}}{{/error}}

{{/last_result}}

    """

    environ = Environ.from_dict(environ)
    notif = Notification.from_dict(notification)
    notif = notif | environ.notification

    failure_email = notif.failure.email

    if failure_email is None:
        raise EmailNotificationNotDefined(notif.failure)

    attachs = Status.from_dict(status).attachments()

    return email( status, 
        environ=environ.to_dict(), 
        from_addr=failure_email.from_,
        subject=subject,
        body=body,
        to_addrs=list(failure_email.to),
        cc_addrs=list(failure_email.cc),
        bcc_addrs=list(failure_email.bcc),
        attachments=attachs
    )



def email( last_result, *, 
    environ,
    from_addr, 
    subject, 
    body, 
    to_addrs=[], 
    cc_addrs=[], 
    bcc_addrs=[],
    html_body=None,
    attachments=[]
):
   
    assert len(to_addrs) > 0, u"No email recipient specified"

    environ = Environ.from_dict(environ)
    config = Config.from_dict( env.email_config() )

    data = environ.to_data()
    data[u'last_result'] = last_result
    data[u'email_from'] = str(from_addr)
    data[u'email_to'] = to_addrs
    data[u'email_cc'] = cc_addrs
    data[u'email_bcc'] = bcc_addrs

    subject = subject.format(**data)
    body = pystache.render(body, data)
    html_body = ( html_layout( HTML_HEADER, markdown( body ) ) if html_body is None else 
                  html_layout( HTML_HEADER, pystache.render( html_body, data) )
                )

    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = from_addr
    if len(to_addrs) > 0:
        msg['To'] = u', '.join(to_addrs)
    if len(cc_addrs) > 0:
        msg['Cc'] = u', '.join(cc_addrs)
    if len(bcc_addrs) > 0:
        msg['Bcc'] = u', '.join(bcc_addrs)
    msg.set_content(body)
    msg.add_alternative(html_body, subtype=u'html')

    for fname in attachments:
       (content_type, charset) = mimetypes.guess_type(fname, strict=False)
       if content_type is None:
           content_type = u'application/octet-stream'
       (maintype, subtype) = content_type.split(u'/')
       attachdata = b''

       try:
           with open(fname, 'rb') as f:
               attachdata = f.read()
       except OSError as e:
           logger.warn(u'Unable to open or read attachment file, skipping: %s (%s)' % (fname,str(e)))
           continue

       try:
           msg.add_attachment( 
               attachdata, maintype=maintype, subtype=subtype, filename=os.path.basename(fname) 
           )
       except Exception as e:
           logger.warn(u'Unable to attach file: %s (%s)' % (fname,str(e)))
           continue

    logger.info(u"Sending notification email '%s'" % (subject,))
    logger.debug( str(msg) )

    with SMTP( **config.to_dict() ) as s:
       s.send_message(msg)


""" Don't try this at home """
def html_layout(head,content):
    return u"<html><head>%s</head><body>%s</body></html>" % (head,content)

