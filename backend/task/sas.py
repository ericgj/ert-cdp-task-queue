from itertools import chain
import logging
import os
import os.path
import shutil
import re
from subprocess import Popen, TimeoutExpired, CalledProcessError

logger = logging.getLogger(__name__)

import env
from model.environ import Environ
import model.sas

class SasLogError(Exception):
    def __init__(self, log_file):
        self.attachments = [ log_file ]

    def __str__(self):
        return u"Error(s) detected in SAS log file. Please check log file: %s" % self.attachments[0]


class SasTransactionError(Exception):
    def __init__(self, procerr, log_file=None, out_file=None):
        self.procerr = procerr
        self.attachments = ( ( [] if log_file is None else [ log_file ] ) +
                             ( [] if out_file is None else [ out_file ] )
                           )

    def __str__(self):
        return u"Error in SAS execution (return code %d)" % (self.procerr.returncode,)


def run(last_result, *, environ, in_directory, program_file, log=None, sas_options=[]):

    sas_environ = model.sas.Environ.from_dict(env.sas_config())

    data = Environ.from_dict(environ).to_data()

    in_directory = in_directory.format(**data)
    program_file = program_file.format(**data)
    log_path = in_directory if log is None else log.format(**data)

    cmd = (
        [ os.path.basename(sas_environ.exe),
          os.path.join(in_directory, program_file)
        ] +
        option_pair(u"CONFIG", sas_environ.config) +
        option_pair(u"AUTOEXEC", sas_environ.autoexec) +
        option_pair(u"SASINITIALFOLDER", in_directory) +
        option_pair(u"LOG", log_path) +
        option_pair(u"PRINT", in_directory) +
        option_pair(u"SASUSER", sas_environ.sasuser ) +
        option_pair(u"WORK", sas_environ.work ) +
        list(
            chain.from_iterable([ option_pair(k,v) for (k,v) in sas_environ.options ])
        ) +
        list(
            chain.from_iterable([ [ option(k) ] for k in sas_environ.boolean_options ])
        ) +
        list(
            chain.from_iterable([ [ option(k) ] if v is None else option_pair(k,v) 
                for (k,v) in sas_options
            ])
        )
    )

    logger.info("Running SAS via cmd: %s" % cmd )

    with Popen( cmd, executable=sas_environ.exe, cwd=os.path.dirname(sas_environ.exe) ) as subproc:
        while True:
            try: 
                subproc.wait(timeout=30)
            
            except TimeoutExpired:
                logger.info("Note: SAS process still running")
                continue

            if subproc.returncode is None:
                logger.info("Note: SAS process still running")
                continue

            else:
                logger.info("SAS process return code: %d" % (subproc.returncode,))
                rc = abs(subproc.returncode)
                
                if rc == 0:
                    log_file = sas_log_file(log_path, program_file)
                    if os.path.exists(log_file):
                        if has_sas_log_errors( log_file ):
                            raise SasLogError(log_file=log_file)
                        else:
                            return [ u"Success (%d)" % (rc,) ]
                    else:
                        return [ 
                            u"Success (%d)." % (rc,),
                            u"Warning: log file not found: %s." % (log_file,) 
                        ]
                    
                elif rc == 1:
                    logger.warn("Note: SAS ran with warnings. Please check log files.")

                    log_file = sas_log_file(log_path, program_file)
                    if os.path.exists(log_file):
                        if has_sas_log_errors( log_file ):
                            raise SasLogError(log_file=log_file)
                        else:
                            return [ 
                                u"Success with warnings (%d)." % (rc,),
                                u"Please check SAS log files." 
                            ]
                    else:
                        return [ 
                            u"Success with warnings (%d)." % (rc,),
                            u"Warning: log file not found: %s." % (log_file,) 
                        ]
                
                else:
                    raise CalledProcessError(returncode=subproc.returncode,cmd=cmd)



def transaction_run(last_result, *, 
    environ, 
    in_directory, 
    program_file, 
    program_name,
    sub_dir=u'auto',
    last_result_var=None,
    vars=[],
    libnames=[],
    sas_options=[]
):
    
    last_result_value = None
    if not last_result_var is None:
        if isinstance(last_result,list):
            assert len(last_result) > 0, \
                u"No %s specified" % (last_result_var,)
            last_result_value = u','.join(last_result)

        else:
            assert isinstance(last_result,str), \
                u"Unknown type of %s: %s" % (last_result_var, last_result.__class__.__name__)
            last_result_value = last_result

    sas_environ = model.sas.Environ.from_dict(env.sas_config())
    environ = Environ.from_dict(environ)

    data = environ.to_data()
    in_directory = in_directory.format(**data)
    program_file = program_file.format(**data)
    program_name = program_name.format(**data)
    sub_dir = sub_dir.format(**data)
    vars = (
        [ (k, v.format(**data)) for (k,v) in vars ] + 
        ( [] if last_result_var is None else 
          [ (last_result_var, last_result_value) ]
        )
    )
    libnames = [ (k, v.format(**data)) for (k,v) in libnames ]

    main_program_file = '%s.sas' % (program_name,)
    
    ( tx_dir, tx_log_file, tx_out_file ) = (
        create_transaction_dir(
            root_dir=in_directory,
            timestamp=environ.runtime.string_started_at(),
            log_file=('%s.log' % (program_name,)),
            out_file=('%s.pdf' % (program_name,)),
            program_file=program_file,
            main_program_file=main_program_file,
            sub_dir=sub_dir,
            vars=[ (k,v) for (k,v) in environ.to_dict().items() 
                     if k in sas_environ.environ_vars 
                 ] + vars,
            libnames=libnames,
            sas_options=sas_options
        )
    )

    try:
        return run( [],
            environ=environ.to_dict(),
            in_directory=tx_dir,
            log=tx_log_file,
            program_file=main_program_file
        )

    except CalledProcessError as e:
        raise SasTransactionError(e, log_file=tx_log_file, out_file=tx_out_file)


def create_transaction_dir(*, 
    root_dir, 
    timestamp, 
    log_file, 
    out_file, 
    program_file, 
    main_program_file=u'main.sas',
    sub_dir=u'', 
    vars=[], 
    libnames=[],
    sas_options=[]
): 
    tx_dir = os.path.normpath( os.path.join(root_dir, sub_dir, timestamp) )
    tx_main_program_file =  os.path.normpath( os.path.join( tx_dir, main_program_file ) )
    tx_log_file =  os.path.normpath( os.path.join( tx_dir, u'log', log_file ) )
    tx_out_file =  os.path.normpath( os.path.join( tx_dir, u'output', out_file ) )
    tx_libnames = [ (k, os.path.normpath(os.path.join(tx_dir, v))) for (k,v) in libnames ]

    # Create directories if they don't exist
    logger.info(u"Creating transaction directory if it doesn't exist: %s" % (tx_dir,))

    mkdir_p( tx_dir )
    mkdir_p( os.path.dirname( tx_main_program_file ) )
    mkdir_p( os.path.dirname( tx_log_file ) )
    mkdir_p( os.path.dirname( tx_out_file ) )
    for (_, lib) in tx_libnames:
        mkdir_p( lib )

    src_program_file = os.path.join( root_dir, program_file )
    if os.path.exists( src_program_file ):
        tx_program_file = src_program_file
    else:
        tx_program_file = os.path.normpath( program_file )

    ## Copy program file to tx_dir, if it exists 
    ## NOTE: currently disabled. Existing program file is used directly.
    # if os.path.exists( src_program_file ):
    #     logger.info(u"Copying program: %s" % (program_file,))
    #     tx_program_file =  os.path.normpath( os.path.join( tx_dir, program_file ) )
    #     mkdir_p( os.path.dirname( tx_program_file ) )
    #     cp_p( src_program_file, tx_program_file )
    # else:
    #    tx_program_file = os.path.normpath( program_file )

    # Create main program
    logger.info(u"Generating main program: %s" % (main_program_file,))

    lines = (
        [
          macro_var_assign(u'outfile', tx_out_file),
          u''
        ] +
        [ u'ODS PDF CLOSE;',
          u'ODS PDF FILE = "&outfile";',
          u''
        ] +
        [ macro_var_assign(u'timestamp', timestamp),
          macro_var_assign(u'root', tx_dir),
          u''
        ] +
        [ macro_var_assign(k,v) for (k,v) in vars ] +
        [ u'' ] +
        [ sas_option_assign(k,v) for (k,v) in sas_options ] +
        [ u'' ] +
        [ libname_assign(k, u'&root\\%s' % (v,)) for (k,v) in libnames ] +
        [ u'' ] +
        [ sas_include(tx_program_file),
          u''
        ] + 
        [ u'ODS PDF CLOSE;'
        ]
    )

    with open( tx_main_program_file, 'w' ) as f:
        for l in lines:
            f.write(l + "\r\n")

    return ( tx_dir, tx_log_file, tx_out_file )
          


def option_pair(key, value):
    return [u"-%s" % (key,), value]

def option(key):
    return u"-%s" % (key,)

def set_option_pair(key, value):
    return [u"-set", key, value]

def macro_var_assign(key, value):
    return u"%%LET %s = %s;" % (key, value)  

def libname_assign(key, value):
    return u'LIBNAME %s "%s";' % (key, value)

def sas_option_assign(key, value):
    return u'OPTIONS %s %s;' % (key, value)

def sas_include(filename):
    return u'%%INCLUDE "%s";' % (filename,)


def sas_log_file(log_path, program_name):
    if os.path.isdir(log_path):
        return os.path.join( 
            log_path, 
            os.path.splitext(os.path.basename(program_name))[0] + u".log" 
        )
    else:
        return log_path

def has_sas_log_errors(log_file):
    errs = re.compile(
        u'|'.join([
            u'_ERROR_=[1-9]',
            u'LOST CARD',
            u'SAS went to a new line when INPUT',
            u'Limit set by ERRORS= option reached'
        ])
    )
    found = False
    with open(log_file, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            if not re.search(errs, line) is None:
                found = True
                break

    return found


def mkdir_p(name):
    return os.makedirs(name, exist_ok=True)

def cp_p(src, dst):
    return shutil.copy2(src, dst)

