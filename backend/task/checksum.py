import os
import os.path
import hashlib

from model.environ import Environ

def md5(last_result, *, environ, file_name, to_directory):
    return checksum( last_result, 
        environ=environ,
        algorithm=u"md5", 
        file_name=file_name, 
        to_directory=to_directory
    )

def checksum( last_result, *, environ, algorithm, file_name, to_directory ):
    """ 
    Note the output mimicks Windows `CertFile -hashfile` format
    """
    assert (not last_result is None), u"No files to calculate checksums on"
    if isinstance(last_result, list):
        assert len(last_result) > 0, u"No files to calculate checksums on"
    else:
        assert isinstance(last_result,str), \
            u"Unknown type of last result: %s" % (last_result.__class__.__name__)
        last_result = [ last_result ]

    environ = Environ.from_dict(environ)
    data = environ.to_data()
    
    file_name = file_name.format(**data)
    to_directory = to_directory.format(**data)
    output_file = os.path.join( to_directory, file_name )

    os.makedirs(to_directory, exist_ok=True)

    with open( output_file, 'w' ) as output:
        for input_file in last_result:
            hash = hash_file(input_file, algorithm)

            output.write(u"%s hash of file %s:\r\n" % 
                (algorithm.upper(), os.path.normpath(input_file))
            )
            output.write(" ".join( chunks(hash.hexdigest(), 2) ) + "\r\n\r\n")

    return [ output_file ]


def hash_file(file, algorithm):
    hash = None
    with open( file, 'rb' ) as input:
        hash = hashlib.new( algorithm )
        while True:
            newchunk = input.read(1024)
            if len(newchunk) == 0:
                break
            else:
                hash.update(newchunk)
    return hash


def chunks(seq, n):
    n = max(1,n)
    return ( seq[i:i+n] for i in range(0,len(seq),n) )

        
