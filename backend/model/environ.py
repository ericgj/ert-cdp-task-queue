from collections import namedtuple

from timestamp import to_utc_datetime, utc_string, utc_human
from model.notification import Notification

def utc_datetime_timestamp(ts):
    return None if ts is None else to_utc_datetime(ts / 1000)

def string_timestamp(ts):
    return None if ts is None else utc_string(ts / 1000)

def human_timestamp(ts):
    return None if ts is None else utc_human(ts / 1000)


class Environ( namedtuple(u"Environ", 
                 ( u"client", u"protocol", u"account", u"datatype", u"location",
                   u"notification", u"runtime"
                 ) 
               ) 
):

    @classmethod
    def from_dict(cls, d):
        runtime = d.get(u'runtime')
        notif = d.get(u'notification')
        return cls(
            client = d[u'client'],
            protocol = d[u'protocol'],
            account = d[u'account'],
            datatype = d[u'datatype'],
            location = d[u'location'],
            notification = None if notif is None else Notification.from_dict(notif),
            runtime = None if runtime is None else Runtime.from_dict(runtime)
        )

    def __new__(cls, *, client, protocol, account, datatype, location, 
                notification=None, runtime=None):
        return super().__new__(cls,
            client, protocol, account, datatype, location, notification, runtime
        )

    def set_runtime(self, rt):
        return self._replace( runtime = rt )
    
    def pipeline_id(self):
        if self.runtime is None:
            return None
        return self.runtime.pipeline_id()
    
    def to_dict(self):
        """ TODO: make this a case-insensitive dict? """
        return { 
            u'client': self.client,
            u'protocol': self.protocol,
            u'account': self.account,
            u'datatype': self.datatype,
            u'location': self.location,
            u'notification': None if self.notification is None else self.notification.to_dict(),
            u'runtime': None if self.runtime is None else self.runtime.to_dict()
          }

    def to_data(self):
        return dict(
            [ (k,v) for (k,v) in self.to_dict().items() 
                if not k in [ u'runtime', u'notification'] 
            ] +
            ( [] if self.runtime is None else
              [ (u'runtime_' + k, v) for (k,v) in self.runtime.to_data().items() ]
            ) +
            ( [] if self.notification is None else
              [ (u'notification_' + k, v) for (k,v) in self.notification.to_data().items() ]
            )
        )


class Runtime( namedtuple("Runtime", 
                 ( u"task_seq_id", u"job_id", u"timestamp_started_at")
               )
):
    
    @classmethod
    def from_dict(cls, d):
        return cls(
            task_seq_id = d[u'task_seq_id'],
            job_id = d[u'job_id'],
            timestamp_started_at = d[u'timestamp_started_at']
        )

    def __new__(cls, *, task_seq_id, job_id, timestamp_started_at=None):
        return super().__new__(cls, task_seq_id, job_id, timestamp_started_at)

    def string_started_at(self):
       return string_timestamp(self.timestamp_started_at)

    def human_started_at(self):
       return human_timestamp(self.timestamp_started_at)

    def utc_datetime_started_at(self):
        return utc_datetime_timestamp(self.timestamp_started_at)

    def started_at(self, ts):
        return self._replace( timestamp_started_at = ts )

    def pipeline_id(self):
        return ( self.task_seq_id, self.job_id, self.timestamp_started_at )

    def to_dict(self):
        """ TODO: make this a case-insensitive dict? """
        return {
            u"task_seq_id": self.task_seq_id,
            u"job_id": self.job_id,
            u"timestamp_started_at": self.timestamp_started_at,
            u"string_started_at": self.string_started_at(),
            u"human_started_at": self.human_started_at()
        }

    # Note: not JSON encodable -- use for string formatting dates
    def to_data(self):
        data = self.to_dict()
        data[u'utc_datetime_started_at'] = self.utc_datetime_started_at()
        return data

