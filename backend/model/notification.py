from collections import namedtuple


class Notification( namedtuple(u'Notification', (u'failure', u'success')) ):

    @classmethod
    def from_dict(cls, d):
        return cls(
            failure=NotificationOptions.from_dict(d.get(u'failure',{})),
            success=NotificationOptions.from_dict(d.get(u'success',{}))
        )

    def __new__(cls, *, failure, success):
        return super().__new__(cls, failure, success)

    def __or__(self, other):
       return self._replace(
           failure=self.failure | other.failure,
           success=self.success | other.success
       )

    def to_dict(self):
        return {
          u'failure': self.failure.to_dict(),
          u'success': self.success.to_dict()
        }

    def to_data(self):
        return dict(
            [ (u'failure_' + k, v) for (k,v) in self.failure.to_data().items() ] +
            [ (u'success_' + k, v) for (k,v) in self.success.to_data().items() ]
        )

class NotificationOptions( namedtuple(u'NotificationOptions', (u'email')) ):

    @classmethod
    def from_dict(cls, d):
        email = d.get(u'email')
        return cls(
            email=None if email is None else Email.from_dict(email)
        )

    def __new__(cls, *, email=None):
        return super().__new__(cls, email)

    def __or__(self, other):
        return self._replace(
            email = ( (self.email | other.email) 
                          if not self.email is None and not other.email is None else
                      (other.email if not other.email is None else self.email)
                    )
        )

    def to_dict(self):
        return {
          u'email': None if self.email is None else self.email.to_dict()
        }

    def to_data(self):
        return dict(
            ( [] if self.email is None else
              [ (u'email_' + k, v) for (k, v) in self.email.to_dict().items() ]
            )
        )

class Email( namedtuple(u'Email', (u'from_', u'to', u'cc', u'bcc') ) ):

    @classmethod
    def from_dict(cls, d):
        return cls(
            from_=d.get(u'from'),
            to=set(d.get(u'to',[])),
            cc=set(d.get(u'cc',[])),
            bcc=set(d.get(u'bcc',[]))
        )

    def __new__(cls, *, from_=None, to=None, cc=None, bcc=None):
        return super().__new__(cls, from_, to or set(), cc or set(), bcc or set())

    def __or__(self, other):
        return self._replace(
            from_=other.from_ if not other.from_ is None else self.from_,
            to=self.to | other.to,
            cc=self.cc | other.cc,
            bcc=self.bcc | other.bcc
        )

    def to_dict(self):
        return {
          u'from': self.from_,
          u'to': list(self.to),
          u'cc': list(self.cc),
          u'bcc': list(self.bcc)
        }


