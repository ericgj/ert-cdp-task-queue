import json

from model.environ import Environ

""" Note: these assume validation has been run on input """

class TaskSeq:

    @classmethod
    def from_dict(cls, d):
        return cls( 
            environ=Environ.from_dict(d[u'environ']),
            tasks=[ Task.from_dict(t) for t in d[u'tasks'] ]
        )

    def __init__(self, environ, tasks):
        self.environ = environ
        self.tasks = tasks

    def to_dict(self):
        return {
          u'environ': self.environ.to_dict(),
          u'tasks': [ t.to_dict() for t in self.tasks ]
        }

    def to_params(self):
        return [ t.to_params() for t in self.tasks ]


class Task:

    @classmethod
    def from_dict(cls, d):
        ctor = d[u'ctor']
        value = d[u'value']
        return cls( ctor=ctor, params=[ Param.from_dict(p) for p in value ] )

    def __init__(self, ctor, params):
        self.ctor = ctor
        self.params = params

    def to_dict(self):
        return {
            u'ctor': self.ctor,
            u'value': [
                p.to_dict() for p in self.params
            ]
        }

    def to_params(self):
        return dict( p.to_param() for p in self.params )


class Param:

    @classmethod
    def from_dict(cls, d):
        ctor = d['ctor']
        (name, value) = d['value']
        return cls( name, ctor, value )

    def __init__(self, name, ctor, data):
        self.name = name
        self.ctor = ctor
        self.data = data

    def to_dict(self):
       return {
           u'ctor': self.ctor,
           u'value': [ self.name, self.data ]
       }

    def to_param(self):
        return (self.name, self.data)


class Created:

   @classmethod
   def from_dict(cls, d):
       return cls(d[u'id'])

   def __init__(self, id):
       self.id = id

   def to_dict(self):
       return { u'id': self.id }


