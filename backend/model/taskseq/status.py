from collections import namedtuple
from itertools import chain

from timestamp import to_utc_datetime, utc_string, utc_human
from model.environ import Environ
from model.taskseq.schedule import Schedule

NOT_STARTED = u'NOT STARTED'
STARTED = u'STARTED'
FAILURE = u'FAILURE'
SUCCESS = u'SUCCESS'

class Status( namedtuple(u"Status", 
                  (u"task_seq_id", u"job_id", u"seq_timestamp", u"environ", u"messages")
              ) 
):

    @classmethod
    def from_dict(cls, d):
        job_id = d[u'job_id']
        task_seq_id = d[u'task_seq_id']
        seq_timestamp = d[u'seq_timestamp']
        environ = d[u'environ']
        messages = d[u'messages']
        return cls(
            task_seq_id=task_seq_id,
            job_id=job_id,
            seq_timestamp=seq_timestamp,
            environ=None if environ is None else Environ.from_dict(environ),
            messages=[ MessageStatus.from_dict(m) for m in messages ]
        )

    def __new__(cls, *, task_seq_id, job_id, seq_timestamp, environ, messages):
        return super().__new__(cls, task_seq_id, job_id, seq_timestamp, environ, messages)

    def is_run(self):
        return len(self.messages) > 0

    def is_success(self):
        return self.status() == SUCCESS

    def is_failure(self):
        return self.status() == FAILURE

    def status(self):
        if len(self.messages) == 0:
            return None

        substats = [ m.status for m in self.messages ]
        if FAILURE in substats:
            return FAILURE
        elif NOT_STARTED in substats:
            return NOT_STARTED
        elif STARTED in substats:
            return STARTED
        else:
            return SUCCESS

    """ Note: assumes messages are in order """
    def last_message(self):
        return None if len(self.messages) == 0 else self.messages[-1]

    def first_failure_message(self):
        try:
            return next( m for m in self.messages if m.is_failure() )
        except StopIteration:
            return None

    def result(self):
        msg = self.last_message()
        return None if msg is None else msg.result

    def error(self):
        msg = self.first_failure_message()
        return None if msg is None else msg.error

    def elapsed_seconds(self):
        if len(self.messages) == 0:
            return None

        return ( ( max( t.timestamp for t in self.messages ) -
                   min( t.timestamp for t in self.messages )
                 ) / 1000
               ) 

    def elapsed_minutes(self):
        secs = self.elapsed_seconds()
        return ( None if secs is None else
                 secs / 60.0
               )

    def elapsed_time_human(self):
        secs = self.elapsed_seconds()
        return ( None if secs is None else
                 "%d min %d sec" % ( int(secs) // 60, round(secs) % 60 )
               )

    def string_timestamp(self):
        return ( None if self.seq_timestamp is None else
                 utc_string(self.seq_timestamp / 1000)
               )

    def human_timestamp(self):
        return ( None if self.seq_timestamp is None else
                 utc_human(self.seq_timestamp / 1000)
               )

    def utc_datetime_timestamp(self):
        return ( None if self.seq_timestamp is None else
                 utc_datetime_timestamp(self.seq_timestamp / 1000)
               )

    def attachments(self):
        return list(
            chain.from_iterable([ m.attachments() for m in self.messages ])
        )

    def to_dict(self):
        is_success = self.is_success()
        is_failure = self.is_failure()
        result = self.result()
        err = self.error()
        return {
            u'task_seq_id': self.task_seq_id,
            u'job_id': self.job_id,
            u'seq_timestamp': self.seq_timestamp,
            u'string_timestamp': self.string_timestamp(),
            u'human_timestamp': self.human_timestamp(),
            u'environ': None if self.environ is None else self.environ.to_dict(),
            u'messages': [ m.to_dict() for m in self.messages ],
            u'is_run': self.is_run(),
            u'is_success': is_success,
            u'is_failure': is_failure,
            u'status': self.status(),
            u'result': None if not is_success else result,
            u'result_string': u'' if not is_success else repr(result), 
            u'result_list': ( [] if not is_success else 
                              ( result if isinstance(result,list) else [result] )
                            ),
            u'error':  None if not is_failure else 
                       ( None if not err else err.to_dict() ),
            u'elapsed_seconds': self.elapsed_seconds(),
            u'elapsed_minutes': self.elapsed_minutes(),
            u'elapsed_time_human': self.elapsed_time_human(),
            u'attachments': self.attachments()
        }

    
    # Note: not JSON encodable -- use for string formatting dates
    def to_data(self):
        data = self.to_dict()
        data[u'utc_datetime_timestamp'] = self.utc_datetime_timestamp()
        data[u'messages'] = [ m.to_data() for m in self.messages ]
        return data



class MessageStatus( namedtuple(u"MessageStatus", 
                        ( u"task_seq_id",
                          u"job_id", 
                          u"seq_timestamp", 
                          u"message_id", 
                          u"status", 
                          u"timestamp", 
                          u"message", 
                          u"result", 
                          u"error" )
                   )
):

    @classmethod
    def from_dict(cls, d):
        result = d.get(u'result')
        error = d.get(u'error', result)
        return cls(
            task_seq_id=d[u'task_seq_id'],
            job_id=d[u'job_id'],
            seq_timestamp=d[u'seq_timestamp'],
            message_id=d[u'message_id'],
            status=d[u'status'],
            timestamp=d[u'status_timestamp'],
            message=d[u'message'],
            result=None if d[u'status'] == FAILURE else result,
            error=None if not d[u'status'] == FAILURE else MessageError.from_dict(error)
        )

    def __new__(cls, *, 
        task_seq_id,
        job_id,
        seq_timestamp,
        message_id,
        status,
        timestamp,
        message,
        result,
        error
    ):
        return super().__new__(cls, 
            task_seq_id,
            job_id, 
            seq_timestamp, 
            message_id, 
            status, 
            timestamp, 
            message, 
            result, 
            error
        )

    def is_success(self):
        return self.status == SUCCESS

    def is_failure(self):
        return self.status == FAILURE

    def string_timestamp(self):
        return ( None if self.timestamp is None else
                 utc_string(self.timestamp / 1000)
               )

    def human_timestamp(self):
        return ( None if self.timestamp is None else
                 utc_human(self.timestamp / 1000)
               )

    def utc_datetime_timestamp(self):
        return ( None if self.timestamp is None else
                 utc_datetime_timestamp(self.timestamp / 1000)
               )

    def attachments(self):
        return [] if self.error is None else self.error.attachments()

    def to_dict(self):
        is_success = self.is_success()
        is_failure = self.is_failure()
        return {
            u'task_seq_id': self.task_seq_id,
            u'job_id': self.job_id,
            u'seq_timestamp': self.seq_timestamp,
            u'message_id': self.message_id,
            u'status': self.status,
            u'status_timestamp': self.timestamp,
            u'string_timestamp': self.string_timestamp(),
            u'human_timestamp': self.human_timestamp(),
            u'message': self.message,
            u'result': None if not is_success else self.result,
            u'result_string': u'' if not is_success else repr(self.result),
            u'result_list': ( [] if not is_success else 
                              ( self.result if isinstance(self.result,list) else [self.result] )
                            ),
            u'error': None if not is_failure else MessageError.to_dict(self.error),
            u'error_message': self.error.message if is_failure else None,
            u'is_success': is_success,
            u'is_failure': is_failure,
            u'attachments': self.attachments()
        }

    # Note: not JSON encodable -- use for string formatting dates
    def to_data(self):
        data = self.to_dict()
        data[u'utc_datetime_timestamp'] = self.utc_datetime_timestamp()
        return data


class MessageError( namedtuple(u"MessageError", ( u"ctor", u"message", u"traceback", u"data" )) ):
    @classmethod
    def from_dict(cls, d):
        return cls(
            ctor=d[u'type'],
            message=d[u'message'],
            traceback=d.get(u'traceback',[]),
            data=d.get(u'data',{})
        )

    def __new__(cls, *, ctor, message, traceback=[], data={}):
        return super().__new__(cls, ctor, message, traceback, data)

    def attachments(self):
        return self.data.get(u'attachments',[])

    def to_dict(self):
        return {
            u'type': self.ctor,
            u'message': self.message,
            u'traceback': self.traceback,
            u'data': self.data,
            u'attachments': self.attachments()
        }


