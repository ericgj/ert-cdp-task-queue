from collections import namedtuple
from datetime import datetime
from calendar import timegm
from pytz import utc
from apscheduler.util import convert_to_datetime
from apscheduler.triggers.cron import CronTrigger

class Schedule( namedtuple("Schedule", (u"job_id", u"job_name", u"trigger", u"next_run_time")) ):

    """ 
    Note next_run_time is stored internally and output (to_dict) in milliseconds
    But converted from datetime when loaded from Job 
    """
    @classmethod
    def from_job(cls, job):
        return cls(
            job_id=job.id,
            job_name=job.name,
            trigger=job.trigger,
            next_run_time= ( timegm( job.next_run_time.utctimetuple() ) * 1000 ) 
        )
        
    @classmethod
    def from_dict(cls, d):
        return cls(
            job_id=d[u'job_id'],
            job_name=d[u'job_name'],
            trigger=CronTrigger(**d[u'trigger']),
            next_run_time=d[u'next_run_time']
        )

    def __new__(cls, *, job_id, job_name, trigger, next_run_time):
        return super().__new__(cls, 
            job_id, job_name, trigger, next_run_time
        )

    def to_dict(self):
        return {
            u'job_id': self.job_id,
            u'job_name': self.job_name,
            u'trigger': dict( (f.name, str(f)) for f in self.trigger.fields if not f.is_default ),
            u'next_run_time': self.next_run_time
        }


class Params:
    """ 
    Simple container for CronTrigger arguments. Basically scrubs them for valid
    values and passes them through.
    """

    @classmethod
    def from_dict(cls, d):
        scrubbed = dict(
            (k,coerce_param(k,v)) for (k,v) in d.items() 
                if k in ( list(CronTrigger.FIELD_NAMES) + 
                              ['timezone', 'jitter', 'start_date', 'end_date'])
        )
        return cls(**scrubbed)

    def __init__(self, start_date=None, end_date=None, **kwargs):
        self.start_date = start_date 
        self.end_date = end_date 
        self.fields = kwargs

    def to_dict(self):
        return dict( 
            [ (k,v) for (k,v) in self.fields.items() if not v is None ] + 
            [ (u'start_date', ( self.start_date if self.start_date is None else
                                self.start_date.strftime('%Y-%m-%dT%H:%M:%S'))
              ),
              (u'end_date', ( self.end_date if self.end_date is None else
                              self.end_date.strftime('%Y-%m-%dT%H:%M:%S'))
              )
            ]
        )

def coerce_param(k,v):
    if k == 'jitter':
        return float(v)
    elif (k == 'start_date' or k == 'end_date'):
        if v is None:
            return v
        else:
            return datetime.strptime(v, '%Y-%m-%dT%H:%M:%S')
    else:
        return v


class Created:

    @classmethod
    def from_dict(cls,d):
        return cls(
            taskseq_id = d[u'taskseq_id'],
            id = d[u'id']
        )

    def __init__(self, taskseq_id, id):
        self.taskseq_id = taskseq_id
        self.id = id

    def to_dict(self):
        return {
            u'taskseq_id': self.taskseq_id,
            u'id': self.id
        }



