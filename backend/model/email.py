from collections import namedtuple

class Config( namedtuple('Config', 
                  ( u'host', u'port', u'local_hostname', u'timeout', u'source_address')
              )
):
    @classmethod
    def from_dict(cls, d):
        return cls(
            host=d[u'host'],
            port=d[u'port'],
            local_hostname=d.get(u'local_hostname',None),
            timeout=d.get(u'timeout',None),
            source_address=d.get(u'source_address',None)
        )
            
    
    def __new__(cls, *, host, port, local_hostname=None, timeout=None, source_address=None):
        return super().__new__(cls, host, port, local_hostname, timeout, source_address)

    def to_dict(self):
        ret = {
          u'host': self.host,
          u'port': self.port,
          u'local_hostname': self.local_hostname,
          u'source_address': self.source_address
        }
        if not self.timeout is None:
          ret[u'timeout'] = self.timeout

        return ret


