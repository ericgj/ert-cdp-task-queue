from collections import namedtuple
import os.path

class Environ( namedtuple(u"Environ", 
                 ( u"exe", u"config", u"autoexec", u"mysasfiles", u"sasuser", u"work",
                   u"options", u"boolean_options", u"environ_vars" ) 
               ) 
):

    @classmethod
    def from_dict(cls, d):
        return cls(
            exe = os.path.expandvars( d[u'exe'] ),
            config = os.path.expandvars( d[u'config'] ),
            autoexec = os.path.expandvars( d[u'autoexec'] ),
            mysasfiles = os.path.expandvars( d[u'mysasfiles'] ),
            sasuser = os.path.expandvars( d[u'sasuser'] ),
            work = os.path.expandvars( d[u'work'] ),
            options = d.get(u'options', []),
            boolean_options = d.get(u'boolean_options', []),
            environ_vars = d.get(u'environ_vars', [])
        )

    def __new__(cls, *, exe, config, autoexec, mysasfiles, sasuser, work, 
                options=[], boolean_options=[], environ_vars=[]):
        return super().__new__(cls,
            exe, config, autoexec, mysasfiles, sasuser, work, 
            options, boolean_options, environ_vars
        )


        
