from collections import namedtuple
from enum import Enum

from timestamp import utc_human

DATE_UNIT = Enum(u'DATE_UNIT', (u'seconds',u'minutes',u'hours',u'days'))
DATE_COMPARISON = Enum(u'DATE_COMPARISON', (u'before',u'after'))
DATE_RANGE = Enum(u'DATE_RANGE', (u'within',u'at_least'))

SIZE_UNIT = Enum(u'SIZE_UNIT', (u'kb', u'mb', u'gb'))
SIZE_COMPARISON = Enum(u'SIZE_COMPARISON', (u'lt', u'lteq', u'gteq', u'gt'))

class FileDateExpr( namedtuple(u'FileDateExpr', 
                    (u'quantity', u'unit', u'comparison', u'range') )
):

    @classmethod
    def from_dict(cls, d):
        return cls(
            quantity = d[u'quantity'],
            unit = decode_date_unit(d[u'unit']),
            comparison = decode_date_comparison(d[u'comparison']),
            range = decode_date_range(d[u'range'])
        )


    def __new__(cls, *, quantity, unit, comparison, range):
        return super().__new__(cls, quantity, unit, comparison, range)

    def evaluate(self, ts0, ts1):
        factor = date_unit_seconds(self.unit)
        diff = ts1 - ts0
        return date_compare(self.comparison, self.range, self.quantity * factor, diff)

    def assert_true(self, ts0, ts1):
        assert self.evaluate(ts0, ts1), u'%s is not %s %s' % ( 
            utc_human(ts1), str(self), utc_human(ts0)
        )
        
    def to_dict(self):
        return {
          u'quantity': self.quantity,
          u'unit': encode_date_unit(self.unit),
          u'comparison': encode_date_comparison(self.comparison),
          u'range': encode_date_range(self.range)
        }

    def __str__(self):
        return u'%s %d %s %s' % ( 
            encode_date_range(self), 
            self.quantity,
            encode_date_unit(self),
            encode_date_comparison(self)
        )

class FileSizeExpr( namedtuple(u'FileSizeExpr', 
                    (u'quantity', u'unit', u'comparison') )
):

    @classmethod
    def from_dict(cls, d):
        return cls(
            quantity = d[u'quantity'],
            unit = decode_size_unit(d[u'unit']),
            comparison = decode_size_comparison(d[u'comparison']),
        )


    def __new__(cls, *, quantity, unit, comparison):
        return super().__new__(cls, quantity, unit, comparison)

    def evaluate(self, sz):
        factor = size_unit_bytes(self.unit)
        return size_compare(self.comparison, self.quantity * factor, sz)

    def assert_true(self, sz):
        assert self.evaluate(sz), u'%d bytes is not %s' % ( 
            sz, str(self)
        )
        
    def to_dict(self):
        return {
          u'quantity': self.quantity,
          u'unit': encode_size_unit(self.unit),
          u'comparison': encode_size_comparison(self.comparison)
        }

    def __str__(self):
        return u'%s %d %s' % ( 
            encode_size_comparison(self),
            self.quantity,
            encode_size_unit(self)
        )




# Decoding

def decode_date_unit(raw):
    if raw.lower() == u'seconds':
        return DATE_UNIT.seconds
    elif raw.lower() == u'minutes':
        return DATE_UNIT.minutes
    elif raw.lower() == u'hours':
        return DATE_UNIT.hours
    elif raw.lower() == u'days':
        return DATE_UNIT.days
    else:
        raise KeyError(raw)

def decode_date_comparison(raw):
    if raw.lower() == u'before':
        return DATE_COMPARISON.before
    elif raw.lower() == u'after':
        return DATE_COMPARISON.after
    else:
        raise KeyError(raw)

def decode_date_range(raw):
    if raw.lower() == u'within':
        return DATE_RANGE.within
    elif raw.lower() == u'at least':
        return DATE_RANGE.at_least
    else:
        raise KeyError(raw)


def decode_size_unit(raw):
    if raw.lower() == u'kb':
        return SIZE_UNIT.kb
    elif raw.lower() == u'mb':
        return SIZE_UNIT.mb
    elif raw.lower() == u'gb':
        return SIZE_UNIT.gb
    else:
        raise KeyError(raw)

def decode_size_comparison(raw):
    if raw.lower() == u'<':
        return SIZE_COMPARISON.lt
    elif raw.lower() == u'<=':
        return SIZE_COMPARISON.lteq
    elif raw.lower() == u'>':
        return SIZE_COMPARISON.gt
    elif raw.lower() == u'>=':
        return SIZE_COMPARISON.gteq
    else:
        raise KeyError(raw)


# Encoding

def encode_date_unit(expr):
    if expr.unit == DATE_UNIT.seconds:
        return u'seconds'
    elif expr.unit == DATE_UNIT.minutes:
        return u'minutes'
    elif expr.unit == DATE_UNIT.hours:
        return u'hours'
    elif expr.unit == DATE_UNIT.days:
        return u'days'
    else:
        raise KeyError(str(expr.unit))

def encode_date_comparison(expr):
    if expr.comparison == DATE_COMPARISON.before:
        return u'before'
    elif expr.comparison == DATE_COMPARISON.after:
        return u'after'
    else:
        raise KeyError(str(expr.comparison))

def encode_date_range(expr):
    if expr.range == DATE_RANGE.within:
        return u'within'
    elif expr.range == DATE_RANGE.at_least:
        return u'at least'
    else:
        raise KeyError(str(expr.range))

def encode_size_unit(expr):
    if expr.unit == SIZE_UNIT.kb:
        return u'KB'
    elif expr.unit == SIZE_UNIT.mb:
        return u'MB'
    elif expr.unit == SIZE_UNIT.gb:
        return u'GB'
    else:
        raise KeyError(str(expr.unit))

def encode_size_comparison(expr):
    if expr.comparison == SIZE_COMPARISON.lt:
        return u'<'
    elif expr.comparison == SIZE_COMPARISON.lteq:
        return u'<='
    elif expr.comparison == SIZE_COMPARISON.gt:
        return u'>'
    elif expr.comparison == SIZE_COMPARISON.gteq:
        return u'>='
    else:
        raise KeyError(str(expr.comparison))


# multi functions

def date_compare(comparison, range, expected, diff):
    if comparison == DATE_COMPARISON.before:
        if range == DATE_RANGE.within:
            return (diff >= 0 and expected > diff)
        elif range == DATE_RANGE.at_least:
            return (diff >= 0 and expected <= diff)
        else:
            return False
    elif comparison == DATE_COMPARISON.after:
        if range == DATE_RANGE.within:
            return (diff <= 0 and expected > (-1 * diff) )
        elif range == DATE_RANGE.at_least:
            return (diff <= 0 and expected <= (-1 * diff) )
        else:
            return False

def size_compare(comparison, expected, sz):
    if comparison == SIZE_COMPARISON.lt:
        return sz < expected
    elif comparison == SIZE_COMPARISON.lteq:
        return sz <= expected
    elif comparison == SIZE_COMPARISON.gt:
        return sz > expected
    elif comparison == SIZE_COMPARISON.gteq:
        return sz >= expected
    else:
        return False


def date_unit_seconds(unit):
    if unit == DATE_UNIT.seconds:
        return 1
    elif unit == DATE_UNIT.minutes:
        return 60
    elif unit == DATE_UNIT.hours:
        return 3600
    elif unit == DATE_UNIT.days:
        return 3600 * 24
    else:
        raise KeyError(str(unit))

def size_unit_bytes(unit):
    if unit == SIZE_UNIT.kb:
        return 1000
    elif unit == SIZE_UNIT.mb:
        return 1000**2
    elif unit == SIZE_UNIT.gb:
        return 1000**3
    else:
        raise KeyError(str(unit))


