import time
from collections import OrderedDict

""" Random functions that python should have built in """

def current_timestamp():
    return int(time.time() * 1000)


def group_by( xs, getter ):
  ret = OrderedDict()
  for x in xs:
      key = getter(x)
      accum = ret.get(key,[])
      accum.append(x)
      ret[key] = accum
  return ret


def dict_merge(a,b):
   c = b.copy()
   c.update(a)
   return c


def take(n, iterable):
   i=0
   for x in iterable:
       if i < n:
           i = i + 1
           yield x
       else:
           break


"""
Recursive to_dict from 
https://stackoverflow.com/questions/1036409/recursively-convert-python-object-graph-to-dictionary#1118038
"""
def to_dict(obj, classkey=None):
    if isinstance(obj, dict):
        data = {}
        for (k, v) in obj.items():
            data[k] = to_dict(v, classkey)
        return data
    elif hasattr(obj, "_ast"):
        return to_dict(obj._ast())
    elif hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [to_dict(v, classkey) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, to_dict(value, classkey)) 
            for key, value in obj.__dict__.items() 
            if not callable(value) and not key.startswith('_')])
        if classkey is not None and hasattr(obj, "__class__"):
            data[classkey] = obj.__class__.__name__
        return data
    else:
        return obj
