import logging

import requests

from model.environ import Environ
from model.taskseq import TaskSeq
from model.taskseq.schedule import Schedule
from model.taskseq.status import Status
from cli.util import format_timestamp

logger = logging.getLogger(__name__)


def cmd( hostport, ids, report ):
    host, port = hostport
    taskseq_id, sched_id = ids

    taskseq_read = requests.get( taskseq_read_url(host,port,taskseq_id) )
    logger.debug(taskseq_read.text)
    taskseq_read.raise_for_status()    # TODO handle gracefully

    taskseq_status = (
        requests.get( taskseq_status_list_url(host,port,taskseq_id),
            { u'job': sched_id }             
        )
    )
    logger.debug(taskseq_status.text)
    taskseq_status.raise_for_status()    # TODO handle gracefully

    taskseq, jobs = parsed_taskseq_read( taskseq_read.json() )
    results = parsed_taskseq_status_list( taskseq_status.json() )

    if len(results) == 0:
        report( (taskseq_id,None), taskseq.environ,
            [ [ u"Next run time: %s  (job: %s)" % ( format_timestamp(job.next_run_time), job.job_id )
                  for job in jobs
              ] +
              [ u"Not yet run" ]
            ]
        )

    else:
        report( (taskseq_id,None), taskseq.environ, 
            ( [ u"Next run time: %s  (job: %s)" % ( format_timestamp(job.next_run_time), job.job_id )
                  for job in jobs
              ] +
              [ [ status_summary(s) ] + 
                [ status_detail(i,m) for (i,m) in enumerate(s.messages) ]
                for s in results
              ]
            )
        )

def status_summary(status):
    return (
        "Task sequence has not been run" if not status.is_run() else
        ( "Run on %s: %s, elapsed time %0.2f seconds" % 
            ( status.human_timestamp() or "-", 
              status.status(), 
              status.elapsed_seconds() / 1000.0
            )
        )
    )

def status_detail(i,message):
    return (
        "  %2d. %-15s %-11s %s %s" % 
            ( i+1,
              message.message[u'actor_name'], 
              message.status,
              message.human_timestamp() or "-",
              "" if not message.is_failure() else 
                  ( "\n        %s (%s)" % 
                      ( message.error.ctor, message.error.message ) 
                  )
            )
    )


def taskseq_read_url(host,port,taskseq_id):
    return u"http://%s:%d/u/1/taskseq/%s" % (host,port,taskseq_id)

def taskseq_status_list_url(host,port,taskseq_id):
    return u"http://%s:%d/u/1/taskseq/%s/status" % (host,port,taskseq_id)

def parsed_taskseq_read(d):
    t = TaskSeq.from_dict(d)
    js = [ Schedule.from_dict(j) for j in d[u'jobs'] ]
    return (t, js)

def parsed_taskseq_status_list(data):
    return [ Status.from_dict(status) for status in data ] 


