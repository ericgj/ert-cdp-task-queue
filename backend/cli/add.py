import logging
import json
from urllib.parse import urlencode
import requests

from model.taskseq import TaskSeq
import model.taskseq.schedule
from cli.util import format_timestamp

logger = logging.getLogger(__name__)

# TODO: refactor this with cli.task ?

def cmd( hostport, taskseq_id, report, 
         year=None, month=None, day=None, week=None, wday=None, 
         hour=None, minute=None, second=None, start=None, end=None,
         timezone=None, jitter=None ):
    host, port = hostport
    
    immed = (
        ( year is None ) and ( month is None ) and ( day is None ) and ( week is None ) and
        ( wday is None ) and ( hour is None ) and ( minute is None ) and ( second is None )
    )

    if immed:
        sched = requests.post( taskseq_sched_create_url(host,port,taskseq_id)) 
    else:
        params = model.taskseq.schedule.Params( 
                     year=year, month=month, day=day, week=week, day_of_week=wday,
                     hour=hour, minute=minute, second=second,
                     start_date=start, end_date=end,
                     timezone=timezone, jitter=jitter
                )
        logger.debug(params.to_dict())
        sched = requests.post( taskseq_sched_create_url(host,port,taskseq_id), 
                    json=params.to_dict() 
                )

    logger.debug(sched.text)
    sched.raise_for_status()
    sched_id = parsed_taskseq_sched_created( sched.json() )

    taskseq_read = requests.get( taskseq_read_url(host,port,taskseq_id))
    logger.debug(taskseq_read.text)
    taskseq_read.raise_for_status()    # TODO handle gracefully

    taskseq, jobs = parsed_taskseq_read( taskseq_read.json() )

    report( (taskseq_id, None), taskseq.environ,
            [ u"Next run time: %s  (job: %s)" % ( format_timestamp(job.next_run_time), job.job_id )
                for job in jobs
            ]
    )


def taskseq_sched_create_url(host, port, id):
    return "http://%s:%d/u/1/taskseq/%s/schedule" % ( host, port, str(id) )

def taskseq_read_url(host,port,taskseq_id):
    return u"http://%s:%d/u/1/taskseq/%s" % (host,port,taskseq_id)

def parsed_taskseq_sched_created(d):
    return model.taskseq.schedule.Created.from_dict(d).id

def parsed_taskseq_read(d):
    t = TaskSeq.from_dict(d)
    js = [ model.taskseq.schedule.Schedule.from_dict(j) for j in d[u'jobs'] ]
    return (t, js)


