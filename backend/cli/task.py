import logging
import json
from urllib.parse import urlencode
import requests
from unflatten import unflatten
from model.environ import Environ
from model.taskseq import Param, Task, TaskSeq, Created
import model.taskseq.schedule 
from cli.util import format_timestamp

logger = logging.getLogger(__name__)

def cmd( hostport, jsonfile, report, 
         year=None, month=None, day=None, week=None, wday=None, 
         hour=None, minute=None, second=None, start=None, end=None,
         timezone=None, jitter=None):
  host, port = hostport
  taskseq_data = parsed_json(jsonfile)
  create = requests.post( taskseq_create_url(host,port), json=taskseq_data.to_dict() )
  logger.debug(create.text)
  create.raise_for_status()
  taskseq_id = parsed_taskseq_created( create.json() )

  report( (taskseq_id, None), taskseq_data.environ, [u"Created task sequence"] )

  immed = (
      ( year is None ) and ( month is None ) and ( day is None ) and ( week is None ) and
      ( wday is None ) and ( hour is None ) and ( minute is None ) and ( second is None )
  )

  if immed:
      sched = requests.post( taskseq_sched_create_url(host,port,taskseq_id)) 
  else:
      params = model.taskseq.schedule.Params( 
                   year=year, month=month, day=day, week=week, day_of_week=wday,
                   hour=hour, minute=minute, second=second,
                   start_date=start, end_date=end,
                   timezone=timezone, jitter=jitter
              )
      logger.debug(params.to_dict())
      sched = requests.post( taskseq_sched_create_url(host,port,taskseq_id), 
                  json=params.to_dict() 
              )

  logger.debug(sched.text)
  sched.raise_for_status()
  sched_id = parsed_taskseq_sched_created( sched.json() )

  if immed:
      report( (taskseq_id, sched_id), taskseq_data.environ,
              [ u"Running immediately" ]
      )
  else:
      job = requests.get( taskseq_sched_read_url(host,port,taskseq_id,sched_id) )
      logger.debug(job.text)
      job.raise_for_status()
      job_data = parsed_taskseq_sched_read( job.json() )

      report( (taskseq_id, sched_id), taskseq_data.environ, 
              [ u"Scheduled task sequence", 
                u"Next run time: %s" % format_timestamp(job_data.next_run_time)
              ]
      )


def taskseq_create_url(host, port):
    return "http://%s:%d/u/1/taskseq" % ( host, port )

def taskseq_sched_create_url(host, port, id):
    return "http://%s:%d/u/1/taskseq/%s/schedule" % ( host, port, str(id) )

def taskseq_sched_read_url(host, port, id, sched_id):
    return ( "http://%s:%d/u/1/taskseq/%s/schedule/%s" % 
               ( host, port, str(id), str(sched_id) )
           )

def parsed_taskseq_created(d):
    return Created.from_dict(d).id

def parsed_taskseq_sched_created(d):
    return model.taskseq.schedule.Created.from_dict(d).id

def parsed_taskseq_sched_read(d):
    return model.taskseq.schedule.Schedule.from_dict(d)


def parsed_json(jsonfile):
    raw = None
    with open(jsonfile,'r') as f:
        raw = json.load(f)

    if raw is None:
       raise ValueError(u"Cannot read json file")
    if raw.get(u'sequence') is None:
       raise ValueError(u"No task sequence specified in json file")

    environ = Environ.from_dict( 
        dict([ (k,v) for (k,v) in raw.items() if not k == u'sequence' ])
    )
    tasks = [ parsed_json_task(t) for t in raw[u'sequence'] ]

    return TaskSeq(environ, tasks)

def parsed_json_task(tuple):
    ctor, data = tuple
    params = [ 
        Param( name=k, ctor=None, data=v ) 
            for (k,v) in data.items() 
    ]
    return Task(ctor, params)


""" 
Parsing from INI file is disabled, too much work to parse complex structures

def parsed_ini(inifile):
    p = ConfigParser()
    p.read(inifile)
    sections = p.sections()
    if len(sections) < 1:
        raise ValueError(u"Config file had no sections: %s" % inifile)
    
    first, *rest = sections
    if not first.lower() == u'environment':
        raise ValueError(u"The first section must be [Environment]")

    environ = parsed_ini_environ(p[first])
    tasks = [ parsed_ini_task(r, p[r]) for r in rest ]
    
    return model.taskseq.TaskSeq(environ, tasks)


def parsed_ini_environ(data):
    return model.environ.Environ.from_dict( unflatten(data) )


def parsed_ini_task( section, data ):
    ctor = section.split(u'.')[0]
    params = [ 
        model.taskseq.Param( name=k, ctor=None, data=v ) 
            for (k,v) in unflatten(data).items() 
    ]
    return model.taskseq.Task(ctor, params)

"""

