import logging
import json
from urllib.parse import urlencode
import requests

from model.taskseq import TaskSeq
from model.taskseq.schedule import Schedule
from cli.util import format_timestamp

logger = logging.getLogger(__name__)

def cmd( hostport, report ):
    host, port = hostport

    taskseq_list = requests.get( taskseq_list_url(host,port))

    logger.debug(taskseq_list.text)
    taskseq_list.raise_for_status()    # TODO handle gracefully

    taskseqs = parsed_taskseq_list( taskseq_list.json() )
 
    for (id, taskseq, jobs) in taskseqs:
        report( (id, None), taskseq.environ,
            [ u"Next run time: %s  (job: %s)" % ( format_timestamp(job.next_run_time), job.job_id )
                for job in jobs
            ]
        )


def taskseq_list_url(host,port):
    return u"http://%s:%d/u/1/taskseq" % (host,port)

def parsed_taskseq_list(pairs):
    return [ parsed_taskseq_list_item(id, taskseq) for (id, taskseq) in pairs ]

def parsed_taskseq_list_item(id, d):
    t = TaskSeq.from_dict(d)
    js = [ Schedule.from_dict(j) for j in d[u'jobs'] ]
    return (id, t, js)

    
