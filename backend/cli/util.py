from datetime import datetime
from time import gmtime

def format_timestamp(ts):
    dt = datetime( *gmtime(ts / 1000.0)[:6] )
    return dt.strftime("%a, %d %b %Y @ %I:%M:%S%p (UTC)")

