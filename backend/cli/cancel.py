import logging
import json
from urllib.parse import urlencode
import requests

from model.taskseq import TaskSeq
from model.taskseq.schedule import Schedule
from cli.util import format_timestamp

logger = logging.getLogger(__name__)

def cmd( hostport, ids, report ):
    host, port = hostport
    taskseq_id, sched_id = ids
 
    taskseq_read = requests.get( taskseq_read_url(host,port,taskseq_id),
        {} if sched_id is None else { u'job': sched_id } 
    )
    logger.debug(taskseq_read.text)
    taskseq_read.raise_for_status()    # TODO handle gracefully

    taskseq, jobs = parsed_taskseq_read( taskseq_read.json() )
    
    if len(jobs) == 0:
        report( ids, taskseq.environ,
                [ "No jobs scheduled" ]
        )
    else:
        for job in jobs:
            taskseq_cancel = requests.delete( taskseq_cancel_url(host,port,taskseq_id,job.job_id) )
            logger.debug(taskseq_cancel.text)
            taskseq_cancel.raise_for_status()   # TODO handle gracefully

        report( ids, taskseq.environ, 
                [ "Job cancelled: %s" % job.job_id for job in jobs ]
        )

def taskseq_read_url(host,port,taskseq_id):
    return u"http://%s:%d/u/1/taskseq/%s" % (host,port,taskseq_id)

def taskseq_cancel_url(host,port,taskseq_id,job_id):
    return u"http://%s:%d/u/1/taskseq/%s/schedule/%s" % (host,port,taskseq_id,job_id)

def parsed_taskseq_read(d):
    t = TaskSeq.from_dict(d)
    js = [ Schedule.from_dict(j) for j in d[u'jobs'] ]
    return (t, js)


