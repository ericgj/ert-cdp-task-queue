from functools import wraps
from webob.exc import HTTPUnprocessableEntity
import jsonschema
from jsonschema import RefResolver, RefResolutionError, ValidationError

def validate( schemas, query=None, request=None, response=None ):
    def decorator(func):
        @wraps(func)
        def _validate(req, params, config=None ):
            resolver = RefResolver(schemas)

            if not query is None:
                _validate(req.params, query, resolver)

            if not request is None:
                _validate(req.json_body, request, resolver)

            res = func(req, params, config)

            if not response is None:
                _validate(res.json_body, response, resolver)

            return res
        return _validate
    return decorator
            
def validate_query(url, schemas):
    return validate(query=url, schemas=schemas)

def validate_request(url, schemas):
    return validate(request=url, schemas=schemas)

def validate_response(url, schemas):
    return validate(response=url, schemas=schemas)



def _validate(instance, url, resolver):
    try:
        schema = resolver.resolve_from_url(url)
    except RefResolutionError as e:
        raise ref_resolution_error(e)
        
    try:
        jsonschema.validate( instance, schema )
    except ValidationError as e:
        raise validation_error(e)


def ref_resolution_error(e):
    return HTTPUnprocessableEntity(
        detail=str(e)
    )

def validation_error(e):
    return HTTPUnprocessableEntity( 
        detail=str(e),
        comment="\n".join( 
            str(e_) for e_ in sorted( e.context, key=lambda x: x.schema_path )
        )
    )

