import logging
from dramatiq.middleware import Middleware

from dramatiq_extra.common import ExceptionData

def pipeline_messages(message, cls):
    target = message.options.get(u'pipe_target')
    if target is None:
        return [ message ]
    else:
        next_message = cls(**target)
        return [ message ] + pipeline_messages( next_message, cls )


class PipelineResults(Middleware):

    def __init__(self, *, get_pipeline_id, storage):
        self.get_pipeline_id = get_pipeline_id
        self.storage = storage
        self.logger = logging.getLogger('__name__')

    @property
    def actor_options(self):
        return {
          u"pipe_init",
          u"pipe_target"
        }

    def before_process_message(self, broker, message):
        from dramatiq.message import Message

        pipeline_id = self.get_pipeline_id(message)
        if pipeline_id is None:
            return

        pipe_init = bool(message.options.get(u'pipe_init',False))
        if pipe_init:
           messages = pipeline_messages(message, Message)
           self.storage.pipeline_init(pipeline_id, messages)

        self.storage.message_init(pipeline_id, message)
            

    def after_process_message(self, broker, message, *, result=None, exception=None):
        pipeline_id = self.get_pipeline_id(message)
        if pipeline_id is None:
            return

        if exception is None:
            self.storage.message_success(pipeline_id, message, result)

        else:
            e = ExceptionData.from_exception(exception)
            self.storage.message_failure(pipeline_id, message, e)



