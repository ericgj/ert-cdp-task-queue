

class PipelineStorage():
    """ 
    ABC for storage of pipeline results
    Used by PipelineResults middleware
    """

    def pipeline_init(self, id, messages):
        raise NotImplementedError()

    def message_init(self, id, message):
        raise NotImplementedError()


    def message_success(self, id, message, result):
        raise NotImplementedError()


    def message_failure(self, id, message, error):
        raise NotImplementedError()


