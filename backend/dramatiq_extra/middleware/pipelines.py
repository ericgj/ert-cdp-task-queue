from dramatiq.middleware import Middleware

from dramatiq_extra.common import ExceptionData

class Pipelines(Middleware):
    """Middleware that lets you pipe actors together so that the
    output of one actor feeds into the input of another.

    Parameters:
      get_pipeline_id(message -> None|str): get pipeline ID from message, 
        or None

      on_success(None|str): actor callback to run when no further messages
        in pipeline

      on_failure(None|str): actor callback to run when exception thrown in
        pipeline
      
      callback_delay(None|int): delay when triggering callbacks (default None)

    Actor options:
      pipe_ignore(bool): When True, ignores the result of the
        previous actor in the pipeline.
      
      pipe_target(dict): A message representing the actor the current
        result should be fed into.
      
    """

    def __init__(self, *, 
        get_pipeline_id,
        on_failure=None, 
        on_success=None, 
        callback_delay=None
    ):
        self.get_pipeline_id = get_pipeline_id
        self.on_failure = on_failure
        self.on_success = on_success
        self.callback_delay = callback_delay

    @property
    def actor_options(self):
        return {
            "pipe_ignore",
            "pipe_target"
        }

    def after_process_message(self, broker, message, *, result=None, exception=None):
        # Since Pipelines is a default middleware, this import has to
        # happen at runtime in order to avoid a cyclic dependency
        # from broker -> pipelines -> messages -> broker.
        from dramatiq.message import Message

        pipeline_id = self.get_pipeline_id(message)
        if pipeline_id is None:
            return

        if exception is None:
            message_data = message.options.get("pipe_target")
            if message_data is None:
                actor_name = self.on_success
                if not actor_name is None:
                    actor = broker.get_actor(actor_name)
                    actor.send_with_options(
                        args=(message.asdict(), result),
                        delay=self.callback_delay
                    )

            else:
                next_message = Message(**message_data)
                if not message.options.get("pipe_ignore", False):
                    next_message = next_message.copy(args=next_message.args + (result,))
                broker.enqueue(next_message)
 
        else:
            actor_name = self.on_failure
            if not actor_name is None:
                exception_data = ExceptionData.from_exception(exception)
                actor = broker.get_actor(actor_name)
                actor.send_with_options(
                    args=(message.asdict(), exception_data.to_dict()),
                    delay=self.callback_delay
                )
                    

