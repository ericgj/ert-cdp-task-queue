import sys
from traceback import format_exception
from collections import namedtuple


class ExceptionData( namedtuple(u'ExceptionData', 
                         ( u'ctor', u'message', u'traceback', u'data') )
):

    """ A simple wrapper for exceptions to capture traceback and other data

    Note: you must call `from_exception` on the last thrown exception
    in order for the traceback to be correct!
    """

    @classmethod
    def from_dict(cls,d):
        return cls(
            ctor=d[u'type'],
            message=d[u'message'],
            traceback=d[u'traceback'],
            data=d[u'data']
        )

    @classmethod
    def from_exception(cls,e):
        return cls(
            ctor=type(e).__name__,
            message=str(e),
            traceback=format_exception(*sys.exc_info()),
            data=( to_dict(e) if hasattr(e,'__dict__') else 
                   {}
            )
        )

    def __new__(cls, *, ctor, message, traceback, data={}):
        return super().__new__(cls, ctor, message, traceback, data)

    def to_dict(self):
        return {
          u'type': self.ctor,
          u'message': self.message,
          u'traceback': self.traceback,
          u'data': self.data
        }


"""
Recursive to_dict from 
https://stackoverflow.com/questions/1036409/recursively-convert-python-object-graph-to-dictionary#1118038
"""
def to_dict(obj, classkey=None):
    if isinstance(obj, dict):
        data = {}
        for (k, v) in obj.items():
            data[k] = to_dict(v, classkey)
        return data
    elif hasattr(obj, "_ast"):
        return to_dict(obj._ast())
    elif hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [to_dict(v, classkey) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, to_dict(value, classkey)) 
            for key, value in obj.__dict__.items() 
            if not callable(value) and not key.startswith('_')])
        if classkey is not None and hasattr(obj, "__class__"):
            data[classkey] = obj.__class__.__name__
        return data
    else:
        return obj


