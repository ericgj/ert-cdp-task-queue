import uuid
import logging

from dramatiq import actor
from dramatiq.composition import pipeline
from apscheduler.jobstores.base import JobLookupError
from rest_api.decorators import created, ok
from rest_api import HTTPNotFound

from util import group_by
import db.taskseq
import db.results
from model.taskseq import TaskSeq
from model.taskseq.status import Status
import model.taskseq.schedule

logger = logging.getLogger(__name__)


@created(location='taskseq.read')
def create( req, params, config ):
    t = TaskSeq.from_dict( req.json_body )  
    storage = db.taskseq.Storage( config.db )
    id = storage.create(t)
    return { 'id': id } 

@ok
def read( req, params, config ):
    id = params[u'id']
    job_prefix = req.params.get(u'job')

    storage = db.taskseq.Storage( config.db )
    data = None
    try:
        data = storage.fetch(id).to_dict()
    except db.RecordNotFound as e:
        raise HTTPNotFound( str(e) )

    jobs = []
    if job_prefix is None:
        jobs = fetch_jobs_for_taskseq(config.scheduler, id)
    else:
        jobs = fetch_jobs_for_taskseq_starting_with(config.scheduler, id, job_prefix)

    if not job_prefix is None and len(jobs) == 0:
        raise HTTPNotFound("No jobs found starting with '%s'" % (job_prefix,))
    else:
        data[u'jobs'] = [ j.to_dict() for j in jobs ]
    return data

@ok
def list( req, params, config ):
    """ TODO
    sched = bool( int(req.params.get(u'scheduled','0')) ) 
    unsched = bool( int(req.params.get(u'unscheduled','0')) )
    """

    def _add_job_data(jobmap,id,t):
        data = t.to_dict()
        data[u'jobs'] = [ j.to_dict() for j in jobmap.get(str(id),[]) ]
        return (id, data)

    jobs = fetch_jobs(config.scheduler)
    
    storage = db.taskseq.Storage( config.db )
    taskseqs = storage.list([ int(j.job_name) for j in jobs ])

    jobmap = {}
    for j in jobs:
        jobs = jobmap.get(j.job_name,[]) 
        jobs.append(j)
        jobmap[j.job_name] = jobs

    return [
        _add_job_data(jobmap,id,t) for (id,t) in taskseqs
    ]


# SCHEDULING

@created(location='taskseq.schedule.read')
def create_schedule( req, params, config ):
    
    # find taskseq in db
    id = params[u'taskseq_id']
    storage = db.taskseq.Storage( config.db )
    seq = storage.fetch(id)

    trigger = None
    trigger_args = {}

    # get trigger from body, or run immediately if no body given
    if req.content_length == 0:
        trigger = 'date'
    else:
        sched = model.taskseq.schedule.Params.from_dict(req.json_body)
        trigger = 'cron'
        trigger_args = sched.to_dict()

    job_id = get_unique_id()
    data = taskseq_send_options(config.broker, id, job_id, seq ) 

    job = config.scheduler.add_job(
        "mq:send_sequence",
        id=job_id,
        name=id,
        kwargs=data,
        trigger=trigger,
        **trigger_args
    )

    return { u'taskseq_id': id, u'id': job.id }


@ok
def read_schedule( req, params, config ):
    job = config.scheduler.get_job( params[u'id'] )
    if job is None:
        raise HTTPNotFound()
    sched = model.taskseq.schedule.Schedule.from_job(job)
    return sched.to_dict()

@ok
def delete_schedule( req, params, config ):
    try:
        config.scheduler.remove_job( params[u'id'] )
    except JobLookupError as e:
        raise HTTPNotFound( str(e) )


# STATUS

@ok
def list_status( req, params, config ):
    job_prefix = req.params.get(u'job')
    task_seq_id = params[u'taskseq_id']
    
    jobs = fetch_jobs_for_taskseq_starting_with(config.scheduler, task_seq_id, job_prefix)

    statuses = []
    if job_prefix is None:
        job = None
        query = db.results.Query( config.db )
        statuses = query.for_taskseq( task_seq_id )

    else:
        if len(jobs) < 1:
            raise HTTPNotFound()

        job = jobs[0]   # pick the first one
        query = db.results.Query( config.db )
        statuses = query.for_taskseq_and_job( task_seq_id, job.job_id )

    return [ s.to_dict() for s in statuses ]


# UTILS

def fetch_jobs(scheduler):
    return [ model.taskseq.schedule.Schedule.from_job(job) for job in scheduler.get_jobs()
           ]

def fetch_jobs_for_taskseq(scheduler, id):
    return [ model.taskseq.schedule.Schedule.from_job(job) for job in scheduler.get_jobs()
               if ( job.name == id
                  )
           ]

def fetch_jobs_for_taskseq_starting_with(scheduler, id, prefix):
    return [ model.taskseq.schedule.Schedule.from_job(job) for job in scheduler.get_jobs()
               if ( job.name == id and 
                    (prefix is None or job.id.startswith(prefix))
                  )
           ]

def get_unique_id():
    return uuid.uuid4().hex

def taskseq_send_options( broker, task_seq_id, job_id, taskseq ):
   messages = [ task_to_message(broker, t).asdict() for t in taskseq.tasks ]
   return {
       u'task_seq_id': task_seq_id,
       u'job_id': job_id,
       u'environ': taskseq.environ.to_dict(),
       u'messages': messages 
   }

def task_to_message( broker, task ):
    actor = broker.get_actor(task.ctor)
    return actor.message(**task.to_params())

