from dramatiq import actor
from dramatiq.message import Message
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.composition import pipeline
from dramatiq_extra.middleware.pipelines import Pipelines
from dramatiq_extra.results.middleware import PipelineResults

from util import current_timestamp, dict_merge
import env
from db.results import PipelineStorage, get_pipeline_id
from model.environ import Environ, Runtime
import task.filesys
import task.sas
import task.zip
import task.checksum
import task.sftp
import task.notification


broker = RabbitmqBroker(
    middleware=[ 
        PipelineResults(
            get_pipeline_id=get_pipeline_id,
            storage=PipelineStorage( env.db_config() )
        ),
        Pipelines( 
            get_pipeline_id=get_pipeline_id,
            on_success=u'notify_sequence_success', 
            on_failure=u'notify_sequence_failure',
            callback_delay=3000
        ) 
    ],
    **env.rabbitmq_config()
)

queue_name = env.rabbitmq_queue()

def create_actor(name, fn):
    return actor(broker=broker, actor_name=name, queue_name=queue_name)(fn)


# module-accessible actors

SelectFiles = create_actor("SelectFiles", task.filesys.select_files)
CheckFiles = create_actor("CheckFiles", task.filesys.check_files)
MoveFiles = create_actor("MoveFiles", task.filesys.move_files)
CopyFiles = create_actor("CopyFiles", task.filesys.copy_files)
VerifyFiles = create_actor("VerifyFiles", task.filesys.verify_files)
RunSas = create_actor("RunSas", task.sas.run)
RunSasTransaction = create_actor("RunSasTransaction", task.sas.transaction_run)
ZipFiles = create_actor("ZipFiles", task.zip.zip_files)
ChecksumMD5 = create_actor("ChecksumMD5", task.checksum.md5)
UploadSftp = create_actor("UploadSftp", task.sftp.upload)
NotifyEmail = create_actor("NotifyEmail", task.notification.email)


def _notify_sequence_success(msgdata, result):
    msg = Message(**msgdata)
    pipeline_id = get_pipeline_id(msg)
    if pipeline_id is None:
        return

    environ = msg.options.get(u'environ')
    notif = env.notification_config()

    storage = PipelineStorage( env.db_config() )
    status = storage.pipeline_fetch(pipeline_id)

    task.notification.success_email( status.to_dict(),
        environ=environ,
        notification=notif
    )

def _notify_sequence_failure(msgdata, error):
    msg = Message(**msgdata)
    pipeline_id = get_pipeline_id(msg)
    if pipeline_id is None:
        return

    environ = msg.options.get(u'environ')
    notif = env.notification_config()

    storage = PipelineStorage( env.db_config() )
    status = storage.pipeline_fetch(pipeline_id)
    
    task.notification.failure_email( status.to_dict(),
        environ=environ,
        notification=notif
    )

notify_sequence_success = create_actor("notify_sequence_success", _notify_sequence_success)
notify_sequence_failure = create_actor("notify_sequence_failure", _notify_sequence_failure)



def send_sequence( task_seq_id, job_id, environ, messages ):
    ts = current_timestamp()
    environ = Environ.from_dict(environ)
    runtime = Runtime(
        task_seq_id=task_seq_id, 
        job_id=job_id,
        timestamp_started_at=ts
    )
    environ = environ.set_runtime(runtime)
    
    pipe = sequenced_messages(environ, messages)

    p = pipeline( pipe , broker=broker )
    p.run()


def sequenced_messages(environ, messages):
    return [ 
        _inject_environ_and_options( environ, 
            Message( **_reset_message(msgdata) ),
            i==0
        )  
            for (i,msgdata) in enumerate(messages)
    ]

def _reset_message(msgdata):
    return dict( 
        (k,v) for (k,v) in msgdata.items() 
            if k not in [u'message_id', u'message_timestamp']
    )

def _inject_environ_and_options(environ, message, is_first):
    environ = environ.to_dict()
    return message._replace( 
        args=message.args + (None,) if is_first else message.args,
        kwargs=dict_merge(
            { u'environ': environ 
            },
            message.kwargs
        ) ,
        options=dict_merge(
            { u'environ': environ,
              u'pipe_init': is_first,
              u'pipe_ignore': False
            }, 
            message.options
        )
    )

