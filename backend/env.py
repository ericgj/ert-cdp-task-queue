import os
import os.path
import json

import pika

APP_ENV = os.environ.get('APP_ENV', 'development')

def local_file(fname):
    return os.path.join(os.path.dirname(__file__),'..',fname)

def config_file(fname):
    return local_file( os.path.join('config', APP_ENV, fname) )

def load_json(fname):
    data = None
    with open(fname, 'r') as f:
        data = json.load(f)
    return data

def db_config():
    return load_json( config_file('db.json') )

def scheduler_config():
    return load_json( config_file('apscheduler.json') )

def rabbitmq_config():
    raw = load_json( config_file('rabbitmq.json') )
    creds = raw.get(u'credentials')
    if not creds is None:
      raw[u'credentials'] = (
          pika.PlainCredentials(creds.get(u'username'), creds.get(u'password'))
      )
    return raw

def rabbitmq_queue():
    return "ert-cdp-tasks-%s" % APP_ENV

def sas_config():
    return load_json( config_file('sas.json') )

def email_config():
    return load_json( config_file('email.json') )

def logging_config():
    return load_json( config_file('logging.json') )

def worker_logging_config():
    return load_json( config_file('logging-worker.json') )

def notification_config():
    return load_json( config_file('notification.json') )
