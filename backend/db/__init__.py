import sqlite3

class RecordNotFound(Exception):
    def __init__(self, table, field, key):
        self.table = table
        self.field = field
        self.key = key

    def __str__(self):
        return "No record found in %s with %s = %s" % (self.table, self.field, repr(self.key))



def dict_rows(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def connect(*args, **kwargs):
    c = sqlite3.connect(*args, **kwargs)
    c.row_factory = dict_rows
    return c



