import json

import db
from model.taskseq import TaskSeq

class Storage():

    def __init__(self, config):
        self.config = config

    def create( self, taskseq, account_id=None ):
        conn = db.connect(**self.config)
        ins = conn.execute( 
            u"INSERT INTO task_sequence (account_id, environ) VALUES (?,?)", 
            (account_id, json.dumps(taskseq.environ.to_dict())) 
        )
        key = ins.lastrowid
        
        task_ins = conn.executemany(
            u"INSERT INTO task (task_seq_id, task_seq_order, data) VALUES (?,?,?)",
            [ (key, i, json.dumps(t.to_dict())) for (i,t) in enumerate(taskseq.tasks) ]
        )
        
        conn.commit()
        return key


    def fetch( self, id ):
        conn = db.connect(**self.config)
        curs = conn.cursor()
        taskseq = curs.execute(
            u"SELECT environ FROM task_sequence WHERE id = ?", (id,)
        ).fetchone()
        if taskseq is None:
            raise db.RecordNotFound(u'task_sequence', u'id', id)

        environ = taskseq[u'environ']
        decoded_environ = environ if environ is None else json.loads(environ) 

        tasks = curs.execute(
            u"SELECT data FROM task WHERE task_seq_id = ? ORDER BY task_seq_order", (id,)
        ).fetchall()
       
        decoded_tasks = [ json.loads(t[u'data']) for t in tasks ]
        return TaskSeq.from_dict({
            u'environ': decoded_environ,
            u'tasks': decoded_tasks
        })


    def list( self, ids ):
        conn = db.connect(**self.config)
        curs = conn.cursor()
        idlist = u", ".join([str(id) for id in ids])
        taskseqs = curs.execute(
            """
            SELECT id, environ FROM task_sequence WHERE id IN (%s)
            """ % (idlist,) 
        ).fetchall()

        tasks = curs.execute(
            """
            SELECT task_seq_id, data FROM task WHERE task_seq_id IN (%s)
            ORDER BY task_seq_id, task_seq_order
            """ % (idlist,)
        ).fetchall()

        decoded_tasks = {}
        for t in tasks:
            id = t[u'task_seq_id']
            idtasks = decoded_tasks.get(id,[])
            idtasks.append(json.loads(t[u'data']))
            decoded_tasks[id] = idtasks

        # Note: returns list of tuple of (id, taskseq)
        return [
            ( seq[u'id'],
              TaskSeq.from_dict({
                  u'environ': ( None if seq.get(u'environ') is None else
                                json.loads(seq.get(u'environ'))
                              ),
                  u'tasks': decoded_tasks.get(seq[u'id'],[])
              }) 
            ) for seq in taskseqs
        ]




