import logging
import json

import db
from model.environ import Environ
from model.taskseq.status import Status, MessageStatus
from util import current_timestamp, group_by

logger = logging.getLogger(__name__)

def get_pipeline_id(message):
    environ = message.options.get(u'environ')
    if environ is None:
        return None
    environ = Environ.from_dict(environ)
    return environ.pipeline_id()

class PipelineStorage():
    
    """ 
    Note: PipelineStorage is distinct from Query below in that it implements the 
    dramatiq_extra.results.storage.Storage API, which abstracts the underlying 
    task-sequence identity as "pipeline_id". In this application, pipeline_id is 
    made up of `task_seq_id` and `job_id`, as stored in the `environ` option of
    the queued message.
    """

    def __init__(self, config):
        self.config = config

    def pipeline_init( self, pipeline_id, messages ):
        ts = current_timestamp()
        (task_seq_id, job_id, seq_timestamp) = pipeline_id
        conn = db.connect( **self.config )
        ins = conn.executemany(
            u"INSERT INTO task_status " +
            u"( task_seq_id, job_id, seq_timestamp, message_id, status, status_timestamp, message ) " +
            u"VALUES (?, ?, ?, ?, ?, ?, ?)",
            [ ( task_seq_id, 
                job_id, 
                seq_timestamp, 
                m.message_id, 
                u"NOT STARTED", 
                ts, 
                json.dumps(m.asdict())
              )
                for m in messages
            ]
        )
        conn.commit()
        return

    def message_init( self, pipeline_id, message ):
        ts = current_timestamp()
        conn = db.connect( **self.config )
        upd = conn.execute(
            u"UPDATE task_status SET status = 'STARTED', status_timestamp = ?, message = ? " +
            u"WHERE message_id = ?",
            ( ts, 
              json.dumps(message.asdict()), 
              message.message_id 
            )
        )
        conn.commit()
        return upd.lastrowid

    def message_success( self, pipeline_id, message, result ):
        ts = current_timestamp()
        logger.debug(u"storing message success with result: %s\n%s" %
                         (message.message_id, str(result)) )
        conn = db.connect( **self.config )
        return taskstatus_update( conn, "SUCCESS", ts, message, result )

    def message_failure( self, pipeline_id, message, error ):
        ts = current_timestamp()
        logger.debug(u"storing message failure with error: %s\n%s" %
                         (message.message_id, str(error.to_dict())) )
        conn = db.connect( **self.config )
        return taskstatus_update( conn, "FAILURE", ts, message, error.to_dict() )

    def pipeline_fetch( self, pipeline_id ):
        (task_seq_id, job_id, seq_timestamp) = pipeline_id
        conn = db.connect( **self.config )
        return taskstatus_fetch( conn, task_seq_id, job_id, seq_timestamp )


class Query():

    def __init__(self, config):
        self.config = config

    def for_taskseq(self, task_seq_id):
        conn = db.connect( **self.config )
        return taskstatus_list_for_taskseq( conn, task_seq_id )

    def for_taskseq_and_job(self, task_seq_id, job_id):
        conn = db.connect( **self.config )
        return taskstatus_list_for_taskseq_and_job( conn, task_seq_id, job_id )

    def fetch(self, task_seq_id, job_id, seq_timestamp):
        conn = db.connect( **self.config )
        return taskstatus_fetch( conn, task_seq_id, job_id, seq_timestamp )
        



# INTERNALS (do not import these directly)

def taskstatus_update( conn, status, timestamp, message, result ):
    upd = conn.execute(
        u"UPDATE task_status SET status = ?, status_timestamp = ?, message = ?, result = ? " +
        u"WHERE message_id = ?",
        ( status, 
          timestamp, 
          json.dumps(message.asdict()), 
          json.dumps(result), 
          message.message_id 
        )
    )
    conn.commit()
    return upd.lastrowid


def taskstatus_fetch( conn, task_seq_id, job_id, seq_timestamp ):
    # sqlite.Connection -> int -> str -> int -> Status

    curs = conn.cursor()

    q = ( u"SELECT * FROM task_status WHERE task_seq_id = ? AND job_id = ? AND seq_timestamp = ? " +
          u"ORDER BY id, status_timestamp", 
          (task_seq_id, job_id, seq_timestamp) 
        )

    results = taskstatus_query(conn, q)

    if len(results) < 1:
        raise db.RecordNotFound( u'task_status',
            (u'task_seq_id', u'job_id', u'seq_timestamp'),
            (task_seq_id, job_id, seq_timestamp)
        )
    
    return results[0]

   

def taskstatus_list_for_taskseq( conn, task_seq_id):
    # sqlite.Connection -> int -> list[Status]

    q = ( u"SELECT * FROM task_status WHERE task_seq_id = ? " +
          u"ORDER BY job_id, seq_timestamp, id", 
          (task_seq_id,) 
        )

    return taskstatus_query(conn, q)


def taskstatus_list_for_taskseq_and_job( conn, task_seq_id, job_id):
    # sqlite.Connection -> int -> str -> list[Status]
    
    q = ( u"SELECT * FROM task_status WHERE task_seq_id = ? AND job_id = ? " +
          u"ORDER BY job_id, seq_timestamp, id", 
          (task_seq_id, job_id) 
        )

    return taskstatus_query(conn, q)
    

def taskstatus_query( conn, q ):
    curs = conn.cursor()
    message_stats = [ 
        MessageStatus.from_dict(decode_message_status(row))
            for row in curs.execute(*q).fetchall()
    ]
   
    job_stats = group_by(
        message_stats, 
        lambda m: (m.seq_timestamp, m.job_id, m.task_seq_id))

    return sorted(
        [ Status(
              task_seq_id=task_seq_id,
              job_id=job_id,
              seq_timestamp=seq_timestamp,
              environ=first_environ(msgs),
              messages=msgs
          )  
            for ((seq_timestamp, job_id, task_seq_id), msgs) in job_stats.items()
        ], 
        key=lambda s: s.seq_timestamp
    )


    
def decode_message_status(row):
    """ Note: mutates row """
    message = row[u'message'] 
    result = row[u'result']
    row[u'message'] = message if message is None else json.loads(message)
    row[u'result'] = result if result is None else json.loads(result)
    return row


def first_environ(message_statuses):
    if len(message_statuses) < 1:
        return None

    raw = message_statuses[0].message.get(u'options',{}).get(u'environ')
    return ( None if raw is None else
             Environ.from_dict(raw)
           )

