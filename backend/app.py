from apscheduler.schedulers.background import BackgroundScheduler

from rest_api import Api, Resource, wsgi, dispatch
from rest_api.schema import validate_request, validate_response, validate_query

import env
# import schema
import db
import taskseq
import mq

class Config:
    def __init__(self, db, scheduler, broker):
        self.db = db
        self.scheduler = scheduler
        self.broker = broker

# schemas=schema.load_from_directory( env.schema_dir() )
# taskseq_create = validate_request('taskseq.create', schemas)

# Decorate routes with validators (future)
taskseq_create = taskseq.create
taskseq_read = taskseq.read
taskseq_list = taskseq.list
taskseq_schedule_create = taskseq.create_schedule
taskseq_schedule_read = taskseq.read_schedule
taskseq_schedule_delete = taskseq.delete_schedule
taskseq_status_list = taskseq.list_status

# Start appscheduler
scheduler = BackgroundScheduler( env.scheduler_config() )
scheduler.start()

# Build config
config = Config(   
    db=env.db_config(),
    scheduler=scheduler,
    broker=mq.broker
)


api = (
    Api([ Resource( u'taskseq', id=r'\d+',
            create=taskseq_create,
            read=taskseq_read,
            list=taskseq_list,
            resources=[
                Resource( u'schedule', id=r'\w+',
                    create=taskseq_schedule_create,
                    read=taskseq_schedule_read,
                    delete=taskseq_schedule_delete
                ),
                Resource( u'status', id=r'\w+',
                    list=taskseq_status_list
                )
            ]
        ),
    ], config = config
     , id = u'1'
     , name = u'u'
    )
)

app = wsgi( dispatch(api) )

