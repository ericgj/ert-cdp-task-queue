# Data delivery automation

A scheduler and task queue for running SAS jobs, finalizing and sending data,
with notifications, with command-line and browser frontends.

--------------------------------------------------------------------------------

## Overview

The application is divided between _server_, _worker_, _task queue_, and
_database_ components. 

The **server** handles the user interface(s), including scheduling jobs to be
put on the task queue.

The **worker(s)** take tasks off the task queue and run them.

The **task queue** is an instance of RabbitMQ.

The **database** stores task configuration data and the status of tasks that are
running or have completed, for use by the user interface (e.g., for configuring 
and monitoring tasks).

Currently, as the application is being developed, all components run on a 
single machine, and with a single worker process, but this may not be
necessary in the future.


### Server requirements

- x64 Windows or linux OS

  Note: a service control script is provided for setting it up to run as
  a Windows service. But ideally a setup for linux running behind an nginx
  reverse-proxy would be better.

- Python 3.6

  Note: The pywin32 library is needed for the service control script if running
  on Windows, and this library needs some DLLs copied to a system location. 
  But this is part of the install script. See below.

- An open port for serving browser and command-line clients (within the ERT
  network), preferably 80.


### Workstation requirements

- x64 Windows OS

- Python 3.6 (including the pywin32 library as noted above)

- SAS 9.2 or 9.4

- Service account with domain access

  Note: worker processes will need access to an ERT SMTP server for email
  notifications, and file system access to externally-facing FTP servers where 
  they can drop files. (In addition, if the task queue and database components
  are running on separate machines, they would need access to those as well.)


### Database requirements

- SQLite 3

  Note: currently the database is an SQLite file accessed by both server and 
  worker processes on a single machine. If the server and workstation components
  are later distributed, some work would be needed to make the database network-
  accessible (i.e. something other than SQLite).

### RabbitMQ requirements

- Erlang 9.2 / OTP 20.2 (64bit) 

- RabbitMQ 3.7+

- Open port for RabbitMQ communications, standard port is 5672


--------------------------------------------------------------------------------

## Deployment instructions

Please read these instructions thoroughly before starting. Contact Eric
Gjertsen ([egjertsen@ert.com][egjertsen]) if any questions or concerns.

Note that these instructions assume installation of all components on a single 
Windows machine, for now.

### Before you begin

Make sure you have installed [git][git] on the target machine.

Then either:

  - set up an ssh key pair, and send the public key to Eric requesting access 
    to the source code repo (some instructions [here][ssh]);  or

  - set up an account on bitbucket.org and request http access to the source
    code from Eric.

_Alternatively_, you can download a zip file of the master (stable) branch
[here][zip], but you will still need an account on bitbucket.org and request 
access from Eric. Git is the preferred method however because it streamlines 
patching in updates.


### Install infrastructure

#### Install SAS

This is beyond the scope of these instructions, please ask [David Ju][dju] of
Clinical Data Programming for installation media and instructions.

You should also install the SAS PC Files Server for access to different 
versions of MS Office files (Access, Excel, etc.) from within SAS.

After installation, you should create a SAS program `autoexec_batch.sas` in
C:\Program Files\SASHome\SASFoundation\9.4 (assuming SAS v9.4):

    %let _utime = %sysfunc( datetime(), datetime19.0 );
    %let usertime = %sysfunc(compress(&_utime, ':'));

    ODS NORESULTS;

I'm not sure this is absolutely necessary, but at the very least a blank 
`autoexec_batch.sas` file should exist in that directory.


#### Install Python

Follow instructions on http://python.org/downloads/windows to run the 
"Windows x86 executable installer" for the latest patch version of python 3.6.

During installation,

  - Select "Add to PATH"
  - Choose to install py launcher
  - Choose to install for all users
  - Install to the recommended directory, C:\Program Files (x86)\Python36-x
    (where x is the patch version)


#### Install Erlang

Follow instructions on http://erlang.org/downloads to run the Windows installer
("64-bit Binary File") for OTP 20.2. (Later versions may be compatible as well; 
check the RabbitMQ [compatibility page][rabbitmq-compat].)

Choose the default options during installation.

After installation, open an elevated (Run as Administrator) cmd shell and run:

    setx /m ERLANG_HOME "C:\Program Files\erlx.y"

where `x.y` is the Erlang version you installed.


#### Install RabbitMQ

Follow instructions on https://www.rabbitmq.com/install-windows.html to run
the Windows installer (current version as of Feb 2018 is 3.7.3; the latest
version in the 3.7.x series should be fine).

Choose the default options during installation.

After installation, check Windows Services to confirm that a RabbitMQ service
is running and is set to automatically start on boot.

You should also install the RabbitMQ management UI. Open an elevated (Run as 
Administrator) cmd shell and run:

    cd "C:\Program Files\RabbitMQ Server\rabbitmq_server-x.y.z\sbin"
    rabbitmq-plugins enable rabbitmq_management

where `x.y.z` is the RabbitMQ version you installed.

After installing, restart the RabbitMQ service.

To confirm the UI is available, open a web browser and go to 
http://localhost:15672/  and log in as guest/guest.

_TODO: how to add a new RabbitMQ user and remove the guest account._


#### Install SQLite

Download the precompiled binaries for Windows x64 (zip files) at 
http://sqlite.org/download.html. Download both the "64-bit DLL (x64) for 
SQLite" and the "bundle of command-line tools for managing SQLite database 
files".

Unzip all files to C:\sqlite3.

Add this directory to the system path. In an elevated (Run as Administrator) 
cmd shell run:

    setx /m path "%path%;C:\sqlite3"

In the same cmd shell, test that the command-line client works:
    
    sqlite3 c:\sqlite3\test.db

You should get an `sqlite>` prompt. Type `.quit <ENTER>` to exit.



### Initialize application

#### Clone source code

Decide where to put the source code directory. Generally this should be an 
accessible location outside of an individual user's home directory. For example,
you could put it into this parent directory under the Public user: 
`C:\Users\Public\Documents\Scripts\`.

If you have set up for **ssh access** to the source code repo, run the following
in a git bash shell:
```
cd path/to/parent/folder
git clone git@github.com:ericgj/ert-cdp-task-queue.git cdp-task-queue
```

If instead you have set up for **http access** using a bitbucket.org account, 
run the following in a git bash shell:
```
cd path/to/parent/folder
git clone https://your-user-name@bitbucket.org/ericgj/ert-cdp-task-queue.git cdp-task-queue
```

#### Run install script

In a cmd shell with elevated (Run as Administrator) permissions, run the 
following:
```
cd cdp-task-queue
bin\install
```

This will set up a virtual environment for Python, install dependencies, and
copy pywin32 DLLs to the right system directories.


#### Copy and tweak configuration files

Copy the `config\examples` directory to `config\production`. Then edit the
files in `config\production` for the environment you are running the server and
worker processes in. See detailed notes below under "Application configuration".


### Initialize database

In a cmd shell, run:
```
type db\create.sql | sqlite3 db\tasks-prod.sqlite3
```

Note `db\tasks-prod.sqlite3` is just a suggested name for the SQLite database
file. Whatever you call it, it should live under the `db` directory and be
referenced in the `config\production\db.json` config file.


### Start application services

Before you can start the application services, make sure a domain service 
account has been set up by IT and that it has been granted permissions to run
services on the local machine.


#### Create and start worker

In a cmd shell, run:
```
bin\worker_service production 2 ^
    --username="ERT\service.account" ^
    --password=letmeinyouidiot ^
    install

bin\worker_service production 2 start
```

This will create and start a Windows service named "ERT SAS Automation
Worker", which runs two worker threads internally (meaning two jobs can be
processed concurrently).

You should set the username/password to a service account with write permissions
on the server you ultimately need to copy files to, and with permissions to
connect to the SMTP server to send out email notifications. Also if the 
RabbitMQ instance is running on a separate machine, this service account should
have access to network resources (TCP/IP connection to the RabbitMQ machine
basically).

(Until further testing takes place, it's not recommended to increase the 
worker threads above 2.)


#### Create and start server

In a cmd shell, run:
```
bin\service production 80  ^
    --username="ERT\service.account" ^
    --password=letmeinyouidiot ^
    install

bin\service production 80 start
```

This will start the web server as a Windows service listening on port 80 
(bound to all network interfaces). Note you can use a different port if 80 is
not available. The name of the service is "ERT SAS Automation Server".

Note it's not absolutely necessary to set a username/password for the web server 
to run under (since it only accesses local resources, except if the RabbitMQ
instance is running on a separate machine). But it's probably a good idea to
set it to the same account as the worker service.


### Confirm application

#### Check services

Once the services have been started, confirm they are running by checking
Windows Services or in a cmd shell run:
```
sc query ert-sas-automation-worker
sc query ert-sas-automation-server
```

#### Check logs

In the `log` folder you should see the files `worker-production.log` and 
`server-production.log`.

`worker-production.log` should look something like this (perhaps without the
DEBUG lines if you are logging only at INFO level):

```
INFO|2018-02-12 22:19:47 -0500|PID 12336|MainThread|worker|Creating worker...
INFO|2018-02-12 22:19:47 -0500|PID 12336|MainThread|worker|Starting worker...
DEBUG|2018-02-12 22:19:47 -0500|PID 12336|MainThread|dramatiq.worker._WorkerMiddleware|Adding consumer for queue 'ert-cdp-tasks-production'.
DEBUG|2018-02-12 22:19:47 -0500|PID 12336|Thread-1|dramatiq.worker.ConsumerThread(ert-cdp-tasks-production)|Running consumer thread...
DEBUG|2018-02-12 22:19:47 -0500|PID 12336|MainThread|dramatiq.worker._WorkerMiddleware|Adding consumer for delay queue 'ert-cdp-tasks-production.DQ'.
DEBUG|2018-02-12 22:19:47 -0500|PID 12336|Thread-2|dramatiq.worker.ConsumerThread(ert-cdp-tasks-production.DQ)|Running consumer thread...
DEBUG|2018-02-12 22:19:47 -0500|PID 12336|Thread-3|dramatiq.worker.WorkerThread|Running worker thread...
INFO|2018-02-12 22:19:47 -0500|PID 12336|MainThread|worker|Worker process is ready for action.
```

`server-production.log` should be blank until the server receives a request.
If you see an error and stack trace, something went wrong starting up the
server process.

#### Test installation

TODO

#### Test operation

TODO


## Application configuration

Configuration consists of editing json files under `config\production`. For the
most part default settings can be used, except where noted below.

### Scheduler

Scheduler options live in `apscheduler.json`. The recommendation (for now) is
to set the scheduler database to `sqlite:///db/apscheduler.sqlite3`: that is,
to an SQLite database file `db\apscheduler.sqlite3`

The options follow the format described in [APScheduler][apsched] documentation,
which is not great, but see the [example "Method 2" here][apsched-examples].


### Database

Database options live in `db.json`. There should be at least a `database` value,
which is the path to SQLite database file initialized above (with forward-
slashes). 

These options are passed in to python's `sqlite3.connect()` function. Other
options are documented [here][python-sqlite3].  Please note these options are
subject to change.


### RabbitMQ

Application-specific (client) RabbitMQ options are specified in `rabbitmq.json`. 
The main options to be concerned with are the `host`, `virtual_host`,  and 
`credentials`.

- **host** is the RabbitMQ host name. If running the server and worker processes
  on a single machine, you can set this to "localhost". Otherwise it should be
  the name or IP address of the machine where RabbitMQ is running.

- **virtual_host** is the RabbitMQ "virtual host" name. This is basically a way
  you can host multiple sets of channels/queues through a single RabbitMQ 
  instance. If you haven't set up multiple virtual hosts on the RabbitMQ server,
  you don't need to specify this option (it will default to "/").

- **credentials** consists of `username` and `password` keys, corresponding to
  a RabbitMQ user account on the server. If you don't specify this option it
  will default to "guest/guest", which is the default RabbitMQ account. But in
  production you should remove the guest account and set up an appropriate 
  service account for this application on the RabbitMQ server. Instructions for
  doing this are beyond the scope of these instructions, but see the
  [RabbitMQ documentation][rabbitmq-acl].

Other RabbitMQ client options are documented [here][pika]. Generally it's safe
to use the other option defaults in `config\example\rabbitmq.json`. 

The RabbitMQ server has its own set of (quite extensive)
[configuration][rabbitmq-config], about which it's beyond the scope of these
instructions to provide guidance. In general, unless you are running into
particular performance issues, the default server configuration is fine for this
application, which is not high-volume.


### Email

The application needs to connect to an SMTP server for sending out notification
emails. This connection is specified via the `email.json` config file. The main
options are `host` and `port` (SMTP host and port respectively). Other options
are documented in the [python smtplib][python-smtp] documentation. (Note that
authentication and encryption mechanisms connecting to SMTP are not currently
implemented, but could be added if needed).

### Notifications

You can set application-wide settings for the sender and recipients of emails
sent when a task sequence succeeds or fails, in the `notification.json` config
file. The example shows how this file should be structured. Available options
include:

  - **from**: the email address that notification emails are sent under. 
    Generally this should be the application service account.

  - **to**, **cc**, and **bcc**  are lists of email addresses that notification
    emails are sent to, cc'd, or bcc'd, respectively.

Note: additional notification methods besides email may be added in the future.
The `notification.json` config file collects their settings in one place.

Also note that notification can be set up per scheduled task sequence, in 
addition to application-wide. So for instance additional emails can be added
to the to, cc, or bcc for particular task sequences.


### SAS

The worker needs some basic info about the SAS installation as well as default
options and environment variables when SAS jobs are run. These options live in
the `sas.json` config file. In general these settings do not need to be tweaked
except for variations in paths depending on whether SAS 9.2 or 9.4 is installed.


## Application updates

A basic update to the application that does not involve database schema changes
or changes to service control scripts or other infrastructure changes should
follow these steps (assuming you are using git to manage patches).

In a git bash shell:

```
cd path/to/app/source
git pull origin master
```

Then in an elevated (Run As Administrator) cmd shell:

```
cd path\to\app\source
bin\update
bin\worker_service production <number-of-threads> stop
bin\service production <http-port> stop
```

Note you do not have to run `bin\update` unless libraries have been updated,
which you can tell by running `git diff ^HEAD requirements.txt` in the git
bash shell and seeing if there were any libraries added or changed.


**IMPORTANT: before restarting services, make sure there are no orphaned
worker processes still running.** (This is currently -- as of 2 Mar 2018 --
a known problem in Windows 10, the services are not closing cleanly.) If
you don't force them closed, you will see inconsistent results when tasks
are pulled off the queue.

  - Open Task Manager (taskmgr.exe)
  - Show all processes, "More details"
  - Scroll down to "Background processes", and look for any `Python (32 bit)` 
    listed.
  - Select each, and click "End task".


Then in an elevated (Run As Administrator) cmd shell:

```
cd path\to\app\source
bin\worker_service production <number-of-threads> start
bin\service production <http-port> start
```


[egjertsen]: mailto:egjertsen@ert.com
[dju]: mailto:dju@ert.com
[git]: https://git-scm.com/
[ssh]: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
[zip]: https://bitbucket.org/ericgj/ert-cdp-task-queue/get/master.zip
[rabbitmq-compat]: https://www.rabbitmq.com/which-erlang.html
[rabbitmq-config]: https://www.rabbitmq.com/configure.html 
[rabbitmq-acl]: https://www.rabbitmq.com/rabbitmqctl.8.html#User_Management
[pika]: https://pika.readthedocs.io/en/latest/modules/parameters.html#connectionparameters
[apsched]: https://apscheduler.readthedocs.io/en/latest/modules/schedulers/base.html#module-apscheduler.schedulers.base
[apsched-examples]: https://apscheduler.readthedocs.io/en/latest/userguide.html#configuring-the-scheduler
[python-smtp]: https://docs.python.org/3/library/smtplib.html
[python-sqlite3]: https://docs.python.org/3/library/sqlite3.html#sqlite3.connect

