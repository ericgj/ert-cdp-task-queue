CREATE TABLE IF NOT EXISTS data_type 
    ( id INTEGER PRIMARY KEY AUTOINCREMENT
    , name TEXT NOT NULL
    )
;
CREATE INDEX IF NOT EXISTS idx_data_type_name ON data_type ( name );

CREATE TABLE IF NOT EXISTS account 
    ( id INTEGER PRIMARY KEY AUTOINCREMENT
    , account TEXT NOT NULL
    , protocol TEXT NOT NULL
    , client TEXT NOT NULL
    , data_type_id INTEGER NOT NULL
    , cmis_id TEXT
    , gcal_id TEXT
    , CONSTRAINT unq_account 
        UNIQUE ( account, protocol, client, data_type_id ) 
            ON CONFLICT ABORT
    , CONSTRAINT fk_account_data_type 
        FOREIGN KEY ( data_type_id ) REFERENCES data_type ( id )
            ON DELETE RESTRICT
    )
;
CREATE INDEX IF NOT EXISTS idx_account_account ON account ( account );
CREATE INDEX IF NOT EXISTS idx_account_data_type_id ON account ( data_type_id );

CREATE TABLE IF NOT EXISTS task_sequence
    ( id INTEGER PRIMARY KEY AUTOINCREMENT
    , source_seq_id INTEGER
    , account_id INTEGER
    , environ BLOB 
    , CONSTRAINT fk_task_sequence_source
        FOREIGN KEY ( source_seq_id ) REFERENCES task_sequence ( id )
            ON DELETE SET NULL
    , CONSTRAINT fk_task_sequence_account
        FOREIGN KEY ( account_id ) REFERENCES account ( id )
            ON DELETE SET NULL
    )
;
CREATE INDEX IF NOT EXISTS idx_task_sequence_source_seq_id 
    ON task_sequence ( source_seq_id );
CREATE INDEX IF NOT EXISTS idx_task_sequence_account_id 
    ON task_sequence ( account_id ); 

CREATE TABLE IF NOT EXISTS task
    ( id INTEGER PRIMARY KEY AUTOINCREMENT
    , task_seq_id INTEGER NOT NULL
    , task_seq_order INTEGER NOT NULL
    , data BLOB
    , CONSTRAINT fk_task_task_sequence
        FOREIGN KEY ( task_seq_id ) REFERENCES task_sequence ( id )
            ON DELETE CASCADE
    )
;
CREATE INDEX IF NOT EXISTS idx_task_task_seq_id ON task ( task_seq_id );
CREATE INDEX IF NOT EXISTS idx_task_task_seq_order ON task ( task_seq_order );


CREATE TABLE IF NOT EXISTS task_status
    ( id INTEGER PRIMARY KEY AUTOINCREMENT 
    , task_seq_id INTEGER NOT NULL
    , job_id TEXT NOT NULL   
    , seq_timestamp DATETIME NOT NULL
    , message_id TEXT NOT NULL
    , status TEXT NOT NULL
        CONSTRAINT rule_task_result_status 
            CHECK (status IN ('NOT STARTED', 'STARTED', 'SUCCESS', 'FAILURE'))
    , status_timestamp DATETIME NOT NULL
    , message BLOB
    , result BLOB
    , log BLOB
    )
;

CREATE INDEX IF NOT EXISTS idx_task_status_task_seq_id ON task_status ( task_seq_id );
CREATE INDEX IF NOT EXISTS idx_task_status_job_id ON task_status ( job_id );
CREATE INDEX IF NOT EXISTS idx_task_status_seq_timestamp ON task_status ( seq_timestamp );
CREATE INDEX IF NOT EXISTS idx_task_status_message_id ON task_status ( message_id );
CREATE INDEX IF NOT EXISTS idx_task_status_status_timestamp ON task_status ( status_timestamp );


