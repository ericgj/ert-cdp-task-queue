@ECHO OFF
pip install virtualenv
CD %~dp0..
virtualenv .env
.env\Scripts\activate && pip install -r requirements.txt && python .env\Scripts\pywin32_postinstall.py -install  && deactivate

if not exist log ( md log )
