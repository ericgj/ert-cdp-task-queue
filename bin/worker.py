import os
import sys
import signal
import time
import logging
import logging.config
import env

logging.config.dictConfig( env.worker_logging_config() )
logging.getLogger("pika").setLevel(logging.ERROR)

import mq
from dramatiq import Worker, ConnectionError

def run(threads):
  logger = logging.getLogger("worker")
  
  try:
      logger.info("Creating worker...")
      worker = Worker(mq.broker, worker_threads=threads)
      logger.info("Starting worker...")
      worker.start()
      
  except ConnectionError:
      logger.exception("Broker connection failed.")
      return os._exit(3)
      
  def termhandler(signum, frame):
      nonlocal running
      if running:
          logger.info("Stopping worker process...")
          running = False
          
      else:
          logger.warning("Killing worker process...")
          return os._exit(1)
          
          
  logger.info("Worker process is ready for action.")
  signal.signal(signal.SIGINT, termhandler)
  signal.signal(signal.SIGTERM, termhandler)
  if hasattr(signal, 'SIGBREAK'):
      signal.signal(signal.SIGBREAK, termhandler)
  
  running = True
  while running:
      time.sleep(1)
      
  worker.stop()
  mq.broker.close()

  return os._exit(0)
  

if __name__ == "__main__":
    threads = int(sys.argv[1]) if len(sys.argv)>1 else 1
    run( threads )

