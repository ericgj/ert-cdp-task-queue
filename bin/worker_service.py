import win32serviceutil
import win32service
import win32event
import win32api
import win32con
import socket

SERVICE_PARAMS_KEY = 'Params'

class PySvc(win32serviceutil.ServiceFramework):
    
    _svc_name_ = 'ert-sas-automation-worker'
    _svc_display_name_ = "ERT SAS Automation Worker"
    _svc_description_ = _svc_display_name_
    _svc_deps_ = [ "RabbitMQ" ]
    
    @classmethod
    def customOptionHandler(cls, opts):
        app_env, app_worker_threads = 'development', '1'
        for (opt, val) in opts:
            if opt == '-e':
                app_env = val
            
            elif opt == '-t':
                app_worker_threads = val

        """
        Note: service name has to be statically set in class, because 
        PythonService starts the service based on file path + class name, not 
        through the command line.

        This means we can't run more than one instance of the service at a
        time for different environments, but that's probably ok.

        And actually the display name and description updates here are never
        applied, because this handler is run _after_ an install/update. Oh well.
        """
        # cls._svc_name_ = '%s-%s' % (cls._svc_name_, app_env)
        cls._svc_display_name_ = '%s (%s)' % (cls._svc_display_name_, app_env)
        cls._svc_description_ = cls._svc_display_name_

        setServiceParams(cls._svc_name_, [app_env, app_worker_threads])


    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)  
        socket.setdefaulttimeout(60)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    # Stopping gracefully on system shutdown
    SvcShutdown = SvcStop
    
    def SvcDoRun(self):
        import sys
        from traceback import format_exception
        import servicemanager
        
        self.ReportServiceStatus(win32service.SERVICE_RUNNING)
        servicemanager.LogMsg(
           servicemanager.EVENTLOG_INFORMATION_TYPE,
           servicemanager.PYS_SERVICE_STARTED,
           (self._svc_name_,'')
        )
        try:
            (app_env, app_worker_threads) = getServiceParams(self.__class__._svc_name_)
            main(self.hWaitStop, app_env, app_worker_threads)
        except Exception as e:
            # print(str(e))
            tb = format_exception(*sys.exc_info())
            servicemanager.LogErrorMsg("\r\n".join(tb))

        
def main(stop_event, app_env, app_worker_threads):
    import os.path
    import subprocess
    import servicemanager

    proc = None
    try:
      rc = None
      cmd = ['cmd', '/c', 'worker.cmd', app_env, app_worker_threads ]
      cwd = os.path.dirname(__file__)
      proc = subprocess.Popen(
          cmd, cwd=cwd, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP
      )
      while rc != win32event.WAIT_OBJECT_0:  
          try:
              proc.wait(timeout=5)
          except subprocess.TimeoutExpired:
              if not stop_event is None:
                  rc = win32event.WaitForSingleObject(stop_event, 0)
          except KeyboardInterrupt:
               break

    finally:
      if proc:
          terminate(proc)


def terminate_signal(p):
    """
    This works in python debug mode, but fails for some reason in service stop,
    orphaning the subprocess.
    """
    import platform
    import signal
    import subprocess

    if platform.system() == 'Windows':
        try:
            # print("Attempting to kill PID %d" % p.pid)
            p.send_signal(signal.CTRL_BREAK_EVENT)
            try:
                p.wait(timeout=10)
            except subprocess.TimeoutExpired:
                terminate_force(p)

        except Exception:
            terminate_force(p)
    else:
        p.terminate()


def terminate(p):
    """ 
    Windows subprocess termination using psutil, as suggested by
    https://stackoverflow.com/questions/1230669/subprocess-deleting-child-processes-in-windows
    
    Note: Does not quite work.
    
    In Windows 7, it fails to terminate the parent process within 5 secs
    and so forces termination.

    In Windows 10, the children processes are for some reason invalid so it
    throws an error when attempting to send a signal to them, and then 
    forces termination.

    """
    import platform
    if platform.system() == 'Windows':
        import psutil
        import signal

        try:
            parent = psutil.Process(p.pid)
            children = parent.children(recursive=True)
            for c in children:
                # print("Attempting to kill PID %d" % c.pid)
                c.send_signal(signal.CTRL_BREAK_EVENT)
            gone, still_alive = psutil.wait_procs(children, timeout=5)
            # print("Attempting to kill parent PID %d" % parent.pid)
            parent.send_signal(signal.CTRL_BREAK_EVENT)
            try:
                parent.wait(5)
            except psutil.TimeoutExpired:
                terminate_force(p)

        except Exception:
            terminate_force(p)
    else:
        p.terminate()

def terminate_force(p):
    """ 
    Note: NOT a graceful shutdown. Use only if other options don't work.
    """
    import subprocess
    import platform

    if platform.system() == 'Windows':
        subprocess.call(['taskkill', '/F', '/T', '/PID', str(p.pid)])
    else:
        p.terminate()


def setServiceParams(service_name, params):
    key = win32api.RegOpenKeyEx(
        win32con.HKEY_LOCAL_MACHINE, 
        'System\\CurrentControlSet\\Services\\' + service_name,
        0,
        win32con.KEY_ALL_ACCESS
    )
    try:
        win32api.RegSetValueEx(key, SERVICE_PARAMS_KEY, 0, win32con.REG_SZ, " ".join(params))
    finally:
        key.Close()

def getServiceParams(service_name):
    key = win32api.RegOpenKey(
        win32con.HKEY_LOCAL_MACHINE, 
        'System\\CurrentControlSet\\Services\\' + service_name
    )
    try:
        (params, _) = win32api.RegQueryValueEx(key, SERVICE_PARAMS_KEY)
        return params.split(" ")
    finally:
        key.Close()


if __name__ == '__main__':
    """
    This is the odd way pywin32 lets you specify extra parameters: via 
    _shortopts_ specified in customInstallOptions, and a customOptionHandler 
    callback.

    In this case, we have custom options:
        -e APP_ENV 
        -t APP_WORKER_THREADS

    The custom options are stored to/loaded from a custom registry key under
    the service (SERVICE_PARAMS_KEY).
    """
    
    win32serviceutil.HandleCommandLine(PySvc,
        customInstallOptions='e:t:',
        customOptionHandler=PySvc.customOptionHandler)

