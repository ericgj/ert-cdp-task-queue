import sys
import logging
import logging.config

from requestlogger import WSGILogger, ApacheFormatter

import env
from app import app

logging.config.dictConfig( env.logging_config() )
logged_app = WSGILogger(app, logging.getLogger().handlers, ApacheFormatter() )

import waitress
listen = sys.argv[1]
waitress.serve( logged_app, listen=listen )
