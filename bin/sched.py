from argparse import ArgumentParser
from datetime import datetime
import logging
import os

import cli.task
import cli.add
import cli.list_
import cli.status
import cli.cancel

if os.environ.get(u'DEBUG','0') == '0':
    logging.basicConfig(level=logging.INFO)
else:
    logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

# Argument parsers

def parse_host_port(s):
    try:
       host, port = s.split(':')
       port = int(port)
       return (host, port)
    except Exception:
       raise ValueError("Host and port must be specified as 'host:port'")

def parse_datetime(s):
    try:
        return datetime.strptime(s, '%Y-%m-%dT%H:%M:%S')
    except ValueError:
        raise ValueError("Datetime arguments must be in ISO8601 format with no timezone")

def parse_float(s):
    try:
        return float(s)
    except ValueError:
        raise ValueError("Argument must be numeric: '%s'" % (s,))


def parse_reporter(s):
    if s.lower() == 'simple':
        return simple_reporter
    else:
        raise ValueError("The only report currently implemented is 'simple'")
        
def parse_status_formatter(s):
    if s.lower() == 'csv':
        return csv_formatter
    else:
        raise ValueError("The only format currently implemented is 'csv'")
        
def parse_ids(s):
    ids = s.split(':',1)
    if len(ids) == 1:
        return (ids[0], None)
    else:
        return ids


# Reporters

def simple_reporter( ids, environ, msgs ):
    seq_id, job_id = ids
    label = "{client} {protocol} {account} {datatype}".format(**environ.to_dict())
    print("[%s:%s] %s" % (seq_id, "-" if job_id is None else job_id, label) )
    for msg in msgs:
        if isinstance(msg, list):
            for submsg in msg:
                print('  ' + submsg)
            print('')
        else:
            print('  ' + str(msg))


# Command wrappers

def call_task_cmd(args):
    return cli.task.cmd( 
        args.host, args.json, parse_reporter(args.report), 
        start=args.start, end=args.end,
        year=args.year, month=args.month, day=args.day, wday=args.wday,
        hour=args.hour, minute=args.minute, second=args.second,
        timezone=args.timezone, jitter=args.jitter 
    )

def call_add_cmd(args):
    return cli.add.cmd( 
        args.host, args.id, parse_reporter(args.report), 
        start=args.start, end=args.end,
        year=args.year, month=args.month, day=args.day, wday=args.wday,
        hour=args.hour, minute=args.minute, second=args.second,
        timezone=args.timezone, jitter=args.jitter
    )

def call_list_cmd(args):
    return cli.list_.cmd(
        args.host, parse_reporter(args.report)
    )

def call_status_cmd(args):
    return cli.status.cmd(
        args.host, args.ids, parse_reporter(args.report)
    )

def call_cancel_cmd(args):
    return cli.cancel.cmd(
        args.host, args.ids, parse_reporter(args.report)
    )


parser = ArgumentParser(description="A command line client for the cdp-task-queue.")
parser.add_argument('host', metavar="HOST:PORT", type=parse_host_port,
                    help="Host and port")
parser.add_argument('--report', choices=("simple",), default="simple",
                    help="Type of output report")
subparser = parser.add_subparsers(help="Command")


task_parser = subparser.add_parser("task", 
    help="Create and schedule a task sequence",
    description="Note that all date-time parameters are relative to the timezone of the scheduler, unless specified. It's recommended that this be set to UTC."
)
task_parser.add_argument('json', metavar="JSON-FILE", help="JSON file specifying task sequence")
task_parser.add_argument('--year', metavar="YEAR-EXPR", default=None, 
                         help="year schedule expression") 
task_parser.add_argument('--month', metavar="MONTH-EXPR", default=None, 
                         help="month schedule expression") 
task_parser.add_argument('--day', metavar="DAY-EXPR", default=None, 
                         help="day-of-month schedule expression") 
task_parser.add_argument('--week', metavar="WEEK-EXPR", default=None, 
                         help="week-of-year schedule expression") 
task_parser.add_argument('--wday', metavar="WDAY-EXPR", default=None, 
                         help="day-of-week schedule expression") 
task_parser.add_argument('--hour', metavar="HOUR-EXPR", default=None, 
                         help="hour schedule expression") 
task_parser.add_argument('--minute', metavar="MINUTE-EXPR", default=None, 
                         help="minute schedule expression") 
task_parser.add_argument('--second', metavar="SECOND-EXPR", default=None, 
                         help="second schedule expression") 
task_parser.add_argument('--start', metavar="DATETIME", type=parse_datetime, default=None, 
                         help="Start datetime in ISO8601 format")
task_parser.add_argument('--end', metavar="DATETIME", type=parse_datetime, default=None,
                         help="End datetime in ISO8601 format")
task_parser.add_argument('--timezone', metavar="TIMEZONE", default=None, 
                         help="IANA timezone, e.g. 'America/New_York'") 
task_parser.add_argument('--jitter', metavar="N", default=None, type=parse_float,
                         help="Randomly jitter execution +/- N seconds") 
task_parser.set_defaults(func=call_task_cmd)


add_parser = subparser.add_parser("add", 
    help="Add a job schedule for existing task sequence",
    description="Note that all date-time parameters are relative to the timezone of the scheduler, unless specified. It's recommended that this be set to UTC."
)
add_parser.add_argument('id', metavar="TASK", type=str,
                        help="Task Sequence ID")
add_parser.add_argument('--year', metavar="YEAR-EXPR", default=None, 
                         help="year schedule expression") 
add_parser.add_argument('--month', metavar="MONTH-EXPR", default=None, 
                         help="month schedule expression") 
add_parser.add_argument('--day', metavar="DAY-EXPR", default=None, 
                         help="day-of-month schedule expression") 
add_parser.add_argument('--week', metavar="WEEK-EXPR", default=None, 
                         help="week-of-year schedule expression") 
add_parser.add_argument('--wday', metavar="WDAY-EXPR", default=None, 
                         help="day-of-week schedule expression") 
add_parser.add_argument('--hour', metavar="HOUR-EXPR", default=None, 
                         help="hour schedule expression") 
add_parser.add_argument('--minute', metavar="MINUTE-EXPR", default=None, 
                         help="minute schedule expression") 
add_parser.add_argument('--second', metavar="SECOND-EXPR", default=None, 
                         help="second schedule expression") 
add_parser.add_argument('--start', metavar="DATETIME", type=parse_datetime, default=None, 
                         help="Start datetime in ISO8601 format")
add_parser.add_argument('--end', metavar="DATETIME", type=parse_datetime, default=None,
                         help="End datetime in ISO8601 format")
add_parser.add_argument('--timezone', metavar="TIMEZONE", default=None, 
                         help="IANA timezone, e.g. 'America/New_York'") 
add_parser.add_argument('--jitter', metavar="N", default=None, type=parse_float,
                         help="Randomly jitter execution +/- N seconds") 
add_parser.set_defaults(func=call_add_cmd)


list_parser = subparser.add_parser("list", help="List all scheduled task sequences",
                                   description="List all scheduled task sequences")
list_parser.set_defaults(func=call_list_cmd)


status_parser = subparser.add_parser("status", help="List job schedule and status",
                                     description="List job schedule and status")
status_parser.add_argument('ids', metavar="TASK:[SCHED]", type=parse_ids,
                           help="Task Sequence ID and optionally Job ID, separated by a colon")
status_parser.set_defaults(func=call_status_cmd)


cancel_parser = subparser.add_parser("cancel", help="Cancel job schedule",
                                     description="Cancel job schedule")
cancel_parser.add_argument('ids', metavar="TASK:[SCHED]", type=parse_ids,
                           help="Task Sequence ID and optionally Job ID, separated by a colon")
cancel_parser.set_defaults(func=call_cancel_cmd)


args = parser.parse_args()
args.func(args)

