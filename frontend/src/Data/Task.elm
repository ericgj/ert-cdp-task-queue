module Data.Task
    exposing
        ( Task(..)
        , TaskParam(..)
        , RelativeSizeValueData
        , RelativeTimeValueData
        , encode
        , decode
        , schema
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS
import JsonSchema.Extra exposing (wrapConstructors)
import Validation exposing (ValidationResult)

import Data.Value.TimeVar as TimeVar exposing (TimeVar)
import Data.Value.TimeUnit as TimeUnit exposing (TimeUnit)
import Data.Value.TimeComparison as TimeComparison exposing (TimeComparison)
import Data.Value.TimeRange as TimeRange exposing (TimeRange)
import Data.Value.FileSizeUnit as FileSizeUnit exposing (FileSizeUnit)
import Data.Value.FileSizeComparison as FileSizeComparison exposing (FileSizeComparison)


-- -----------------------------------------------------------------------------
-- MODEL
-- -----------------------------------------------------------------------------


type Task
    = Task (List ( String, TaskParam ))


type TaskParam
    = TextValue String
    | NumberValue Int
    | RelativeTimeValue RelativeTimeValueData
    | RelativeSizeValue RelativeSizeValueData
    | GroupTextValue (List String)
    | OptionalValueNotSpecified



-- Note


type alias RelativeTimeValueData =
    { timeVar : TimeVar
    , quantity : Int
    , unit : TimeUnit
    , comparison : TimeComparison
    , range : TimeRange
    }


type alias RelativeSizeValueData =
    { quantity : Int
    , unit : FileSizeUnit
    , comparison : FileSizeComparison
    }



-- -----------------------------------------------------------------------------
-- ENCODING/DECODING
-- -----------------------------------------------------------------------------


encode : Task -> JE.Value
encode (Task params) =
    params
        |> List.map (\( key, param ) -> JE.list [ JE.string key, encodeParam param ])
        |> JE.list


encodeParam : TaskParam -> JE.Value
encodeParam param =
    case param of
        TextValue str ->
            JE.object
                [ ( "ctor", JE.string "TextValue" )
                , ( "value", JE.string str )
                ]

        NumberValue int ->
            JE.object
                [ ( "ctor", JE.string "NumberValue" )
                , ( "value", JE.int int )
                ]

        RelativeTimeValue { timeVar, quantity, unit, comparison, range } ->
            JE.object
                [ ( "ctor", JE.string "RelativeTimeValue" )
                , ( "value"
                  , JE.object
                        [ ( "time_var", TimeVar.encode timeVar )
                        , ( "quantity", JE.int quantity )
                        , ( "unit", TimeUnit.encode unit )
                        , ( "comparison", TimeComparison.encode comparison )
                        , ( "range", TimeRange.encode range )
                        ]
                  )
                ]

        RelativeSizeValue { quantity, unit, comparison } ->
            JE.object
                [ ( "ctor", JE.string "RelativeSizeValue" )
                , ( "value"
                  , JE.object
                        [ ( "quantity", JE.int quantity )
                        , ( "unit", FileSizeUnit.encode unit )
                        , ( "comparison", FileSizeComparison.encode comparison )
                        ]
                  )
                ]

        GroupTextValue strs ->
            JE.object
                [ ( "ctor", JE.string "GroupTextValue" )
                , ( "value", JE.list (strs |> List.map JE.string) )
                ]

        OptionalValueNotSpecified ->
            JE.object
                [ ( "ctor", JE.string "OptionalValueNotSpecified" )
                , ( "value", JE.object [])
                ]

schema : JS.Schema
schema =
    JS.array
        [ JS.items paramSchema
        , JS.minItems 1
        ]

-- Note: a workaround for json-elm-schema not allowing tuple-style array items

paramSchema : JS.Schema
paramSchema =
    JS.array
        [ JS.items <| JS.oneOf [] [ JS.string [], paramSchemaHelp ]
        , JS.minItems 2
        , JS.maxItems 2
        ]

paramSchemaHelp : JS.Schema
paramSchemaHelp =
    JS.oneOf []
        [ wrapConstructors ["TextValue"] paramTextValueSchema 
        , wrapConstructors ["NumberValue"] paramNumberValueSchema 
        , wrapConstructors ["RelativeTimeValue"] paramRelativeTimeValueSchema 
        , wrapConstructors ["RelativeSizeValue"] paramRelativeSizeValueSchema 
        , wrapConstructors ["GroupTextValue"] paramGroupTextValueSchema 
        , wrapConstructors ["OptionalValueNotSpecified"] paramOptionalValueNotSpecifiedSchema 
        ]

paramTextValueSchema : JS.Schema
paramTextValueSchema =
    JS.string []

paramNumberValueSchema : JS.Schema
paramNumberValueSchema =
    JS.number []

paramRelativeTimeValueSchema : JS.Schema
paramRelativeTimeValueSchema =
    JS.object
        [ JS.properties
            [ JS.required "quantity" <| JS.number []
            , JS.required "unit" <| TimeUnit.schema
            , JS.required "comparison" <| TimeComparison.schema
            , JS.required "range" <| TimeRange.schema
            , JS.required "time_var" <| TimeVar.schema
            ]
        ]

paramRelativeSizeValueSchema : JS.Schema
paramRelativeSizeValueSchema =
    JS.object
        [ JS.properties
            [ JS.required "quantity" <| JS.number []
            , JS.required "comparison" <| FileSizeComparison.schema
            , JS.required "unit" <| JS.string []
            ]
        ]

paramGroupTextValueSchema : JS.Schema
paramGroupTextValueSchema =
    JS.array
        [ JS.items <| JS.string []
        ]

paramOptionalValueNotSpecifiedSchema : JS.Schema
paramOptionalValueNotSpecifiedSchema =
    JS.oneOf [] [ JS.null [], JS.object [] ]
    


decode : JD.Decoder Task
decode =
    JD.list decodeParamWithId
        |> JD.map Task


decodeParamWithId : JD.Decoder ( String, TaskParam )
decodeParamWithId =
    JD.map2 (,)
        (JD.index 0 JD.string)
        (JD.index 1 decodeParam)


decodeParam : JD.Decoder TaskParam
decodeParam =
    JD.field "ctor" JD.string
        |> JD.andThen decodeParamHelp


decodeParamHelp : String -> JD.Decoder TaskParam
decodeParamHelp ctor =
    case ctor of
        "TextValue" ->
            JD.field "value" JD.string
                |> JD.map TextValue

        "NumberValue" ->
            JD.field "value" JD.int
                |> JD.map NumberValue

        "RelativeTimeValue" ->
            JD.map5 RelativeTimeValueData
                (JD.field "time_var" TimeVar.decode)
                (JD.field "quantity" JD.int)
                (JD.field "unit" TimeUnit.decode)
                (JD.field "comparison" TimeComparison.decode)
                (JD.field "range" TimeRange.decode)
                |> JD.map RelativeTimeValue

        "RelativeSizeValue" ->
            JD.map3 RelativeSizeValueData
                (JD.field "quantity" JD.int)
                (JD.field "unit" FileSizeUnit.decode)
                (JD.field "comparison" FileSizeComparison.decode)
                |> JD.map RelativeSizeValue

        "GroupTextValue" ->
            JD.field "value" (JD.list JD.string)
                |> JD.map GroupTextValue

        "OptionalValueNotSpecified" ->
            JD.succeed OptionalValueNotSpecified

        _ ->
            JD.fail ("Unknown TaskParam type '" ++ ctor ++ "'")
