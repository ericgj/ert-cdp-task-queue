module Data.Value.FileSizeComparison
    exposing
        ( FileSizeComparison(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS

type FileSizeComparison
    = LessThan
    | LessThanOrEqualTo
    | GreaterThanOrEqualTo
    | GreaterThan


toString : FileSizeComparison -> String
toString fileSizeComparison =
    case fileSizeComparison of
        LessThan ->
            "<"

        LessThanOrEqualTo ->
            "<="

        GreaterThanOrEqualTo ->
            ">="

        GreaterThan ->
            ">"


fromString : String -> Maybe FileSizeComparison
fromString s =
    case s of
        "<" ->
            Just LessThan

        "<=" ->
            Just LessThanOrEqualTo

        ">=" ->
            Just GreaterThanOrEqualTo

        ">" ->
            Just GreaterThan

        _ ->
            Nothing


encode : FileSizeComparison -> JE.Value
encode =
    toString >> JE.string


schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]


decode : JD.Decoder FileSizeComparison
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown FileSizeComparison value '" ++ s ++ "'"))
            )


enum : List FileSizeComparison
enum =
    [ LessThan, LessThanOrEqualTo, GreaterThanOrEqualTo, GreaterThan ]
