module Data.Value.TimeVar
    exposing
        ( TimeVar(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS


type TimeVar
    = Submitted
    | Scheduled
    | LastQueued


toString : TimeVar -> String
toString timeVar =
    case timeVar of
        Submitted ->
            "Submitted"

        Scheduled ->
            "Scheduled"

        LastQueued ->
            "Last Queued"


fromString : String -> Maybe TimeVar
fromString s =
    case s of
        "Submitted" ->
            Just Submitted

        "Scheduled" ->
            Just Scheduled

        "Last Queued" ->
            Just LastQueued

        _ ->
            Nothing


encode : TimeVar -> JE.Value
encode =
    toString >> JE.string

schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]

decode : JD.Decoder TimeVar
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown TimeVar value '" ++ s ++ "'"))
            )


enum : List TimeVar
enum =
    [ Submitted, Scheduled, LastQueued ]
