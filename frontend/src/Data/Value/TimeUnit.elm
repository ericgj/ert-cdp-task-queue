module Data.Value.TimeUnit
    exposing
        ( TimeUnit(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS


type TimeUnit
    = Seconds
    | Minutes
    | Hours
    | Days


toString : TimeUnit -> String
toString timeUnit =
    case timeUnit of
        Seconds ->
            "Seconds"

        Minutes ->
            "Minutes"

        Hours ->
            "Hours"

        Days ->
            "Days"


fromString : String -> Maybe TimeUnit
fromString s =
    case s of
        "Seconds" ->
            Just Seconds

        "Minutes" ->
            Just Minutes

        "Hours" ->
            Just Hours

        "Days" ->
            Just Days

        _ ->
            Nothing


encode : TimeUnit -> JE.Value
encode =
    toString >> JE.string

schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]

decode : JD.Decoder TimeUnit
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown TimeUnit value '" ++ s ++ "'"))
            )


enum : List TimeUnit
enum =
    [ Seconds, Minutes, Hours, Days ]
