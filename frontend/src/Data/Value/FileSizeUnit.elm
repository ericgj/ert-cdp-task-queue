module Data.Value.FileSizeUnit
    exposing
        ( FileSizeUnit(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS


type FileSizeUnit
    = Kb
    | Mb
    | Gb
    | Tb


toString : FileSizeUnit -> String
toString fileSizeUnit =
    case fileSizeUnit of
        Kb ->
            "KB"

        Mb ->
            "MB"

        Gb ->
            "GB"

        Tb ->
            "TB"


fromString : String -> Maybe FileSizeUnit
fromString s =
    case s of
        "KB" ->
            Just Kb

        "MB" ->
            Just Mb

        "GB" ->
            Just Gb

        "TB" ->
            Just Tb

        _ ->
            Nothing


encode : FileSizeUnit -> JE.Value
encode =
    toString >> JE.string

schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]


decode : JD.Decoder FileSizeUnit
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown FileSizeUnit value '" ++ s ++ "'"))
            )


enum : List FileSizeUnit
enum =
    [ Kb, Mb, Gb, Tb ]
