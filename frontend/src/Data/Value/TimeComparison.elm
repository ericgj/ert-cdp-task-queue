module Data.Value.TimeComparison
    exposing
        ( TimeComparison(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS


type TimeComparison
    = Before
    | After


toString : TimeComparison -> String
toString timeComparison =
    case timeComparison of
        Before ->
            "Before"

        After ->
            "After"


fromString : String -> Maybe TimeComparison
fromString s =
    case s of
        "Before" ->
            Just Before

        "After" ->
            Just After

        _ ->
            Nothing


encode : TimeComparison -> JE.Value
encode =
    toString >> JE.string

schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]


decode : JD.Decoder TimeComparison
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown TimeComparison value '" ++ s ++ "'"))
            )


enum : List TimeComparison
enum =
    [ Before, After ]
