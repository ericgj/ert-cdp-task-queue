module Data.Value.TimeRange
    exposing
        ( TimeRange(..)
        , toString
        , fromString
        , encode
        , schema
        , decode
        , enum
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS


type TimeRange
    = Within
    | AtLeast


toString : TimeRange -> String
toString timeRange =
    case timeRange of
        Within ->
            "Within"

        AtLeast ->
            "At least"


fromString : String -> Maybe TimeRange
fromString s =
    case s of
        "Within" ->
            Just Within

        "At least" ->
            Just AtLeast

        _ ->
            Nothing


encode : TimeRange -> JE.Value
encode =
    toString >> JE.string

schema : JS.Schema
schema =
    JS.string [ JS.enum <| List.map toString enum ]


decode : JD.Decoder TimeRange
decode =
    JD.string
        |> JD.andThen
            (\s ->
                fromString s
                    |> Maybe.map (JD.succeed)
                    |> Maybe.withDefault (JD.fail ("Unknown TimeRange value '" ++ s ++ "'"))
            )


enum : List TimeRange
enum =
    [ Within, AtLeast ]
