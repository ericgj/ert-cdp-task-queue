module Data.Task.Template
    exposing
        ( Template(..)
        , encode
        , decode
        , schema
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS
import JsonSchema.Extra exposing (wrapConstructors)

import Data.Task


{--
Note this type encapsulates validated, concrete template values corresponding
to the forms in View.Task.Template.TemplateForm.

Parallel to Data.Task.Task in relation to View.Task.TaskForm. They are basically
just encapsulating the generic Task/TaskForm with a typed Template/TemplateForm.
--}


type Template
    = CheckFiles Data.Task.Task
    | Sas Data.Task.Task



-- ----------------------------------------------------------------------------
-- ENCODE/DECODE
-- -----------------------------------------------------------------------------


encode : Template -> JE.Value
encode template =
    let
        wrap ctor value =
            JE.object
                [ ( "ctor", JE.string ctor )
                , ( "value", value )
                ]
    in
        case template of
            CheckFiles task ->
                wrap "CheckFiles" <| Data.Task.encode task

            Sas task ->
                wrap "Sas" <| Data.Task.encode task

schema : JS.Schema
schema =
    wrapConstructors [ "CheckFiles", "Sas" ] Data.Task.schema


decode : JD.Decoder Template
decode =
    JD.field "ctor" JD.string
        |> JD.andThen decodeHelp


decodeHelp : String -> JD.Decoder Template
decodeHelp ctor =
    case ctor of
        "CheckFiles" ->
            JD.field "value" Data.Task.decode
                |> JD.map CheckFiles

        "Sas" ->
            JD.field "value" Data.Task.decode
                |> JD.map Sas

        _ ->
            JD.fail <| "Unknown Template '" ++ ctor ++ "'"
