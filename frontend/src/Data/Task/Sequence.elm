module Data.Task.Sequence
    exposing
        ( Sequence(..)
        , encode
        , decode
        , schema
        )

import Json.Encode as JE
import Json.Decode as JD
import JsonSchema as JS
import JsonSchema.Extra exposing (wrapConstructors)

import Data.Task.Template exposing (Template)


type Sequence
    = Sequence
        { name : String
        , templates : List Template
        }



-- ----------------------------------------------------------------------------
-- ENCODE/DECODE
-- -----------------------------------------------------------------------------


encode : Sequence -> JE.Value
encode (Sequence { name, templates }) =
    JE.object
        [ ( "ctor", JE.string "Sequence" )
        , ( "value"
          , JE.object
                [ ( "name", JE.string name )
                , ( "templates"
                  , templates |> List.map Data.Task.Template.encode |> JE.list
                  )
                ]
          )
        ]

schema : JS.Schema
schema =
    wrapConstructors ["Sequence"] <|
        JS.object
            [ JS.properties
                [ JS.required "name" <| JS.string []
                , JS.required "templates" <|
                    JS.array
                        [ JS.items <| Data.Task.Template.schema
                        , JS.minItems 1
                        ]
                ]
            ]

decode : JD.Decoder Sequence
decode =
    JD.field "ctor" JD.string
        |> JD.andThen decodeHelp


decodeHelp : String -> JD.Decoder Sequence
decodeHelp ctor =
    case ctor of
        "Sequence" ->
            JD.map2
                (\name templates ->
                    Sequence { name = name, templates = templates }
                )
                (JD.field "name" JD.string)
                (JD.field "templates" (JD.list Data.Task.Template.decode))

        _ ->
            JD.fail ("Unknown Sequence '" ++ ctor ++ "'")




