module DemoTask exposing (main)

import Json.Encode as JE
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Validation
import Data.Value.TimeVar as TimeVar
import Data.Value.TimeUnit as TimeUnit
import Data.Value.TimeComparison as TimeComparison
import Data.Value.TimeRange as TimeRange
import Data.Value.FileSizeComparison as FileSizeComparison
import Data.Task
import View.Task exposing (taskForm, textParam, relativeTimeParam, relativeSizeParam, withId, required, optional)


type alias Model =
    ( View.Task.TaskForm, Maybe Data.Task.Task )


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = init
        , view = view
        , update = update
        }


checkFilesTask =
    taskForm "Check that files exist"
        [ textParam "in directory"
            |> withId "directory"
            |> required
        , textParam "files named"
            |> withId "fileglob"
            |> required
        , relativeTimeParam "Modified"
            { timeVar = Validation.valid TimeVar.Scheduled
            , unit = Validation.valid TimeUnit.Hours
            , comparison = Validation.valid TimeComparison.After
            , range = Validation.valid TimeRange.Within
            }
            |> optional False
        , relativeSizeParam "File size is"
            { unit = Validation.initial
            , comparison = Validation.valid FileSizeComparison.GreaterThanOrEqualTo
            }
            |> withId "filesize"
            |> optional False
        ]


init : Model
init =
    ( checkFilesTask, Nothing )


type Msg
    = UpdateTask View.Task.Msg
    | Save
    | Load


update : Msg -> Model -> Model
update msg ( form, mtask ) =
    case msg of
        UpdateTask submsg ->
            ( View.Task.update submsg form, mtask )

        Save ->
            View.Task.validate form
                |> Validation.map (\task -> ( form, Just task ))
                |> Validation.withDefault ( form, Nothing )

        Load ->
            case mtask of
                Nothing ->
                    ( form, mtask )

                Just task ->
                    View.Task.load task form
                        |> Result.mapError (Debug.log "Error loading")
                        |> Result.map (\newForm -> ( newForm, Nothing ))
                        |> Result.withDefault ( form, mtask )


view : Model -> Html Msg
view ( form, mtask ) =
    Html.div
        []
        [ View.Task.view form |> Html.map UpdateTask
        , viewSave form
        , Html.div
            []
            ([ viewDescription form ]
                ++ (mtask
                        |> Maybe.map (viewValidTask >> List.singleton)
                        |> Maybe.withDefault []
                   )
            )
        ]


viewSave : View.Task.TaskForm -> Html Msg
viewSave form =
    Html.button
        [ Events.onClick Save
        , Attrs.disabled
            (View.Task.validate form
                |> Validation.map (always False)
                |> Validation.withDefault True
            )
        ]
        [ Html.text "Save" ]


viewDescription : View.Task.TaskForm -> Html msg
viewDescription form =
    Html.div
        []
        [ Html.label [] [ Html.text "Description:" ]
        , Html.p [] [ Html.text <| View.Task.describe form ]
        ]


viewValidTask : Data.Task.Task -> Html Msg
viewValidTask task =
    Html.div
        []
        [ Html.label [] [ Html.text "Resulting task:" ]
        , Html.p [] [ Html.text <| JE.encode 2 <| Data.Task.encode <| task ]
        , Html.label [] [ Html.text "Now try reloading it:" ]
        , Html.button
            [ Events.onClick Load
            ]
            [ Html.text "Reload" ]
        ]
