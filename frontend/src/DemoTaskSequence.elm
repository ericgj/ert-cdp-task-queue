module DemoTaskSequence exposing (main)

import Tuple
import Json.Encode as JE
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Data.Task
import View.Task.Template as Template exposing (TemplateForm)
import View.Task.Sequence as Sequence exposing (SequenceForm)


type alias Model =
    SequenceForm


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = init
        , view = view
        , update = update
        }


init : Model
init =
    Sequence.empty


templates : List TemplateForm
templates =
    [ Template.checkFiles
    , Template.sas
    ]


type Msg
    = UpdateSequence Sequence.Msg


update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateSequence seqmsg ->
            Sequence.update seqmsg model |> Tuple.first


view : Model -> Html Msg
view model =
    Html.div
        []
        [ Sequence.view templates model |> Html.map UpdateSequence
        ]
