module JsonSchema.Extra exposing
    ( wrapConstructors
    )

import JsonSchema as JS

wrapConstructors : List String -> JS.Schema -> JS.Schema
wrapConstructors ctors inner =
    JS.object
        [ JS.properties
            [ JS.required "ctor" <| JS.string [ JS.enum ctors ]
            , JS.required "value" inner
            ]
        ]


