module Validation.Extra
    exposing
        ( requiredString
        , requiredStringWithMessage
        )


requiredString : String -> Result String String
requiredString =
    requiredStringWithMessage "Required"


requiredStringWithMessage : String -> String -> Result String String
requiredStringWithMessage message s =
    if s == "" then
        Err message
    else
        Ok s
