module View.Task
    exposing
        ( Form
        , TaskForm
        , TaskParamForm
        , Msg
        , update
        , view
        , validate
        , load
        , describe
        , taskForm
        , unlabelledTaskForm
        , optional
        , required
        , textParam
        , numberParam
        , relativeSizeParam
        , relativeTimeParam
        , groupTextParam
        , withLabel
        , withId
        , withWidth
        , withText
        , validateTextWith
        , validateAllTextWith
        , withNumber
        , withMin
        , withMax
        , withStep
        , validateNumberWith
        , addTextParam
        )

import Json.Decode as JD
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Html.Extra exposing (onBlur, onChange)
import Validation exposing (ValidationResult)
import Validation.Extra exposing (requiredString)
import Slug
import Data.Task exposing (Task(..), TaskParam(..), RelativeTimeValueData, RelativeSizeValueData)
import Data.Value.TimeVar as TimeVar exposing (TimeVar)
import Data.Value.TimeUnit as TimeUnit exposing (TimeUnit)
import Data.Value.TimeComparison as TimeComparison exposing (TimeComparison)
import Data.Value.TimeRange as TimeRange exposing (TimeRange)
import Data.Value.FileSizeUnit as FileSizeUnit exposing (FileSizeUnit)
import Data.Value.FileSizeComparison as FileSizeComparison exposing (FileSizeComparison)


-- -----------------------------------------------------------------------------
-- MODEL
-- -----------------------------------------------------------------------------


type Form a
    = Required a
    | Toggle Bool a


type TaskForm
    = TaskForm
        { label : Maybe String
        , params : List (Form TaskParamForm)
        }


type TaskParamForm
    = TextParamForm CommonParamFormData TextParamFormData
    | LongTextParamForm CommonParamFormData TextParamFormData -- for textbox
    | SensitiveTextParamForm CommonParamFormData TextParamFormData SensitiveTextParamFormData
    | NumberParamForm CommonParamFormData NumberParamFormData
    | RelativeSizeParamForm CommonParamFormData RelativeSizeParamFormData
    | RelativeTimeParamForm CommonParamFormData RelativeTimeParamFormData
    | GroupTextParamForm CommonParamFormData GroupTextParamFormData


type alias CommonParamFormData =
    { label : Maybe String
    , id : String
    , width : Maybe Int
    }


type alias TextParamFormData =
    { value : ValidationResult String
    , validate : String -> Result String String
    }


type alias SensitiveTextParamFormData =
    { showText : Bool
    }


type alias NumberParamFormData =
    { value : ValidationResult Int
    , min : Maybe Int
    , max : Maybe Int
    , step : Maybe Int
    , validate : String -> Result String Int
    }


type alias EnumParamFormData enum =
    { value : ValidationResult enum
    , options : List enum
    , toString : enum -> String
    }


type alias RelativeSizeParamFormData =
    { quantity : NumberParamFormData
    , unit : EnumParamFormData FileSizeUnit
    , comparison : EnumParamFormData FileSizeComparison
    }


type alias RelativeTimeParamFormData =
    { timeVar : EnumParamFormData TimeVar
    , quantity : NumberParamFormData
    , unit : EnumParamFormData TimeUnit
    , comparison : EnumParamFormData TimeComparison
    , range : EnumParamFormData TimeRange
    }


type alias GroupTextParamFormData =
    { values : List (ValidationResult String)
    , validate : String -> Result String String
    }



-- -----------------------------------------------------------------------------
-- VALIDATION
-- -----------------------------------------------------------------------------


validate : TaskForm -> ValidationResult Task
validate (TaskForm { params }) =
    params
        |> List.map validateParamForm
        |> Validation.sequence
        |> Validation.map Task


validateParamForm : Form TaskParamForm -> ValidationResult ( String, TaskParam )
validateParamForm form =
    case form of
        Toggle False param ->
            Validation.valid
                ( (getCommonParamFormData param |> .id)
                , OptionalValueNotSpecified
                )

        Toggle True param ->
            validateParam param

        Required param ->
            validateParam param


validateParam : TaskParamForm -> ValidationResult ( String, TaskParam )
validateParam param =
    let
        injectId id =
            Validation.map (\v -> ( id, v ))
    in
        case param of
            TextParamForm { id } { value } ->
                Validation.map TextValue value
                    |> injectId id

            LongTextParamForm { id } { value } ->
                Validation.map TextValue value
                    |> injectId id

            SensitiveTextParamForm { id } { value } _ ->
                Validation.map TextValue value
                    |> injectId id

            NumberParamForm { id } { value } ->
                Validation.map NumberValue value
                    |> injectId id

            RelativeSizeParamForm { id } { quantity, unit, comparison } ->
                Validation.valid RelativeSizeValueData
                    |> Validation.andMap quantity.value
                    |> Validation.andMap unit.value
                    |> Validation.andMap comparison.value
                    |> Validation.map RelativeSizeValue
                    |> injectId id

            RelativeTimeParamForm { id } { timeVar, quantity, unit, comparison, range } ->
                Validation.valid RelativeTimeValueData
                    |> Validation.andMap timeVar.value
                    |> Validation.andMap quantity.value
                    |> Validation.andMap unit.value
                    |> Validation.andMap comparison.value
                    |> Validation.andMap range.value
                    |> Validation.map RelativeTimeValue
                    |> injectId id

            GroupTextParamForm { id } { values } ->
                values
                    |> Validation.sequence
                    |> Validation.map GroupTextValue
                    |> injectId id



-- -----------------------------------------------------------------------------
-- LOADING TASKS INTO TASKFORMS
-- -----------------------------------------------------------------------------


load : Task -> TaskForm -> Result String TaskForm
load (Task taskParams) (TaskForm form) =
    let
        wrap params =
            TaskForm { form | params = params }
    in
        form.params
            |> List.map (loadTaskParamForm <| Dict.fromList taskParams)
            |> resultSequence
            |> Result.map wrap



-- TODO: set toggle state based on taskParams


loadTaskParamForm : Dict String TaskParam -> Form TaskParamForm -> Result String (Form TaskParamForm)
loadTaskParamForm taskParams paramForm =
    case paramForm of
        Required param ->
            loadTaskParam True taskParams param
                |> Result.map Required

        Toggle state param ->
            loadTaskParam False taskParams param
                |> Result.map (Toggle state)


loadTaskParam : Bool -> Dict String TaskParam -> TaskParamForm -> Result String TaskParamForm
loadTaskParam required taskParams param =
    let
        id =
            param |> getCommonParamFormData |> .id
    in
        Dict.get id taskParams
            |> Maybe.map (\taskParam -> loadTaskParamFrom required taskParam param)
            |> Maybe.withDefault (Err ("No such task parameter: '" ++ id ++ "'"))


loadTaskParamFrom : Bool -> TaskParam -> TaskParamForm -> Result String TaskParamForm
loadTaskParamFrom required taskParam param =
    let
        errorIfRequired id =
            if required then
                Err ("Expecting value for '" ++ id ++ "'")
            else
                Ok param
    in
        case param of
            TextParamForm common data ->
                case taskParam of
                    TextValue value ->
                        Ok <|
                            TextParamForm
                                common
                                (runValidate value data)

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting TextValue for '" ++ common.id ++ "'")

            LongTextParamForm common data ->
                case taskParam of
                    TextValue value ->
                        Ok <|
                            LongTextParamForm
                                common
                                (runValidate value data)

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting TextValue for '" ++ common.id ++ "'")

            SensitiveTextParamForm common data flag ->
                case taskParam of
                    TextValue value ->
                        Ok <|
                            SensitiveTextParamForm
                                common
                                (runValidate value data)
                                flag

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting TextValue for '" ++ common.id ++ "'")

            -- Note: Not crazy about revalidating numbers this way, but seems the easiest
            NumberParamForm common data ->
                case taskParam of
                    NumberValue value ->
                        Ok <|
                            NumberParamForm
                                common
                                (runValidate (toString value) data)

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting NumberValue for '" ++ common.id ++ "'")

            RelativeSizeParamForm common data ->
                case taskParam of
                    RelativeSizeValue { quantity, unit, comparison } ->
                        Ok <|
                            RelativeSizeParamForm
                                common
                                { data
                                    | quantity = runValidate (toString quantity) data.quantity
                                    , unit = setValid unit data.unit
                                    , comparison = setValid comparison data.comparison
                                }

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting RelativeSizeValue for '" ++ common.id ++ "'")

            RelativeTimeParamForm common data ->
                case taskParam of
                    RelativeTimeValue { timeVar, quantity, unit, comparison, range } ->
                        Ok <|
                            RelativeTimeParamForm
                                common
                                { data
                                    | timeVar = setValid timeVar data.timeVar
                                    , quantity = runValidate (toString quantity) data.quantity
                                    , unit = setValid unit data.unit
                                    , comparison = setValid comparison data.comparison
                                    , range = setValid range data.range
                                }

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting RelativeTimeValue for '" ++ common.id ++ "'")

            GroupTextParamForm common data ->
                case taskParam of
                    GroupTextValue values ->
                        Ok <|
                            GroupTextParamForm
                                common
                                { data
                                    | values =
                                        values
                                            |> List.map (Validation.validate data.validate)
                                }

                    OptionalValueNotSpecified ->
                        errorIfRequired common.id

                    _ ->
                        Err ("Expecting GroupTextValue for '" ++ common.id ++ "'")



-- UTILS


resultSequence : List (Result x a) -> Result x (List a)
resultSequence results =
    case results of
        [] ->
            Ok []

        result :: remaining ->
            Result.map2 (::)
                result
                (resultSequence remaining)



-- -----------------------------------------------------------------------------
-- UPDATE
-- -----------------------------------------------------------------------------
{--note for now, these are focused on user updates to values
whereas the builder methods above are intended for constructing task forms
--}


type Msg
    = UpdateParam Int ParamMsg


type ParamMsg
    = SetInput String
    | SetInputInGroup Int String
    | Validate String
    | ValidateInGroup Int String
    | AddGroupText (ValidationResult String)
    | ToggleParam
    | SetFileSizeUnit (ValidationResult FileSizeUnit)
    | SetFileSizeComparison (ValidationResult FileSizeComparison)
    | SetTimeVar (ValidationResult TimeVar)
    | SetTimeUnit (ValidationResult TimeUnit)
    | SetTimeComparison (ValidationResult TimeComparison)
    | SetTimeRange (ValidationResult TimeRange)


update : Msg -> TaskForm -> TaskForm
update msg (TaskForm data) =
    case msg of
        UpdateParam index paramMsg ->
            TaskForm
                { data
                    | params =
                        List.indexedMap
                            (\i param ->
                                if i == index then
                                    updateWrappedTaskParamForm paramMsg param
                                else
                                    param
                            )
                            data.params
                }


updateWrappedTaskParamForm : ParamMsg -> Form TaskParamForm -> Form TaskParamForm
updateWrappedTaskParamForm msg form =
    case form of
        Toggle state param ->
            if msg == ToggleParam then
                Toggle (not state) param
            else
                Toggle state (updateTaskParamForm msg param)

        Required param ->
            Required (updateTaskParamForm msg param)


updateTaskParamForm : ParamMsg -> TaskParamForm -> TaskParamForm
updateTaskParamForm msg param =
    case msg of
        ToggleParam ->
            param

        SetInput s ->
            updateTaskParamFormSetInput s param

        SetInputInGroup index s ->
            updateTaskParamFormSetInputInGroup index s param

        AddGroupText value ->
            updateTaskParamFormAddGroupText value param

        Validate s ->
            updateTaskParamFormValidate s param

        ValidateInGroup index s ->
            updateTaskParamFormValidateInGroup index s param

        SetFileSizeUnit unit ->
            updateTaskParamFormSetFileSizeUnit unit param

        SetFileSizeComparison comparison ->
            updateTaskParamFormSetFileSizeComparison comparison param

        SetTimeVar timeVar ->
            updateTaskParamFormSetTimeVar timeVar param

        SetTimeUnit unit ->
            updateTaskParamFormSetTimeUnit unit param

        SetTimeComparison comparison ->
            updateTaskParamFormSetTimeComparison comparison param

        SetTimeRange range ->
            updateTaskParamFormSetTimeRange range param


updateTaskParamFormSetInput : String -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetInput input param =
    let
        setUnvalidated data =
            { data | value = Validation.unvalidated input }

        setQuantity value data =
            { data | quantity = value }
    in
        case param of
            TextParamForm common data ->
                TextParamForm common (setUnvalidated data)

            LongTextParamForm common data ->
                LongTextParamForm common (setUnvalidated data)

            SensitiveTextParamForm common textdata data ->
                SensitiveTextParamForm common (setUnvalidated textdata) data

            NumberParamForm common data ->
                NumberParamForm common (setUnvalidated data)

            RelativeSizeParamForm common data ->
                RelativeSizeParamForm common { data | quantity = setUnvalidated data.quantity }

            RelativeTimeParamForm common data ->
                RelativeTimeParamForm common { data | quantity = setUnvalidated data.quantity }

            -- no-op for these; use SetInputInGroup to update single text within group
            GroupTextParamForm common data ->
                param


updateTaskParamFormSetInputInGroup : Int -> String -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetInputInGroup index input param =
    case param of
        GroupTextParamForm common data ->
            GroupTextParamForm common
                { data
                    | values =
                        List.indexedMap
                            (\i value ->
                                if i == index then
                                    Validation.unvalidated input
                                else
                                    value
                            )
                            data.values
                }

        _ ->
            param


updateTaskParamFormAddGroupText : ValidationResult String -> TaskParamForm -> TaskParamForm
updateTaskParamFormAddGroupText value param =
    case param of
        GroupTextParamForm common data ->
            GroupTextParamForm common
                { data | values = value :: data.values }

        _ ->
            param


updateTaskParamFormValidate : String -> TaskParamForm -> TaskParamForm
updateTaskParamFormValidate input param =
    case param of
        TextParamForm common data ->
            TextParamForm common (runValidate input data)

        LongTextParamForm common data ->
            LongTextParamForm common (runValidate input data)

        SensitiveTextParamForm common textdata data ->
            SensitiveTextParamForm common (runValidate input textdata) data

        NumberParamForm common data ->
            NumberParamForm common (runValidate input data)

        RelativeSizeParamForm common data ->
            RelativeSizeParamForm common
                { data
                    | quantity = runValidate input data.quantity
                }

        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common
                { data
                    | quantity = runValidate input data.quantity
                }

        GroupTextParamForm common data ->
            param


updateTaskParamFormValidateInGroup : Int -> String -> TaskParamForm -> TaskParamForm
updateTaskParamFormValidateInGroup index input param =
    case param of
        GroupTextParamForm common data ->
            GroupTextParamForm common
                { data
                    | values =
                        List.indexedMap
                            (\i value ->
                                if i == index then
                                    Validation.validate data.validate input
                                else
                                    value
                            )
                            data.values
                }

        _ ->
            param


updateTaskParamFormSetFileSizeUnit : ValidationResult FileSizeUnit -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetFileSizeUnit unit param =
    case param of
        RelativeSizeParamForm common data ->
            RelativeSizeParamForm common
                { data | unit = setEnumParamFormValue unit data.unit }

        _ ->
            param


updateTaskParamFormSetFileSizeComparison : ValidationResult FileSizeComparison -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetFileSizeComparison comparison param =
    case param of
        RelativeSizeParamForm common data ->
            RelativeSizeParamForm common
                { data | comparison = setEnumParamFormValue comparison data.comparison }

        _ ->
            param


updateTaskParamFormSetTimeVar : ValidationResult TimeVar -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetTimeVar timeVar param =
    case param of
        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common
                { data | timeVar = setEnumParamFormValue timeVar data.timeVar }

        _ ->
            param


updateTaskParamFormSetTimeUnit : ValidationResult TimeUnit -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetTimeUnit unit param =
    case param of
        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common
                { data | unit = setEnumParamFormValue unit data.unit }

        _ ->
            param


updateTaskParamFormSetTimeComparison : ValidationResult TimeComparison -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetTimeComparison comparison param =
    case param of
        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common
                { data | comparison = setEnumParamFormValue comparison data.comparison }

        _ ->
            param


updateTaskParamFormSetTimeRange : ValidationResult TimeRange -> TaskParamForm -> TaskParamForm
updateTaskParamFormSetTimeRange range param =
    case param of
        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common
                { data | range = setEnumParamFormValue range data.range }

        _ ->
            param


runValidate :
    String
    -> { a | value : ValidationResult b, validate : String -> Result String b }
    -> { a | value : ValidationResult b, validate : String -> Result String b }
runValidate input data =
    { data | value = Validation.validate data.validate input }


setValid :
    b
    -> { a | value : ValidationResult b }
    -> { a | value : ValidationResult b }
setValid value data =
    { data | value = Validation.valid value }


setEnumParamFormValue : ValidationResult a -> EnumParamFormData a -> EnumParamFormData a
setEnumParamFormValue value enumForm =
    { enumForm | value = value }



-- -----------------------------------------------------------------------------
-- GETTERS
-- -----------------------------------------------------------------------------


describe : TaskForm -> String
describe (TaskForm { label, params }) =
    ((label |> Maybe.withDefault "")
        :: [ List.filterMap describeParamForm params
                |> String.join ", "
           ]
    )
        |> String.join " "


describeParamForm : Form TaskParamForm -> Maybe String
describeParamForm form =
    case form of
        Required param ->
            Just <| describeParamFormHelp param

        Toggle True param ->
            Just <| describeParamFormHelp param

        Toggle False _ ->
            Nothing


describeParamFormHelp : TaskParamForm -> String
describeParamFormHelp param =
    let
        stringify sensitive toString label data =
            ((label |> Maybe.withDefault "")
                :: [ describeParamValue sensitive toString data.value ]
            )
                |> String.join " "
    in
        case param of
            TextParamForm common data ->
                stringify False identity common.label data

            LongTextParamForm common data ->
                stringify False identity common.label data

            SensitiveTextParamForm common data _ ->
                stringify True identity common.label data

            NumberParamForm common data ->
                stringify False toString common.label data

            RelativeSizeParamForm common { quantity, unit, comparison } ->
                [ common.label |> Maybe.withDefault ""
                , stringify False (FileSizeComparison.toString) Nothing comparison
                , stringify False toString Nothing quantity
                , stringify False (FileSizeUnit.toString) Nothing unit
                ]
                    |> String.join " "

            RelativeTimeParamForm common { quantity, unit, comparison, timeVar, range } ->
                [ common.label |> Maybe.withDefault ""
                , stringify False (TimeRange.toString) Nothing range
                , stringify False toString Nothing quantity
                , stringify False (TimeUnit.toString) Nothing unit
                , stringify False (TimeComparison.toString) Nothing comparison
                , stringify False (TimeVar.toString) Nothing timeVar
                ]
                    |> String.join " "

            GroupTextParamForm common { values } ->
                [ common.label |> Maybe.withDefault ""
                , List.length values
                    |> (\n ->
                            if n == 0 then
                                "(None)"
                            else
                                ("(" ++ (toString n) ++ ")")
                       )
                ]
                    |> String.join " "


describeParamValue : Bool -> (a -> String) -> ValidationResult a -> String
describeParamValue sensitive toString value =
    let
        stringify s =
            if sensitive then
                "*"
            else if (String.length s > 10) then
                (String.left 10 s) ++ "..."
            else
                s
    in
        case value of
            Validation.Initial ->
                "?"

            Validation.Unvalidated s ->
                s

            Validation.Invalid _ s ->
                s

            Validation.Valid v ->
                (toString v)


getCommonParamFormData : TaskParamForm -> CommonParamFormData
getCommonParamFormData param =
    case param of
        TextParamForm common data ->
            common

        LongTextParamForm common data ->
            common

        SensitiveTextParamForm common textdata data ->
            common

        NumberParamForm common data ->
            common

        RelativeSizeParamForm common data ->
            common

        RelativeTimeParamForm common data ->
            common

        GroupTextParamForm common data ->
            common



-- -----------------------------------------------------------------------------
-- VIEW
-- -----------------------------------------------------------------------------


view : TaskForm -> Html Msg
view ((TaskForm { label, params }) as taskForm) =
    Html.div
        []
        ((maybeView viewHeader label)
            ++ [ Html.div
                    []
                    (List.indexedMap
                        (\i p -> viewParamForm p |> Html.map (UpdateParam i))
                        params
                    )
               ]
        )


viewHeader : String -> Html msg
viewHeader label =
    Html.h2
        []
        [ Html.text label ]


viewParamForm : Form TaskParamForm -> Html ParamMsg
viewParamForm form =
    case form of
        Required param ->
            viewParam param

        Toggle state param ->
            viewParamFormHelp state param


viewParamFormHelp : Bool -> TaskParamForm -> Html ParamMsg
viewParamFormHelp toggleState param =
    let
        common =
            getCommonParamFormData param

        toggleId =
            common.id ++ "-toggle"
    in
        Html.div
            []
            ([ Html.div
                []
                ([ Html.input
                    [ Attrs.type_ "checkbox"
                    , Attrs.checked toggleState
                    , Events.onClick ToggleParam
                    , Attrs.id toggleId
                    ]
                    []
                 ]
                    ++ (maybeView (viewLabelFor toggleId) common.label)
                )
             ]
                ++ (if toggleState then
                        [ Html.div
                            []
                            [ viewParam param ]
                        ]
                    else
                        []
                   )
            )


viewParam : TaskParamForm -> Html ParamMsg
viewParam param =
    case param of
        TextParamForm common data ->
            viewTextParam common data

        {--
        LongTextParamForm common data ->
            viewLongTextParam common data

        SensitiveTextParamForm common textdata data ->
            viewSensitiveTextParam common textdata data

        --}
        NumberParamForm common data ->
            viewNumberParam common data

        RelativeSizeParamForm common data ->
            viewRelativeSizeParam common data

        RelativeTimeParamForm common data ->
            viewRelativeTimeParam common data

        GroupTextParamForm common data ->
            viewGroupTextParam common data

        _ ->
            Debug.crash "TBD"


viewTextParam : CommonParamFormData -> TextParamFormData -> Html ParamMsg
viewTextParam { id, label, width } { value, validate } =
    Html.div
        []
        ((maybeView (viewLabelFor id) label)
            ++ [ Html.input
                    [ Attrs.type_ "text"
                    , Attrs.id id
                    , Attrs.style
                        (Maybe.map styleWidth width |> Maybe.withDefault [])
                    , Attrs.value (Validation.toString identity value)
                    , Events.onInput SetInput
                    , onBlur Validate
                    ]
                    []
               , Html.div
                    []
                    [ Html.text
                        (Validation.message value |> Maybe.withDefault "")
                    ]
               ]
        )


viewNumberParam : CommonParamFormData -> NumberParamFormData -> Html ParamMsg
viewNumberParam { id, label, width } { value, validate, min, max, step } =
    let
        viewNumAttr attr n =
            attr (toString n)
    in
        Html.div
            []
            ((maybeView (viewLabelFor id) label)
                ++ [ Html.input
                        ([ Attrs.type_ "number"
                         , Attrs.id id
                         , Attrs.style
                            (Maybe.map styleWidth width |> Maybe.withDefault [])
                         , Attrs.value (Validation.toString toString value)
                         , Events.onInput SetInput
                         , onBlur Validate
                         ]
                            ++ (maybeAttribute (viewNumAttr Attrs.min) min)
                            ++ (maybeAttribute (viewNumAttr Attrs.max) max)
                            ++ (maybeAttribute (viewNumAttr Attrs.step) step)
                        )
                        []
                   , Html.div
                        []
                        [ Html.text
                            (Validation.message value |> Maybe.withDefault "")
                        ]
                   ]
            )


viewRelativeSizeParam : CommonParamFormData -> RelativeSizeParamFormData -> Html ParamMsg
viewRelativeSizeParam { id, label, width } { comparison, quantity, unit } =
    Html.div
        []
        ((maybeView (viewLabelFor id) label)
            ++ [ Html.div
                    []
                    [ viewEnumParamFormData SetFileSizeComparison id comparison
                    , viewNumberParam
                        { id = (id ++ "-quantity")
                        , label = Nothing
                        , width = Nothing
                        }
                        quantity
                    , viewEnumParamFormData SetFileSizeUnit (id ++ "-unit") unit
                    ]
               ]
        )


viewRelativeTimeParam : CommonParamFormData -> RelativeTimeParamFormData -> Html ParamMsg
viewRelativeTimeParam { id, label, width } { range, quantity, unit, comparison, timeVar } =
    Html.div
        []
        ((maybeView (viewLabelFor id) label)
            ++ [ Html.div
                    []
                    [ viewEnumParamFormData SetTimeRange (id ++ "-range") range
                    , viewNumberParam
                        { id = id
                        , label = Nothing
                        , width = Nothing
                        }
                        quantity
                    , viewEnumParamFormData SetTimeUnit (id ++ "-unit") unit
                    , viewEnumParamFormData SetTimeComparison (id ++ "-comparison") comparison
                    , viewEnumParamFormData SetTimeVar (id ++ "-timevar") timeVar
                    ]
               ]
        )


viewEnumParamFormData : (ValidationResult a -> ParamMsg) -> String -> EnumParamFormData a -> Html ParamMsg
viewEnumParamFormData toMsg id { value, options, toString } =
    let
        find =
            String.toInt
                >> Result.toMaybe
                >> Maybe.map (\i -> List.drop i options)
                >> Maybe.andThen List.head
                >> Validation.fromMaybe "Required" ""
    in
        Html.div
            []
            [ Html.select
                [ Attrs.id id
                , onChange (find >> toMsg)
                ]
                ((Html.option [] [ Html.text "" ])
                    :: (List.indexedMap (viewEnumParamFormDataOption toString value) options)
                )
            , Html.div
                []
                [ Html.text
                    (Validation.message value |> Maybe.withDefault "")
                ]
            ]


viewEnumParamFormDataOption : (a -> String) -> ValidationResult a -> Int -> a -> Html msg
viewEnumParamFormDataOption toString selectedOption i option =
    Html.option
        [ Attrs.value (Basics.toString i)
        , Attrs.selected
            (selectedOption
                |> Validation.map ((==) option)
                |> Validation.withDefault False
            )
        ]
        [ Html.text (toString option) ]


viewGroupTextParam : CommonParamFormData -> GroupTextParamFormData -> Html ParamMsg
viewGroupTextParam { id, label, width } { values, validate } =
    let
        viewInput index value =
            Html.div
                []
                [ Html.input
                    [ Attrs.type_ "text"
                    , Attrs.id (id ++ "-" ++ (toString index))
                    , Attrs.value (Validation.toString identity value)
                    , Attrs.style
                        (Maybe.map styleWidth width |> Maybe.withDefault [])
                    , Events.onInput (SetInputInGroup index)
                    , onBlur (ValidateInGroup index)
                    ]
                    []
                , Html.div
                    []
                    [ Html.text
                        (Validation.message value |> Maybe.withDefault "")
                    ]
                ]
    in
        Html.div
            []
            ((maybeView (viewLabelFor id) label)
                ++ (values |> List.indexedMap viewInput |> List.reverse)
                ++ [ Html.div
                        []
                        [ Html.button
                            [ Events.onClick (AddGroupText Validation.initial)
                            ]
                            [ Html.text "+"
                            ]
                        ]
                   ]
            )



-- UTILS


viewLabelFor : String -> String -> Html msg
viewLabelFor id label =
    Html.label [ Attrs.for id ] [ Html.text label ]


styleWidth : Int -> List ( String, String )
styleWidth w =
    [ ( "width", ((toString w) ++ "rem") ) ]


maybeAttribute : (a -> Html.Attribute msg) -> Maybe a -> List (Html.Attribute msg)
maybeAttribute fn maybe =
    maybe |> Maybe.map (fn >> List.singleton) |> Maybe.withDefault []


maybeView : (a -> Html msg) -> Maybe a -> List (Html msg)
maybeView fn maybe =
    maybe |> Maybe.map (fn >> List.singleton) |> Maybe.withDefault []



-- -----------------------------------------------------------------------------
-- CONSTRUCTING TASKS
-- -----------------------------------------------------------------------------
{--For example,

    checkFilesTask = taskForm "Check that files exist"
        [ textParam "in directory"
            |> withId "directory"
            |> validateTextWith validDirectory
            |> required
        , textParam "files named"
            |> withId "fileglob"
            |> validateTextWith validFileGlob
            |> required
        , relativeTimeParam "Modified"
            { timeVar = Nothing
            , unit = Nothing
            , comparison = Just TimeUnit.After
            , range = Just TimeRange.Within
            }
            |> optional
        , relativeSizeParam "File size is"
            { unit = Nothing
            , comparison = Just FileSizeComparison.GreaterThanOrEqualTo
            }
            |> withId "filesize"
            |> optional
        ]

--}


taskForm : String -> List (Form TaskParamForm) -> TaskForm
taskForm label params =
    TaskForm
        { label = Just label
        , params = params
        }


unlabelledTaskForm : List (Form TaskParamForm) -> TaskForm
unlabelledTaskForm params =
    TaskForm
        { label = Nothing
        , params = params
        }


optional : Bool -> TaskParamForm -> Form TaskParamForm
optional toggled param =
    Toggle toggled param


required : TaskParamForm -> Form TaskParamForm
required param =
    Required param


textParam : String -> TaskParamForm
textParam label =
    slug label
        |> Maybe.withDefault label
        |> emptyTextParamForm
        |> withLabel label


numberParam : String -> TaskParamForm
numberParam label =
    slug label
        |> Maybe.withDefault label
        |> emptyNumberParamForm
        |> withLabel label


relativeSizeParam :
    String
    ->
        { a
            | unit : ValidationResult FileSizeUnit
            , comparison : ValidationResult FileSizeComparison
        }
    -> TaskParamForm
relativeSizeParam label defaults =
    slug label
        |> Maybe.withDefault label
        |> (\id -> emptyRelativeSizeParamFormWithDefaults id defaults)
        |> withLabel label


relativeTimeParam :
    String
    ->
        { a
            | timeVar : ValidationResult TimeVar
            , unit : ValidationResult TimeUnit
            , comparison : ValidationResult TimeComparison
            , range : ValidationResult TimeRange
        }
    -> TaskParamForm
relativeTimeParam label defaults =
    slug label
        |> Maybe.withDefault label
        |> (\id -> emptyRelativeTimeParamFormWithDefaults id defaults)
        |> withLabel label


groupTextParam : String -> TaskParamForm
groupTextParam label =
    slug label
        |> Maybe.withDefault label
        |> emptyGroupTextParamForm
        |> withLabel label


withLabel : String -> TaskParamForm -> TaskParamForm
withLabel label =
    mapCommonParamFormData (\common -> { common | label = Just label })


withId : String -> TaskParamForm -> TaskParamForm
withId id =
    mapCommonParamFormData (\common -> { common | id = id })


withWidth : Int -> TaskParamForm -> TaskParamForm
withWidth width =
    mapCommonParamFormData (\common -> { common | width = Just width })


withText : String -> TaskParamForm -> TaskParamForm
withText value =
    mapTextParamFormData (runValidate value)


validateTextWith : (String -> Result String String) -> TaskParamForm -> TaskParamForm
validateTextWith fn =
    mapTextParamFormData (\text -> { text | validate = fn })


validateAllTextWith : (String -> Result String String) -> TaskParamForm -> TaskParamForm
validateAllTextWith fn param =
    case param of
        GroupTextParamForm common data ->
            GroupTextParamForm common { data | validate = fn }

        _ ->
            param


withNumber : Int -> TaskParamForm -> TaskParamForm
withNumber value =
    mapNumberParamFormData (runValidate (toString value))


withMin : Int -> TaskParamForm -> TaskParamForm
withMin min =
    mapNumberParamFormData (\number -> { number | min = Just min })


withMax : Int -> TaskParamForm -> TaskParamForm
withMax max =
    mapNumberParamFormData (\number -> { number | max = Just max })


withStep : Int -> TaskParamForm -> TaskParamForm
withStep step =
    mapNumberParamFormData (\number -> { number | step = Just step })


validateNumberWith : (String -> Result String Int) -> TaskParamForm -> TaskParamForm
validateNumberWith fn =
    mapNumberParamFormData (\number -> { number | validate = fn })


addTextParam : String -> TaskParamForm -> TaskParamForm
addTextParam value param =
    case param of
        GroupTextParamForm common data ->
            GroupTextParamForm common
                { data | values = (Validation.validate data.validate value) :: data.values }

        _ ->
            param


mapCommonParamFormData : (CommonParamFormData -> CommonParamFormData) -> TaskParamForm -> TaskParamForm
mapCommonParamFormData map_ param =
    case param of
        TextParamForm common data ->
            TextParamForm (map_ common) data

        LongTextParamForm common data ->
            LongTextParamForm (map_ common) data

        SensitiveTextParamForm common textdata data ->
            SensitiveTextParamForm (map_ common) textdata data

        NumberParamForm common data ->
            NumberParamForm (map_ common) data

        RelativeSizeParamForm common data ->
            RelativeSizeParamForm (map_ common) data

        RelativeTimeParamForm common data ->
            RelativeTimeParamForm (map_ common) data

        GroupTextParamForm common data ->
            GroupTextParamForm (map_ common) data


mapTextParamFormData : (TextParamFormData -> TextParamFormData) -> TaskParamForm -> TaskParamForm
mapTextParamFormData map_ param =
    case param of
        TextParamForm common data ->
            TextParamForm common (map_ data)

        LongTextParamForm common data ->
            LongTextParamForm common (map_ data)

        SensitiveTextParamForm common textdata data ->
            SensitiveTextParamForm common (map_ textdata) data

        NumberParamForm common data ->
            param

        RelativeSizeParamForm common data ->
            param

        RelativeTimeParamForm common data ->
            param

        GroupTextParamForm common data ->
            param


mapNumberParamFormData : (NumberParamFormData -> NumberParamFormData) -> TaskParamForm -> TaskParamForm
mapNumberParamFormData map_ param =
    case param of
        TextParamForm common data ->
            param

        LongTextParamForm common data ->
            param

        SensitiveTextParamForm common textdata data ->
            param

        NumberParamForm common data ->
            NumberParamForm common (map_ data)

        RelativeSizeParamForm common data ->
            RelativeSizeParamForm common { data | quantity = map_ data.quantity }

        RelativeTimeParamForm common data ->
            RelativeTimeParamForm common { data | quantity = map_ data.quantity }

        GroupTextParamForm common data ->
            param


emptyTaskForm : TaskForm
emptyTaskForm =
    TaskForm
        { label = Nothing
        , params = []
        }


emptyTextParamForm : String -> TaskParamForm
emptyTextParamForm id =
    TextParamForm (emptyCommonParamFormData id) emptyTextParamFormData


emptyLongTextParamForm : String -> TaskParamForm
emptyLongTextParamForm id =
    LongTextParamForm (emptyCommonParamFormData id) emptyTextParamFormData


emptySensitiveTextParamForm : String -> TaskParamForm
emptySensitiveTextParamForm id =
    SensitiveTextParamForm (emptyCommonParamFormData id)
        emptyTextParamFormData
        { showText = False }


emptyNumberParamForm : String -> TaskParamForm
emptyNumberParamForm id =
    NumberParamForm (emptyCommonParamFormData id)
        emptyNumberParamFormData


emptyRelativeSizeParamForm : String -> TaskParamForm
emptyRelativeSizeParamForm id =
    emptyRelativeSizeParamFormWithDefaults id
        { unit = Validation.initial, comparison = Validation.initial }


emptyRelativeSizeParamFormWithDefaults :
    String
    ->
        { a
            | unit : ValidationResult FileSizeUnit
            , comparison : ValidationResult FileSizeComparison
        }
    -> TaskParamForm
emptyRelativeSizeParamFormWithDefaults id { unit, comparison } =
    RelativeSizeParamForm (emptyCommonParamFormData id)
        { quantity = emptyNumberParamFormData
        , unit =
            { value = unit
            , options = FileSizeUnit.enum
            , toString = FileSizeUnit.toString
            }
        , comparison =
            { value = comparison
            , options = FileSizeComparison.enum
            , toString = FileSizeComparison.toString
            }
        }


emptyRelativeTimeParamForm : String -> TaskParamForm
emptyRelativeTimeParamForm id =
    emptyRelativeTimeParamFormWithDefaults id
        { timeVar = Validation.initial
        , unit = Validation.initial
        , comparison = Validation.initial
        , range = Validation.initial
        }


emptyRelativeTimeParamFormWithDefaults :
    String
    ->
        { a
            | timeVar : ValidationResult TimeVar
            , unit : ValidationResult TimeUnit
            , comparison : ValidationResult TimeComparison
            , range : ValidationResult TimeRange
        }
    -> TaskParamForm
emptyRelativeTimeParamFormWithDefaults id { timeVar, unit, comparison, range } =
    RelativeTimeParamForm (emptyCommonParamFormData id)
        { timeVar =
            { value = timeVar
            , options = TimeVar.enum
            , toString = TimeVar.toString
            }
        , quantity = emptyNumberParamFormData
        , unit =
            { value = unit
            , options = TimeUnit.enum
            , toString = TimeUnit.toString
            }
        , comparison =
            { value = comparison
            , options = TimeComparison.enum
            , toString = TimeComparison.toString
            }
        , range =
            { value = range
            , options = TimeRange.enum
            , toString = TimeRange.toString
            }
        }


emptyGroupTextParamForm : String -> TaskParamForm
emptyGroupTextParamForm id =
    GroupTextParamForm (emptyCommonParamFormData id)
        { values = []
        , validate = Ok -- even empty strings are ok here, by default
        }


emptyCommonParamFormData : String -> CommonParamFormData
emptyCommonParamFormData id =
    { id = id
    , label = Nothing
    , width = Nothing
    }


emptyTextParamFormData : TextParamFormData
emptyTextParamFormData =
    { value = Validation.initial
    , validate = requiredString
    }


emptyNumberParamFormData : NumberParamFormData
emptyNumberParamFormData =
    { value = Validation.initial
    , min = Nothing
    , max = Nothing
    , step = Nothing
    , validate = requiredString >> Result.andThen String.toInt
    }



-- UTILS


slug : String -> Maybe String
slug s =
    Slug.generate s |> Maybe.map Slug.toString


validateNotEmpty : String -> String -> Result String String
validateNotEmpty msg s =
    if s == "" then
        Err msg
    else
        Ok s
