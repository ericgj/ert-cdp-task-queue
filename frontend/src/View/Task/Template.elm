module View.Task.Template
    exposing
        ( TemplateForm
        , map
        , form
        , describe
        , validate
        , checkFiles
        , sas
          {--TODO
    , zip
    , sftp
    , email
--}
        )

import Validation exposing (ValidationResult)
import Data.Value.TimeVar as TimeVar
import Data.Value.TimeUnit as TimeUnit
import Data.Value.TimeComparison as TimeComparison
import Data.Value.TimeRange as TimeRange
import Data.Value.FileSizeComparison as FileSizeComparison
import Data.Task
import Data.Task.Template exposing (Template)
import View.Task exposing (..)


type TemplateForm
    = CheckFiles TaskForm
    | Sas TaskForm



{--TODO
    | Zip TaskForm
    | Sftp TaskForm
    | Email TaskForm
--}


map : (TaskForm -> TaskForm) -> TemplateForm -> TemplateForm
map map_ template =
    case template of
        CheckFiles form ->
            CheckFiles (map_ form)

        Sas form ->
            Sas (map_ form)



-- -----------------------------------------------------------------------------
-- GETTERS
-- -----------------------------------------------------------------------------


form : TemplateForm -> TaskForm
form template =
    case template of
        CheckFiles form ->
            form

        Sas form ->
            form


describe : TemplateForm -> String
describe template =
    case template of
        CheckFiles form ->
            "Check files"

        Sas form ->
            "Run SAS program"



-- -----------------------------------------------------------------------------
-- VALIDATION
-- -----------------------------------------------------------------------------


validate : TemplateForm -> ValidationResult Template
validate template =
    case template of
        CheckFiles form ->
            View.Task.validate form
                |> Validation.map Data.Task.Template.CheckFiles

        Sas form ->
            View.Task.validate form
                |> Validation.map Data.Task.Template.Sas



-- -----------------------------------------------------------------------------
-- TEMPLATE CONSTRUCTORS
-- -----------------------------------------------------------------------------


checkFiles : TemplateForm
checkFiles =
    CheckFiles <|
        taskForm "Check that files exist"
            [ textParam "in directory"
                |> withId "directory"
                |> required
            , textParam "named"
                |> withId "fileglob"
                |> required
            , relativeTimeParam "modified"
                { timeVar = Validation.valid TimeVar.Scheduled
                , unit = Validation.valid TimeUnit.Minutes
                , comparison = Validation.valid TimeComparison.Before
                , range = Validation.valid TimeRange.Within
                }
                |> optional False
            , relativeSizeParam "file size is"
                { unit = Validation.initial
                , comparison = Validation.valid FileSizeComparison.GreaterThanOrEqualTo
                }
                |> withId "filesize"
                |> optional False
            ]


sas : TemplateForm
sas =
    Sas <|
        taskForm "Run SAS program"
            [ textParam "in directory"
                |> withId "directory"
                |> withText "%HOME%"
                |> required
            , textParam "program file"
                |> required
            , groupTextParam "with SAS options"
                |> withId "options"
                |> addTextParam "-NOOVP"
                |> addTextParam "-SPLASH OFF"
                |> addTextParam "-ERRORABEND"
                |> optional True
            , groupTextParam "with SAS environment variables"
                |> withId "vars"
                |> optional False
            ]
