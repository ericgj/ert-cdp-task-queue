module View.Task.Sequence
    exposing
        ( SequenceForm
        , Msg
        , empty
        , update
        , view
        )

import Json.Encode as JE
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Extra exposing (onBlur, onChange)
import Validation exposing (ValidationResult)
import Validation.Extra exposing (requiredString)
import Data.Task.Template exposing (Template)
import Data.Task.Sequence exposing (Sequence(..))
import View.Task
import View.Task.Template as Template exposing (TemplateForm)


type SequenceForm
    = SequenceForm
        { selected : Maybe Int
        , name : ValidationResult String
        , forms : List TemplateForm
        }


empty : SequenceForm
empty =
    SequenceForm
        { selected = Nothing
        , name = Validation.initial
        , forms = []
        }


validate : SequenceForm -> ValidationResult Sequence
validate (SequenceForm { name, forms }) =
    Validation.valid
        (\name_ templates_ ->
            Sequence { name = name_, templates = templates_ }
        )
        |> Validation.andMap name
        |> Validation.andMap
            (formValidation forms |> List.reverse |> Validation.sequence)


formValidation : List TemplateForm -> List (ValidationResult Template)
formValidation templates =
    List.map Template.validate templates


validateNameInput : String -> Result String String
validateNameInput =
    requiredString


type Msg
    = SelectTemplate Int
    | DeselectTemplate
    | AddTemplate TemplateForm
    | RemoveTemplate Int
    | UpdateTemplate Int View.Task.Msg
    | SetNameInput String
    | ValidateName String
    | Save Sequence
    | NoOp


update : Msg -> SequenceForm -> ( SequenceForm, Cmd Msg )
update msg (SequenceForm seq) =
    case msg of
        SelectTemplate index ->
            ( SequenceForm { seq | selected = Just index }
            , Cmd.none
            )

        DeselectTemplate ->
            ( SequenceForm { seq | selected = Nothing }
            , Cmd.none
            )

        AddTemplate template ->
            ( SequenceForm
                { seq
                    | forms = template :: seq.forms
                    , selected = Just 0
                }
            , Cmd.none
            )

        RemoveTemplate index ->
            ( SequenceForm
                { seq
                    | forms =
                        seq.forms
                            |> List.take index
                            |> (\first ->
                                    first ++ (List.drop (index + 1) seq.forms)
                               )
                }
            , Cmd.none
            )

        UpdateTemplate index formmsg ->
            ( SequenceForm
                { seq
                    | forms =
                        seq.forms
                            |> List.indexedMap
                                (\i template ->
                                    if index == i then
                                        Template.map (View.Task.update formmsg) template
                                    else
                                        template
                                )
                }
            , Cmd.none
            )

        SetNameInput input ->
            ( SequenceForm
                { seq | name = Validation.unvalidated input }
            , Cmd.none
            )

        ValidateName input ->
            ( SequenceForm
                { seq | name = Validation.validate validateNameInput input }
            , Cmd.none
            )

        -- TODO
        Save sequence ->
            let
                _ =
                    Debug.log "Save" <|
                        JE.encode 0 <|
                            Data.Task.Sequence.encode sequence
            in
                ( SequenceForm seq, Cmd.none )

        NoOp ->
            ( SequenceForm seq, Cmd.none )


view : List TemplateForm -> SequenceForm -> Html Msg
view templates ((SequenceForm { selected, name, forms }) as seq) =
    let
        validStates =
            formValidation forms
    in
        div
            []
            ((List.map2 (,) validStates forms
                |> List.indexedMap (viewTemplate selected)
                |> List.reverse
             )
                ++ [ viewAddTemplate templates
                   , viewName name
                   , viewSave
                        (validate seq)
                        forms
                   ]
            )


viewTemplate : Maybe Int -> Int -> ( ValidationResult Template, TemplateForm ) -> Html Msg
viewTemplate selected index ( result, template ) =
    if (selected |> Maybe.map ((==) index) |> Maybe.withDefault False) then
        viewTemplateHelp True (Validation.isValid result) index template
    else
        viewTemplateHelp False (Validation.isValid result) index template


viewTemplateHelp : Bool -> Bool -> Int -> TemplateForm -> Html Msg
viewTemplateHelp isSelected isValid index template =
    let
        form =
            Template.form template

        selectId =
            ("template-" ++ (toString index) ++ "-select")

        validStyle =
            if isValid then
                []
            else
                [ ( "color", "red" ) ]
    in
        div
            []
            ([ input
                [ type_ "checkbox"
                , id selectId
                , checked isSelected
                , onClick
                    (if isSelected then
                        DeselectTemplate
                     else
                        SelectTemplate index
                    )
                ]
                []
             , label
                [ for selectId
                , style validStyle
                ]
                [ text <| View.Task.describe form
                ]
             ]
                ++ (if isSelected then
                        [ View.Task.view (Template.form template)
                            |> Html.map (UpdateTemplate index)
                        ]
                    else
                        []
                   )
            )


viewAddTemplate : List TemplateForm -> Html Msg
viewAddTemplate templates =
    let
        find =
            String.toInt
                >> Result.toMaybe
                >> Maybe.map (\i -> List.drop i templates)
                >> Maybe.andThen List.head
    in
        select
            [ onInput
                (find >> Maybe.map AddTemplate >> Maybe.withDefault NoOp)
            ]
            ((option [ value "", selected True ] [ text "Add a step..." ])
                :: (List.indexedMap viewAddTemplateOption templates)
            )


viewAddTemplateOption : Int -> TemplateForm -> Html msg
viewAddTemplateOption index template =
    option
        [ value (toString index)
        ]
        [ text <| Template.describe template
        ]


viewName : ValidationResult String -> Html Msg
viewName result =
    let
        nameId =
            "sequence-name"
    in
        div
            []
            [ label
                [ for nameId
                ]
                [ text "Name of this sequence"
                ]
            , input
                [ type_ "text"
                , id nameId
                , value (Validation.toString identity result)
                , onInput SetNameInput
                , onBlur ValidateName
                ]
                []
            , Html.div
                []
                [ Html.text
                    (Validation.message result |> Maybe.withDefault "")
                ]
            ]


viewSave : ValidationResult Sequence -> List TemplateForm -> Html Msg
viewSave result templates =
    div
        []
        [ button
            [ onClick
                (result
                    |> Validation.map Save
                    |> Validation.withDefault NoOp
                )
            , disabled <|
                ((not <| Validation.isValid result)
                    || (List.isEmpty templates)
                )
            ]
            [ text "Save" ]
        ]
