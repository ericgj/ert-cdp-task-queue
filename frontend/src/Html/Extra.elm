module Html.Extra
    exposing
        ( onBlur
        , onChange
        )

import Json.Decode as JD
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (on, targetValue)


onChange : (String -> msg) -> Html.Attribute msg
onChange tagger =
    on "change" (JD.map tagger targetValue)


onBlur : (String -> msg) -> Html.Attribute msg
onBlur tagger =
    on "blur" (JD.map tagger targetValue)
